/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.BrandEO;

/**
 * Repository type class responsible for carrying out transactions to the brands table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see BrandEO
 * @see Integer
 */
@Repository("brandRepository")
public interface BrandRepository extends JpaRepository<BrandEO, Integer> {
	
	/**
	 * Method in charge of searching all brands
	 * @return List all brands
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_BRAND, NAME FROM TBL_CAT_BRANDS WHERE STATUS = 1 AND NAME LIKE %?1% ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchByLetter(@Param("letter") final String letter);

}