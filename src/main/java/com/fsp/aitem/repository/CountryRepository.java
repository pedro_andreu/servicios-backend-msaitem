/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.CountryEO;

/**
 * Repository type class responsible for carrying out transactions to the countries table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see CountryEO
 * @see Integer
 */
@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<CountryEO, Integer> {
	
	/**
	 * Method in charge of searching all active countries
	 * @return List all active countries
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_COUNTRY, NAME FROM TBL_CAT_COUNTRIES WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();
    
    /**
	 * Method in charge of searching all sat codes
	 * @return List all sat codes
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying
	@Query(value = "SELECT ID_COUNTRY, NAME FROM TBL_CAT_COUNTRIES WHERE STATUS = 1 AND NAME LIKE %?1%  ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchByLetter(@Param("letter") final String letter);

}