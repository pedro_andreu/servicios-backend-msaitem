/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.ExclusionListEO;

/**
 * Repository type class responsible for carrying out transactions to the exclusion list table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see ExclusionListEO
 * @see Integer
 */
@Repository("exclusionListRepository")
public interface ExclusionListRepository extends JpaRepository<ExclusionListEO, Integer> {
	
	/**
	 * Method in charge of searching all exclusion list
	 * @return List all exclusion list
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_EXCLUSION_LIST, NAME FROM TBL_CAT_EXCLUSIONS_LIST WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();

}