package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.DocumentEO;

@Repository("documentRepository")
public interface DocumentRepository extends JpaRepository<DocumentEO, Integer> {
	
	@SuppressWarnings("unchecked")
	@Transactional
	public DocumentEO saveAndFlush(final DocumentEO imageEO);
	
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_DOCUMENT, NAME, LINK FROM TBL_CAT_DOCUMENTS WHERE FK_ITEM = :itemId AND STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchDocumentById(@Param("itemId") final Integer id);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE TBL_CAT_DOCUMENTS SET NAME = :name, LINK = :link WHERE ID_DOCUMENT = :id", nativeQuery = true)
	public Integer updateDocumentById(@Param("id") final Integer id, @Param("name") final String name, @Param("link") final String link);
	
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT D.ID_DOCUMENT, D.NAME, B.BARCODE FROM aitemdb.TBL_CAT_DOCUMENTS D " + 
	"INNER JOIN TBL_CAT_ITEMS T ON T.ID_ITEM = D.FK_ITEM " + 
	"INNER JOIN TBL_CAT_BARCODES B ON B.FK_ITEM = T.ID_ITEM " + 
	"WHERE D.ID_DOCUMENT = :id AND B.TYPE = 'Unitario' AND D.STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> getDocument(@Param("id") final Integer id);
	
	public void deleteById(@Param("id") final Integer id);

}