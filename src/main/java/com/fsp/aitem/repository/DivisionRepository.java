/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.DivisionEO;

/**
 * Repository type class responsible for carrying out transactions to the divisions table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see DivisionEO
 * @see Integer
 */
@Repository("divisionRepository")
public interface DivisionRepository extends JpaRepository<DivisionEO, Integer> {
	
	/**
	 * Method in charge of searching all divisions
	 * @return List all divisions
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_DIVISION, NAME FROM TBL_CAT_DIVISIONS WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();
    
    /**
	 * Method in charge of searching a division
	 * @return List all divisions
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_DIVISION, NAME FROM TBL_CAT_DIVISIONS WHERE ID_DIVISION = :id AND STATUS = 1", nativeQuery = true)
    public List<Object[]> searchById(@Param("id") final Integer id);

}