/*
 * Copyright Farmacias San Pablo
 * 21-10-2019
 */
package com.fsp.aitem.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.ItemEO;

/**
 * Repository type class responsible for handling all transactions in the item catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see ItemEO
 * @see Integer
 */
@Repository("itemRepository")
public interface ItemRepository extends JpaRepository<ItemEO, Integer> {
	
	/**
	 * Method responsible for creating a item
	 * @param item, object that stores the values for the creation of a item
	 * @return ItemEO, which stores the created values
	 * @see ItemEO
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false)
	@Modifying(clearAutomatically = true)
	public ItemEO saveAndFlush(final ItemEO item);
	
	/**
	 * Method in charge of searching for a item by id
	 * @param id, item identifier
	 * @return Optional<ItemEO>, object that stores the search result
	 * @see Optional
	 * @see ItemEO
	 * @see Integer
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	public Optional<ItemEO> findById(final Integer id);
	
	/**
	 * Method responsible for searching all item
	 * @return List, object that stores a list with all items
	 * @see List
	 * @see ItemEO
	 */
    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true) 
	@Query(value = "SELECT * FROM TBL_CAT_ITEMS I INNER JOIN TBL_CAT_USERS U ON I.FK_USER_CREATION = U.ID_USER " +
			"OR I.FK_USER_PROVIDER = U.ID_USER WHERE SAP_USER = :sap_user AND I.PROCESS_STATUS = 0 AND I.STATUS = 1", nativeQuery = true)
    public List<ItemEO> searchByProvider(@Param("sap_user") final String sap);
    
    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
    @Query(value = "SELECT * FROM TBL_CAT_ITEMS I INNER JOIN TBL_CAT_USERS U ON I.FK_USER_CREATION = U.ID_USER " +
			"OR I.FK_USER_NEGOTIATOR = U.ID_USER WHERE SAP_USER = :sap_user AND I.PROCESS_STATUS = 0 AND I.STATUS = 1", nativeQuery = true)
    public List<ItemEO> searchByNegotiator(@Param("sap_user") final String sap);
    
    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT * FROM TBL_CAT_ITEMS I WHERE I.ID_ITEM = :id AND I.PROCESS_STATUS = 0 AND I.STATUS = 1", nativeQuery = true)
    public List<ItemEO> searchId(@Param("id") final Integer id);
    
    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT * FROM TBL_CAT_ITEMS I WHERE I.PROCESS_STATUS = 0 AND I.STATUS = 1", nativeQuery = true)
    public List<ItemEO> searchAll();
    
    /**
	 * 
	 */
    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
    @Query(value = "SELECT B.BARCODE FROM TBL_CAT_ITEMS I INNER JOIN TBL_CAT_BARCODES B ON I.ID_ITEM = B.FK_ITEM"
			+ " WHERE B.TYPE = 'Unitario' AND I.ID_ITEM = :id AND I.PROCESS_STATUS = 0 AND I.STATUS = 1", nativeQuery = true)
    public List<Object[]> searchBarcodeById(@Param("id") final Integer id);

    @Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
    @Query(value = "SELECT B.BARCODE FROM TBL_CAT_ITEMS I INNER JOIN TBL_CAT_BARCODES B ON I.ID_ITEM = B.FK_ITEM"
			+ " WHERE B.BARCODE = :barcode AND I.STATUS = 1", nativeQuery = true)
    public List<Object[]> searchItemByBarcode(@Param("barcode") final String barcode);
	
    /**
	 * Method responsible for updating the values of a item
	 * @param user, object that stores the values to update of a item
	 * @return Integer, which stores the updated values
	 * @see Integer
	 */
	@Transactional
	@Modifying
	@Query(value = "UPDATE TBL_CAT_ITEMS SET PROCESS_STATUS = :processStatus, MODIFICATION_DATE = :modificationDate WHERE ID_ITEM = :id", 
	nativeQuery = true)
	public Integer updateProcessStatusById(@Param("id") final Integer id, @Param("processStatus") final Boolean processStatus, @Param("modificationDate") final Date modificationDate);

	@Transactional
	@Modifying
	@Query(value = "UPDATE TBL_CAT_ITEMS SET COMPLETED = :completed, MODIFICATION_DATE = :modificationDate WHERE ID_ITEM = :id", 
	nativeQuery = true)
	public Integer updateCompletedById(@Param("id") final Integer id, @Param("completed") final Boolean completed, @Param("modificationDate") final Date modificationDate);
	
}