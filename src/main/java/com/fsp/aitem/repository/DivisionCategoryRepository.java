/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.DivisionCategoryEO;

/**
 * Repository type class responsible for carrying out transactions to the divisions categories table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see DivisionCategoryEO
 * @see Integer
 */
@Repository("divisionCategoryRepository")
public interface DivisionCategoryRepository extends JpaRepository<DivisionCategoryEO, Integer> {
	
	/**
	 * Method in charge of searching all division categories
	 * @return List all division categories
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_DIVISION_CATEGORY, NAME FROM TBL_CAT_DIVISIONS_CATEGORIES WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();

}