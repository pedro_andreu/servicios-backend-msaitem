/*
 * Copyright Farmacias San Pablo
 * 22-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.ItemTypeEO;

/**
 * Repository type class responsible for carrying out transactions to the item types table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see ItemTypeEO
 * @see Integer
 */
@Repository("itemTypeRepository")
public interface ItemTypeRepository extends JpaRepository<ItemTypeEO, Integer> {
	
	/**
	 * Method in charge of searching all item types
	 * @return List all item types
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_ITEM_TYPE, NAME FROM TBL_CAT_ITEM_TYPES WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();

}