/*
 * Copyright Farmacias San Pablo
 * 23-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.UMConcentrationTypeEO;

/**
 * Repository type class responsible for carrying out transactions to the um concentrations types table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see UMConcentrationTypeEO
 * @see Integer
 */
@Repository("umConcentrationRepository")
public interface UMConcentrationRepository extends JpaRepository<UMConcentrationTypeEO, Integer> {
	
	/**
	 * Method in charge of searching all um concentrations of type presentation
	 * @return List all um concentrations
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_UM_CONCENTRATION_TYPE, UNIT_MEASURED FROM TBL_CAT_UM_CONCENTRATIONS_TYPES WHERE PRESENTATION = 1 AND STATUS = 1 ORDER BY UNIT_MEASURED ASC", nativeQuery = true)
	public List<Object[]> searchPresentations();
	
	/**
	 * Method in charge of searching all um concentrations of type farmaceutical form
	 * @return List all um concentrations
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_UM_CONCENTRATION_TYPE, UNIT_MEASURED FROM TBL_CAT_UM_CONCENTRATIONS_TYPES WHERE FARMACEUTICAL_FORM = 1 AND STATUS = 1 ORDER BY UNIT_MEASURED ASC", nativeQuery = true)
	public List<Object[]> searchFarmaceuticalForm();
	
	/**
	 * Method in charge of searching all um concentrations of type um net content
	 * @return List all um concentrations
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_UM_CONCENTRATION_TYPE, UNIT_MEASURED FROM TBL_CAT_UM_CONCENTRATIONS_TYPES WHERE UM_NET_CONTENT = 1 AND STATUS = 1 ORDER BY UNIT_MEASURED ASC", nativeQuery = true)
	public List<Object[]> searchUmNetContent();
	
	/**
	 * Method in charge of searching all um concentrations of type um concentration
	 * @return List all um concentrations
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_UM_CONCENTRATION_TYPE, UNIT_MEASURED FROM TBL_CAT_UM_CONCENTRATIONS_TYPES WHERE UM_CONCENTRATION = 1 AND STATUS = 1 ORDER BY UNIT_MEASURED ASC", nativeQuery = true)
	public List<Object[]> searchUmConcentration();
	
	/**
	 * Method in charge of searching um concentration by id
	 * @param Integer id
	 * @return List all um concentrations
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT SAP_UM_CONCENTRATION_TYPE FROM TBL_CAT_UM_CONCENTRATIONS_TYPES WHERE ID_UM_CONCENTRATION_TYPE = :id AND STATUS = 1", nativeQuery = true)
	public List<Object[]> searchById(@Param("id") final Integer id);
	
}