/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.FiscalClassificationEO;
import com.fsp.aitem.vo.FiscalClassificationVO;

/**
 * Repository type class responsible for carrying out transactions to the fiscal classification table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see FiscalClassificationEO
 * @see Integer
 */
@Repository("fiscalClassificationRepository")
public interface FiscalClassificationRepository extends JpaRepository<FiscalClassificationEO, Integer> {
	
	/**
	 * Method in charge of searching all sales fiscal classification
	 * @return List all sales fiscal classification
	 * @see Object
	 * @see List
	 * @see FiscalClassificationVO
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_FISCAL_CLASSIFICATION, DENOMINATION FROM TBL_CAT_FISCALS_CLASSIFICATIONS WHERE TYPE = '" + FiscalClassificationVO.SALE + "' AND STATUS = 1 ORDER BY DENOMINATION ASC", nativeQuery = true)
    public List<Object[]> searchSales();
    
    /**
	 * Method in charge of searching all purchase fiscal classification
	 * @return List all purchase fiscal classification
	 * @see Object
	 * @see List
	 * @see FiscalClassificationVO
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_FISCAL_CLASSIFICATION, DENOMINATION FROM TBL_CAT_FISCALS_CLASSIFICATIONS WHERE TYPE = '" + FiscalClassificationVO.PURCHASE + "' AND STATUS = 1 ORDER BY DENOMINATION ASC", nativeQuery = true)
    public List<Object[]> searchPurchases();


}