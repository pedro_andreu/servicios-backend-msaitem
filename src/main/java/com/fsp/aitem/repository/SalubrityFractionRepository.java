/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.SalubrityFractionEO;

/**
 * Repository type class responsible for carrying out transactions to the salubrity fractions table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see SalubrityFractionEO
 * @see Integer
 */
@Repository("salubrityFractionRepository")
public interface SalubrityFractionRepository extends JpaRepository<SalubrityFractionEO, Integer> {
	
	/**
	 * Method in charge of searching all salubrity fractions
	 * @return List all salubrity fractions
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_SALUBRITY_FRACTION, NAME FROM TBL_CAT_SALUBRITYS_FRACTIONS WHERE STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
    public List<Object[]> search();

}