/*
 * Copyright Farmacias San Pablo
 * 16-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.ActiveSubstanceTypeEO;

/**
 * Repository type class responsible for carrying out transactions to the active substance type table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see ActiveSubstanceTypeEO
 * @see Integer
 */
@Repository("activeSubstanceTypeRepository")
public interface ActiveSubstanceTypeRepository extends JpaRepository<ActiveSubstanceTypeEO, Integer> {
	
	/**
	 * Method in charge of searching all active substances types
	 * @return List all active substances types
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_ACTIVE_SUBSTANCE_TYPE, NAME FROM TBL_CAT_ACTIVE_SUBSTANCES_TYPES WHERE STATUS = 1 AND NAME LIKE %?1% ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchByLetter(@Param("letter") final String letter);

}