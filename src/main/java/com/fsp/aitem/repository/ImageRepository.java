package com.fsp.aitem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.ImageEO;

@Repository("imageRepository")
public interface ImageRepository extends JpaRepository<ImageEO, Integer> {
	
	@SuppressWarnings("unchecked")
	@Transactional
	public ImageEO saveAndFlush(final ImageEO imageEO);
	
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT ID_IMAGE, NAME, LINK FROM TBL_CAT_IMAGES WHERE FK_ITEM = :itemId AND STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchByItemId(@Param("itemId") final Integer id);
	
	@Transactional//(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE TBL_CAT_IMAGES SET NAME = :name, LINK = :link WHERE ID_IMAGE = :id", nativeQuery = true)
	public Integer updateById(@Param("id") final Integer id, @Param("name") final String name, @Param("link") final String link);
	
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)
	@Query(value = "SELECT I.ID_IMAGE, I.NAME, B.BARCODE FROM aitemdb.TBL_CAT_IMAGES I " + 
	"INNER JOIN TBL_CAT_ITEMS T ON T.ID_ITEM = I.FK_ITEM " + 
	"INNER JOIN TBL_CAT_BARCODES B ON B.FK_ITEM = T.ID_ITEM " + 
	"WHERE I.ID_IMAGE = :id AND B.TYPE = 'Unitario' AND I.STATUS = 1 ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> getImage(@Param("id") final Integer id);
	
	public void deleteById(@Param("id") final Integer id);

}