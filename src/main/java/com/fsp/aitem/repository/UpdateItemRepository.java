package com.fsp.aitem.repository;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.ItemEO;

@Repository("updateItemRepository")
public class UpdateItemRepository {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Transactional
	public Integer createUMConcentration(final Integer fkItem, final Integer fkConcentration, final String type) {
		return this.entityManager.createNativeQuery("INSERT INTO TBL_CAT_UM_CONCENTRATIONS (FK_ITEM, FK_UM_CONCENTRATION_TYPE, TYPE, STATUS) VALUES (?,?,?,?)")
	      .setParameter(1, fkItem)
	      .setParameter(2, fkConcentration)
	      .setParameter(3, type)
	      .setParameter(4, 1)
	      .executeUpdate();
	}

	@Transactional
	public Integer createActiveSubstance(final Integer fkItem, final Integer fkActiveSubstance, final String type) {
		return this.entityManager.createNativeQuery("INSERT INTO TBL_CAT_ACTIVE_SUBSTANCES (FK_ITEM, FK_ACTIVE_SUBSTANCE_TYPE, TYPE, STATUS) VALUES (?,?,?,?)")
	      .setParameter(1, fkItem)
	      .setParameter(2, fkActiveSubstance)
	      .setParameter(3, type)
	      .setParameter(4, 1)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer createBarcode(final Integer fkItem, final String type, final String barcode, final BigDecimal grossWeight,
			final BigDecimal netWeight, final BigDecimal longitude, final BigDecimal height, final BigDecimal width, final BigDecimal volume) {
		return this.entityManager.createNativeQuery("INSERT INTO TBL_CAT_BARCODES " + 
	             "(FK_ITEM, TYPE, BARCODE, GROSS_WEIGHT, NET_WEIGHT, LONGITUDE, HEIGHT, WIDTH, VOLUME, STATUS) VALUES (?,?,?,?,?,?,?,?,?,?)")
	      .setParameter(1, fkItem)
	      .setParameter(2, type)
	      .setParameter(3, barcode)
	      .setParameter(4, grossWeight)
	      .setParameter(5, netWeight)
	      .setParameter(6, longitude)
	      .setParameter(7, height)
	      .setParameter(8, width)
	      .setParameter(9, volume)
	      .setParameter(10, 1)
	      .executeUpdate();
	}

	@Transactional
	public Integer deleteUMConcentration(final Integer idConcentration, final Integer fkItem) {
		return this.entityManager.createNativeQuery("DELETE FROM TBL_CAT_UM_CONCENTRATIONS WHERE ID_UM_CONCENTRATION = ? AND FK_ITEM = ?")
	      .setParameter(1, idConcentration)
	      .setParameter(2, fkItem)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteActiveSubstance(final Integer idActiveSubstance, final Integer fkItem) {
		return this.entityManager.createNativeQuery("DELETE FROM TBL_CAT_ACTIVE_SUBSTANCES WHERE ID_ACTIVE_SUBSTANCE = ? AND FK_ITEM = ?")
	      .setParameter(1, idActiveSubstance)
	      .setParameter(2, fkItem)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteBarcode(final Integer idBarcode, final Integer fkItem) {
		return this.entityManager.createNativeQuery("DELETE FROM TBL_CAT_BARCODES WHERE ID_BARCODE = ? AND FK_ITEM = ?")
	      .setParameter(1, idBarcode)
	      .setParameter(2, fkItem)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer updateItem(final ItemEO item) {
		return this.entityManager.createNativeQuery("UPDATE TBL_CAT_ITEMS SET " + 
			      "FK_DIVISION = ?, FK_ITEM_TYPE = ?, FK_BRAND = ?, FK_USER_NEGOTIATOR = ?, " + 
			      "FK_USER_PROVIDER = ?, FK_COUNTRY = ?, FK_DIVISION_CATEGORY = ?, FK_SAT_CODE = ?, " + 
			      "FK_EXCLUSION_LIST = ?, FK_SALUBRITY_FRACTION = ?, FK_FISCAL_SALE = ?, FK_FISCAL_PURCHASE = ?, " +
			      "DESCRIPTION = ?, TYPE = ?, COMMERCIAL_NAME = ?, NET_CONTENT = ?, CONCENTRATION = ?, " + 
			      "BRAND = ?, PRODUCT = ?, VARIABLE = ?, ACTIVE_SUBSTANCE = ?, BRAND_PROVIDER = ?, CONC_SUST_ACT_PRINT = ?, " +
			      "CONC_SUST_ACT_PRINT_1 = ?, CONC_SUST_ACT_PRINT_2 = ?, PACKING_UNIT = ?, " +
			      "ANTIBIOTIC = ?, HIGH_ESPECIALITY = ?, MAX_PRICE = ?, LIST_COST = ? " + 
			      "WHERE ID_ITEM = ?")
	      .setParameter(1, item.getDivisionEO().getId())
	      .setParameter(2, item.getItemTypeEO().getId())
	      .setParameter(3, item.getBrandEO().getId())
	      .setParameter(4, item.getUserNegotiator().getId())
	      .setParameter(5, item.getUserProvider().getId())
	      .setParameter(6, item.getCountryEO().getId())
	      .setParameter(7, item.getDivisionCategoryEO().getId())
	      .setParameter(8, item.getSatCodeEO().getId())
	      .setParameter(9, item.getExclusionListEO().getId())
	      .setParameter(10, item.getSalubrityFractionEO().getId())
	      .setParameter(11, item.getFiscalSale().getId())
	      .setParameter(12, item.getFiscalPurchase().getId())
	      .setParameter(13, item.getDescription())
	      .setParameter(14, item.getType())
	      .setParameter(15, item.getCommercialName())
	      .setParameter(16, item.getNetContent())
	      .setParameter(17, item.getConcentration())
	      .setParameter(18, item.getBrandItem())
	      .setParameter(19, item.getProduct())
	      .setParameter(20, item.getVariable())
	      .setParameter(21, item.getActiveSubstance())
	      .setParameter(22, item.getBrandProvider())
	      .setParameter(23, item.getConcentrationActiveSubtance())
	      .setParameter(24, item.getConcentrationActiveSubtance1())
	      .setParameter(25, item.getConcentrationActiveSubtance2())
	      .setParameter(26, item.getPackingUnit())
	      .setParameter(27, item.getAntibiotic())
	      .setParameter(28, item.getHighEspeciality())
	      .setParameter(29, item.getMaxPrice())
	      .setParameter(30, item.getListCost())
	      .setParameter(31, item.getId())
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteItem(final Integer itemId) {
		return this.entityManager.createNativeQuery("UPDATE TBL_CAT_ITEMS SET STATUS = ?, "
				+ "MODIFICATION_DATE = ? WHERE ID_ITEM = ?")
	      .setParameter(1, 0)
	      .setParameter(2, new Date())
	      .setParameter(3, itemId)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteItemBarcode(final Integer barcodeId, final Integer itemFk) {
		return this.entityManager.createNativeQuery("UPDATE TBL_CAT_BARCODES SET STATUS = ?, "
				+ "MODIFICATION_DATE = ? WHERE ID_BARCODE = ? AND FK_ITEM = ?")
	      .setParameter(1, 0)
	      .setParameter(2, new Date())
	      .setParameter(3, barcodeId)
	      .setParameter(4, itemFk)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteItemUMConcentration(final Integer concentrationId, final Integer itemFk) {
		return this.entityManager.createNativeQuery("UPDATE TBL_CAT_UM_CONCENTRATIONS SET STATUS = ?, "
				+ "MODIFICATION_DATE = ? WHERE ID_UM_CONCENTRATION = ? AND FK_ITEM = ?")
	      .setParameter(1, 0)
	      .setParameter(2, new Date())
	      .setParameter(3, concentrationId)
	      .setParameter(4, itemFk)
	      .executeUpdate();
	}
	
	@Transactional
	public Integer deleteItemActiveSubstance(final Integer activeSubstanceId, final Integer itemFk) {
		return this.entityManager.createNativeQuery("UPDATE TBL_CAT_ACTIVE_SUBSTANCES SET STATUS = ?, "
				+ "MODIFICATION_DATE = ? WHERE ID_ACTIVE_SUBSTANCE = ? AND FK_ITEM = ?")
	      .setParameter(1, 0)
	      .setParameter(2, new Date())
	      .setParameter(3, activeSubstanceId)
	      .setParameter(4, itemFk)
	      .executeUpdate();
	}

}