/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsp.aitem.eo.SatCodeEO;

/**
 * Repository type class responsible for carrying out transactions to the sat codes table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see SatCodeEO
 * @see Integer
 */
@Repository("satCodeRepository")
public interface SatCodeRepository extends JpaRepository<SatCodeEO, Integer> {
	
	/**
	 * Method in charge of searching all sat codes
	 * @return List all sat codes
	 * @see Object
	 * @see List
	 */
	@Transactional
	@Modifying
	@Query(value = "SELECT ID_SAT_CODE, SAP_SAT_CODE, NAME FROM TBL_CAT_SAT_CODES WHERE STATUS = 1 AND SAP_SAT_CODE LIKE CONCAT('%', :code, '%') ORDER BY SAP_SAT_CODE ASC", nativeQuery = true)
	public List<Object[]> searchByCode(@Param("code") final String code);
    
}