/*
 * Copyright Farmacias San Pablo
 * 18-10-2019
 */
package com.fsp.aitem.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fsp.aitem.eo.UserEO;

/**
 * Repository type class responsible to make queries to the users table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see JpaRepository
 * @see UserEO
 * @see Integer
 */
@Repository("userRepository")
public interface UserRepository extends JpaRepository<UserEO, Integer> {
	
	/**
	 * Method in charge of searching a user by type
	 * @return List all users
	 * @see String
	 * @see Object
	 * @see List
	 */
	@Transactional(readOnly = true)
	@Modifying(clearAutomatically = true)	
	@Query(value = "SELECT C.ID_USER, C.NAME, C.SECOND_NAME, C.FIRST_NAME, C.LAST_NAME FROM TBL_CAT_USERS C " + 
			"INNER JOIN TBL_CAT_ROLES R ON C.FK_ROLE = R.ID_ROLE WHERE C.FK_ROLE = 3 AND C.TYPE = :type AND C.STATUS = 1", nativeQuery = true)
	public List<Object[]> searchUser(@Param("type") final String type);
	
	public Optional<UserEO> findById(final Integer id);
	
	@Transactional
	@Modifying
	@Query(value = "SELECT ID_USER, NAME FROM TBL_CAT_USERS WHERE TYPE = :type AND STATUS = 1 AND NAME LIKE %:letter% ORDER BY NAME ASC", nativeQuery = true)
	public List<Object[]> searchUserByLetter(@Param("type") final String type, @Param("letter") final String letter);
	
	@Transactional
	@Modifying
	@Query(value = "SELECT U.ID_USER, U.SAP_USER, U.TYPE, R.NAME FROM TBL_CAT_USERS U INNER JOIN TBL_CAT_ROLES R ON U.FK_ROLE = R.ID_ROLE WHERE U.ID_USER = :id", nativeQuery = true)
	public List<Object[]> searchUserAndRole(@Param("id") final Integer id);

}