package com.fsp.aitem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.fsp.commonutil.filter.GlobalHandlerFilter;

@SpringBootApplication
public class App {
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

    @Bean
    public FilterRegistrationBean<GlobalHandlerFilter> myFilter() {
        FilterRegistrationBean<GlobalHandlerFilter> registration = new FilterRegistrationBean<>();
        GlobalHandlerFilter myFilter = new GlobalHandlerFilter();
        this.beanFactory.autowireBean(myFilter);
        registration.setFilter(myFilter);
        registration.addUrlPatterns("*");
        return registration;
    }

}