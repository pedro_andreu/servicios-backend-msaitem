package com.fsp.aitem.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ImageJSON implements Serializable {

	private static final long serialVersionUID = -8078796292124605402L;
	@JsonProperty(value = "id")
	private Integer id;
	@JsonProperty(value = "name")
	private String name;
	@JsonProperty(value = "link")
	private String link;
	
	public ImageJSON(final Integer id, final String name, final String link) {
		this.setId(id);
		this.setName(name);
		this.setLink(link);
	}

}