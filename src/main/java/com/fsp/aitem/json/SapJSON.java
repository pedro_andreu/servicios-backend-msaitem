package com.fsp.aitem.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SapJSON implements Serializable {

	private static final long serialVersionUID = 2898219678119324180L;
	@JsonProperty(value = "message")
	private String message;
	@JsonProperty(value = "id")
	private String id;
	@JsonProperty(value = "number")
	private String number;
	@JsonProperty(value = "type")
	private String type;

}