package com.fsp.aitem.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseSAPJSON implements Serializable {

	private static final long serialVersionUID = 6302283998264022164L;
	@JsonProperty(value = "id")
	private Integer id;
	@JsonProperty(value = "process_status")
	private Boolean procesStatus;
	
	public ResponseSAPJSON(final Integer id, final Boolean procesStatus) {
		this.setId(id);
		this.setProcesStatus(procesStatus);
	}

}