package com.fsp.aitem.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ItemImagesJSON implements Serializable {

	private static final long serialVersionUID = -4637670897502149399L;
	@JsonProperty(value = "id")
	private Integer id;
	@JsonProperty(value = "barcode")
	private String barcode;
	@JsonProperty(value = "images")
	private List<ImageJSON> images;
	
	public ItemImagesJSON(final Integer id, final String barcode, final List<ImageJSON> images) {
		this.setId(id);
		this.setBarcode(barcode);
	}
	
	public ItemImagesJSON() {
		
	}
	
}