package com.fsp.aitem.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class IdJSON implements Serializable {

	private static final long serialVersionUID = -4109904903484406648L;
	@JsonProperty(value = "id")
	private Integer id;
	
	public IdJSON(final Integer id) {
		this.setId(id);
	}

}