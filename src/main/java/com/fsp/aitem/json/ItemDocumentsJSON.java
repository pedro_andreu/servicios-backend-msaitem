package com.fsp.aitem.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ItemDocumentsJSON implements Serializable {

	private static final long serialVersionUID = -8730191443952748951L;
	@JsonProperty(value = "id")
	private Integer id;
	@JsonProperty(value = "barcode")
	private String barcode;
	@JsonProperty(value = "documents")
	private List<DocumentJSON> documents;
	
	public ItemDocumentsJSON(final Integer id, final String barcode, final List<DocumentJSON> documents) {
		this.setId(id);
		this.setBarcode(barcode);
	}
	
	public ItemDocumentsJSON() {
		
	}
	
}