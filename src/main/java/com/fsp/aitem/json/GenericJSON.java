package com.fsp.aitem.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenericJSON implements Serializable {
	
	private static final long serialVersionUID = -481091816014410378L;
	@JsonProperty(value = "id")
	private Integer id;
	@JsonProperty(value = "name")
	private String name;
	
	public GenericJSON(final Integer id, final String name) {
		this.setId(id);
		this.setName(name);
	}

}