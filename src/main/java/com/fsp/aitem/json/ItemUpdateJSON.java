package com.fsp.aitem.json;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Size;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Digits;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Component("itemUpdateJSON")
public class ItemUpdateJSON implements Serializable {
	
	private static final long serialVersionUID = -6909465603316150011L;
	@JsonProperty(access = Access.AUTO, value = "id", required = false)
	private Integer id;
	@JsonProperty(value = "article_type", required = false)
	@Size(min = 0, max = 4, message = "The type property cannot be longer than 4 characters")
	private String type;
	@JsonProperty(access = Access.AUTO, value = "negotiator", required = false)
	@Size(min = 0, max = 12, message = "The negociator property cannot be longer than 12 characters")
	private String negociator;
	@JsonProperty(access = Access.AUTO, value = "provider", required = false)
    @Size(min = 0, max = 45, message = "The provider property cannot be longer than 10 characters")
	private String provider; 
	@JsonProperty(access = Access.AUTO, value = "providerUpdate", required = false)
    private GenericJSON providerUpdate; 
	@JsonProperty(access = Access.AUTO, value = "item_type", required = false)
	@Size(min = 0, max = 1, message = "The item_type property cannot be longer than 1 characters")
	private String itemType;
	@JsonProperty(access = Access.AUTO, value = "division", required = false)
	@Size(min = 0, max = 20, message = "The division property cannot be longer than 20 characters")
	private String division;
	@JsonProperty(access = Access.AUTO, value = "description", required = false)
	@Size(min = 0, max = 100, message = "The description property cannot be longer than 100 characters")
	private String description;
	@JsonProperty(access = Access.AUTO, value = "brand", required = false)
	@Size(min = 0, max = 6, message = "The brand property cannot be longer than 6 characters")
	private String brand;
	@JsonProperty(access = Access.AUTO, value = "brandUpdate", required = false)
	private GenericJSON brandUpdate;
	@JsonProperty(access = Access.AUTO, value = "product", required = false)
	@Size(min = 0, max = 20, message = "The product property cannot be longer than 20 characters")
	private String product;
	@JsonProperty(access = Access.AUTO, value = "commercial_name", required = false)
	@Size(min = 0, max = 20, message = "The commercial name cannot be longer than 20 characters")
	private String commercialName;
	@JsonProperty(access = Access.AUTO, value = "active_substance", required = false)
	@Size(min = 0, max = 20, message = "The active_substance property cannot be longer than 20 characters")
	private String activeSubstance;
	@JsonProperty(access = Access.AUTO, value = "presentation", required = false)
	@Size(min = 0, max = 3, message = "The presentation property cannot be longer than 3 characters")
	private String presentation;
	@JsonProperty(access = Access.AUTO, value = "farmaceutical_form", required = false)
	@Size(min = 0, max = 3, message = "The farmaceutical_form property cannot be longer than 3 characters")
	private String farmaceuticalForm;
	@JsonProperty(access = Access.AUTO, value = "um_net_content", required = false)
	@Size(min = 0, max = 3, message = "The um_net_content property cannot be longer than 3 characters")
	private String umNetContent;
	@JsonProperty(access = Access.AUTO, value = "concentration_unit_measure", required = false)
	@Size(min = 0, max = 3, message = "The concentration_unit_measure property cannot be longer than 3 characters")
	private String concentrationUnitMeasure;
	@JsonProperty(access = Access.AUTO, value = "concentration_unit_measure_1", required = false)
	@Size(min = 0, max = 3, message = "The concentration_active_subtance_1 property cannot be longer than 3 characters")
	private String concentrationUnitMeasure1;
	@JsonProperty(access = Access.AUTO, value = "concentration_unit_measure_2", required = false)
	@Size(min = 0, max = 3, message = "The concentration_active_subtance_2 property cannot be longer than 3 characters")
	private String concentrationUnitMeasure2;
	@JsonProperty(value = "um_concentration", required = false)
	@Size(min = 0, max = 3, message = "The um cocentration property cannot be longer than 3 characters")
	private String umConcentration;
	@JsonProperty(access = Access.AUTO, value = "net_content", required = false)
	@Size(min = 0, max = 20, message = "The net content property cannot be longer than 20 characters")
	private String netContent;
	@JsonProperty(access = Access.AUTO, value = "concentration", required = false)
	@Size(min = 0, max = 20, message = "The concentration property cannot be longer than 20 characters")
	private String concentration;
	@JsonProperty(access = Access.AUTO, value = "brand_provider", required = false)
	@Size(min = 0, max = 20, message = "The brand/provider property cannot be longer than 20 characters")
	private String brandProvider;
	@JsonProperty(access = Access.AUTO, value = "variable", required = false)
	@Size(min = 0, max = 20, message = "The variable property cannot be longer than 20 characters")
	private String variable;
	@NotBlank
	@JsonProperty(access = Access.AUTO, value = "item_bar_code", required = true)
	@Size(min = 0, max = 18, message = "The unit barcode property cannot be longer than 18 characters")
	private String unitBarcode;
	@JsonProperty(access = Access.AUTO, value = "box_barcode")
	@Size(min = 0, max = 18, message = "The box barcode property cannot be longer than 18 characters")
	private String boxBarcode;
	@Digits(integer = 16, fraction = 3, message = "The packing unit property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "packing_unit", required = false)
	private BigDecimal packingUnit;
	@Digits(integer = 16, fraction = 3, message = "The gross weight property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "gross_weight", required = false)
	private BigDecimal grossWeight;
	@Digits(integer = 16, fraction = 3, message = "The net weight property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "net_weight", required = false)
	private BigDecimal netWeight;
	@Digits(integer = 16, fraction = 3, message = "The long item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "long_item", required = false)
	private BigDecimal longItem;
	@Digits(integer = 16, fraction = 3, message = "The width item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "width_item", required = false)
	private BigDecimal widthItem;
	@Digits(integer = 16, fraction = 3, message = "The height item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "height_item", required = false)
	private BigDecimal heightItem;
	@Digits(integer = 16, fraction = 3, message = "The volume item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "volume_item", required = false)
	private BigDecimal volumeItem;
	@Digits(integer = 16, fraction = 3, message = "The box gross weight property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_gross_weight", required = false)
	private BigDecimal boxGrossWeight;
	@Digits(integer = 16, fraction = 3, message = "The box net weight property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_net_weight", required = false)
	private BigDecimal boxNetWeight;
	@Digits(integer = 16, fraction = 3, message = "The box long item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_long_item", required = false)
	private BigDecimal boxLongItem;
	@Digits(integer = 16, fraction = 3, message = "The box width item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_width_item", required = false)
	private BigDecimal boxWidthItem;
	@Digits(integer = 16, fraction = 3, message = "The box height item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_height_item", required = false)
	private BigDecimal boxHeightItem;
	@Digits(integer = 16, fraction = 3, message = "The box volume item property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "box_volume_item", required = false)
	private BigDecimal boxVolumeItem;
	@JsonProperty(access = Access.AUTO, value = "exclusion_list", required = false)
	@Size(min = 0, max = 3, message = "The exclusion list property cannot be longer than 3 characters")
	private String exclusionList;
	@JsonProperty(access = Access.AUTO, value = "division_category", required = false)
	@Size(min = 0, max = 18, message = "The division category property cannot be longer than 18 characters")
	private String divisionCategory;
	@JsonProperty(access = Access.AUTO, value = "brand_item", required = false)
	@Size(min = 0, max = 20, message = "The brand item property cannot be longer than 20 characters")
	private String brandItem;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substance", required = false)
	@Size(min = 0, max = 6, message = "The principal_active_substance property cannot be longer than 6 characters")
	private String principalActiveSubstance;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substance_1", required = false)
	@Size(min = 0, max = 6, message = "The principal_active_substance_1 property cannot be longer than 6 characters")
	private String principalActiveSubstance1;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substance_2", required = false)
	@Size(min = 0, max = 6, message = "The principal_active_substance_2 property cannot be longer than 6 characters")
	private String principalActiveSubstance2;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substanceUpdate", required = false)
	private GenericJSON principalActiveSubstanceUpdate;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substance_1Update", required = false)
	private GenericJSON principalActiveSubstance1Update;
	@JsonProperty(access = Access.AUTO, value = "principal_active_substance_2Update", required = false)
	private GenericJSON principalActiveSubstance2Update;
	@JsonProperty(value = "concentration_active_subtance", required = false)
	@Digits(integer = 16, fraction = 3, message = "The concentration active subtance roperty cannot be longer than 16.3 digits")
	private BigDecimal concentrationActiveSubtance;
	@JsonProperty(value = "concentration_active_subtance_1", required = false)
	@Digits(integer = 16, fraction = 3, message = "The concentration active subtance 1 property cannot be longer than 16.3 digits")
	private BigDecimal concentrationActiveSubtance1;
	@JsonProperty(value = "concentration_active_subtance_2", required = false)
	@Digits(integer = 16, fraction = 3, message = "The concentration active subtance 2 property cannot be longer than 16.3 digits")
	private BigDecimal concentrationActiveSubtance2;
	@JsonProperty(access = Access.AUTO, value = "antibiotic", required = false)
	private Boolean antibiotic;
	@JsonProperty(access = Access.AUTO, value = "high_especiality", required = false)
	private Boolean highEspeciality;
	@JsonProperty(value = "salubrity_fraction", required = false)
	@Size(min = 0, max = 2, message = "The salubrity fraction property cannot be longer than 2 characters")
	private String salubrityFraction;
	@Digits(integer = 16, fraction = 3, message = "The max price property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "max_price", required = false)
	private BigDecimal maxPrice;
	@Digits(integer = 16, fraction = 3, message = "The list cost property cannot be longer than 16.3 digits")
	@JsonProperty(access = Access.AUTO, value = "list_cost", required = false)
	private BigDecimal listCost;
	@JsonProperty(value = "fiscal_sales", required = false)
	@Size(min = 0, max = 2, message = "The fiscal sales property cannot be longer than 1 characters")
	private String fiscalSales;
	@JsonProperty(value = "fiscal_purchase", required = false)
	@Size(min = 0, max = 2, message = "The fiscal purchase property cannot be longer than 2 characters")
	private String fiscalPurchase;
	@JsonProperty(value = "sat_code", required = false)
	@Size(min = 0, max = 18, message = "The sat code property cannot be longer than 18 characters")
	private String satCode;
	@JsonProperty(value = "sat_codeUpdate", required = false)
	private GenericJSON satCodeUpdate;
	@JsonProperty(access = Access.AUTO, value = "country", required = false)
	@Size(min = 0, max = 3, message = "The country property cannot be longer than 3 characters")
	private String country;
	@JsonProperty(access = Access.AUTO, value = "countryUpdate", required = false)
	private GenericJSON countryUpdate;
	@JsonProperty(access = Access.AUTO, value = "process_status", required = false)
	private Boolean processStatus;

}