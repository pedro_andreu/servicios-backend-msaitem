package com.fsp.aitem.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.SapJSON;

@Component("restTemplateUtil")
public class RestTemplateUtil {
	
	private Boolean resultSap;
	
	public ResponseEntity<SapJSON> createRestTemplate(
			final ItemJSON item, 
			final String uuid) {
		String authStr = "MAT_PIP_000:Fsp$2020%";
		//String authStr = "WIN_PIQ_0001:Fsps2019";
	    String base64Creds = Base64.getEncoder().encodeToString(authStr.getBytes());
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Authorization", "Basic " + base64Creds);
	    HttpEntity<ItemJSON> request = new HttpEntity<>(item, headers);
		final String uri = "https://b2bfsanpablo.com/RESTAdapter/SendJSON/VendorMaterial";
	    //final String uri = "http://fsppiqas.fsanpablo.com:55000/RESTAdapter/SendJSON/VendorMaterial";
	    RestTemplate restTemplate = new RestTemplate();
	    List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
	    MediaType mediaTypeJson = new MediaType("application", "json");
	    MediaType mediaTypeXml = new MediaType("application", "xml");
	    MediaType mediaTypeText = new MediaType("plain", "text");
	    List<MediaType> supportedMediaTypes = new ArrayList<MediaType>(Arrays.asList(mediaTypeJson, mediaTypeXml, mediaTypeText));
	    MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
	    jacksonConverter.setSupportedMediaTypes(supportedMediaTypes);
	    messageConverters.add(jacksonConverter);
	    restTemplate.setMessageConverters(messageConverters);
	    ResponseEntity<SapJSON> sap = restTemplate.exchange( uri, HttpMethod.POST, request, SapJSON.class);
	    return sap;
	}
	
	public Boolean getTypeSap(
			final String barcode, 
			final String uuid) {
		this.resultSap = false;
		ResponseEntity<SapJSON> entitySap = this.createRestTemplate(new ItemJSON(barcode, "VALIDATE"), uuid);
		Optional<Integer> optionalEntity = Optional.of(entitySap.getStatusCode().value()).filter((c) -> c == HttpStatus.OK.value());
	    optionalEntity.ifPresent((var r) -> {
	    	SapJSON sap = entitySap.getBody();
	    	Optional<String> optionalType = Optional.of(sap.getType());
	    	optionalType.filter((t) -> t.equals("E")).ifPresent((var t) -> {
	    		this.resultSap = true;
		    });
	    });
	    return this.resultSap;
	}
	    
}