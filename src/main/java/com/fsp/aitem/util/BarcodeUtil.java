/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.aitem.util;

import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fsp.aitem.dto.BarcodeDTO;
import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.SapJSON;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.SAProcessException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Utility class responsible for provide the necessary utilities to validate a barcode
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 * @see Boolean
 */
@Component("barcodeUtil")
public class BarcodeUtil {
	
	private String barcode;
	private Boolean result;
	
	/**
	 * Method in charge of search a bar code by its id
	 * @param itemRepository
	 * @param id
	 * @param uuid
	 * @return String bar code
	 * @see String
	 * @see ItemRepository
	 * @see Integer
	 * @see Optional
	 * @see List
	 * @see Object
	 * @throws DataAccessException
	 */
	public String isValidBarcode(
			final ItemRepository itemRepository,
			final Integer id, 
			final String uuid) 
					throws DBAccessException {
		try {
			this.barcode = null;
			Optional<List<Object[]>> optional = Optional.of(itemRepository.searchBarcodeById(id));
			optional.filter((var l) -> l.size() != 0).ifPresent((var r) -> {
				r.forEach((var b) -> {
					this.barcode = String.valueOf(b[0]);		
				});
			});
			return this.barcode;
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
	}
	
	/**
	 * Method in charge of search a bar code by its bar code
	 * @param itemRepository
	 * @param barcode
	 * @param uuid
	 * @return String bar code
	 * @see String
	 * @see ItemRepository
	 * @see Optional
	 */
	public String isValidBarcode(
			final ItemRepository itemRepository,
			final String barcode, 
			final String uuid) {
		this.barcode = null;
		Optional.of(itemRepository.searchItemByBarcode(barcode)).
		filter((var l) -> l.size() != 0).ifPresent((var r) -> {
			r.forEach((var b) -> {
				this.barcode = String.valueOf(b[0]);		
		    });
		});
		return this.barcode;
	}
	
	/**
	 * Method in charge of search a bar code
	 * @param itemRepository
	 * @param list
	 * @param uuid
	 * @return Boolean result
	 * @see ItemRepository
	 * @see List
	 * @see BarcodeDTO
	 * @see String
	 * @see Boolean
	 */
	public Boolean isValidBarcode(
			final ItemRepository itemRepository,
			final List<BarcodeDTO> list,
			final String uuid) {
		this.result = true;
		list.forEach((var bc) -> {
			if(null != this.isValidBarcode(itemRepository, bc.getBarcode(), uuid)) {
				this.result = false;
			} 
		});
		return this.result;
	}
	
	/**
	 * Method in charge of look for a bar code in sap
	 * @param restTemplateUtil
	 * @param list
	 * @param uuid
	 * @return Boolean result
	 * @see RestTemplateUtil
	 * @see List
	 * @see BarcodeDTO
	 * @see ResponseEntity
	 * @see SapJSON
	 * @see Optional
	 * @see Integer
	 * @see HttpStatus
	 * @see Boolean
	 * @see String
	 * @throws SAProcessException
	 */
	public Boolean isValidBarcodeSap(
			final RestTemplateUtil restTemplateUtil,
			final List<BarcodeDTO> list,
			final String uuid) {
		this.result = false;
		list.forEach((var bc) -> {
			ResponseEntity<SapJSON> entitySap = restTemplateUtil.createRestTemplate(new ItemJSON(bc.getBarcode(), "VALIDATE"), uuid);
			Optional<Integer> optionalEntity = Optional.of(entitySap.getStatusCode().value());
			optionalEntity.filter((c) -> c == HttpStatus.OK.value()).ifPresent((var r) -> {
				if(restTemplateUtil.getTypeSap(bc.getBarcode(), uuid)) {
					this.result = true;
				} 
			});
			optionalEntity.filter((c) -> c != HttpStatus.OK.value()).ifPresent((var e) -> {
				throw new SAProcessException(ResponseMessageVO.ERROR_MESSAGE, uuid);
			});
		});
		return this.result;
	}

}