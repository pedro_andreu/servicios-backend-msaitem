/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.aitem.util;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fsp.aitem.dto.ActiveSubstanceDTO;
import com.fsp.aitem.dto.BarcodeDTO;
import com.fsp.aitem.dto.BrandDTO;
import com.fsp.aitem.dto.CountryDTO;
import com.fsp.aitem.dto.DivisionCategoryDTO;
import com.fsp.aitem.dto.DivisionDTO;
import com.fsp.aitem.dto.ExclusionListDTO;
import com.fsp.aitem.dto.FiscalClassificationDTO;
import com.fsp.aitem.dto.ItemDTO;
import com.fsp.aitem.dto.ItemTypeDTO;
import com.fsp.aitem.dto.SalubrityFractionDTO;
import com.fsp.aitem.dto.SatCodeDTO;
import com.fsp.aitem.dto.UMConcentrationDTO;
import com.fsp.aitem.dto.UserDTO;
import com.fsp.aitem.eo.DivisionEO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.json.GenericJSON;
import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.ItemUpdateJSON;

/**
 * Utility class responsible for provide the necessary utilities to item convert
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 * @see List
 * @see ItemJSON  
 */
@Component("itemUtil")
public class ItemUtil {
	
	@Value("${item.type}")
	private String type;
	@Value("${item.barcode.unit}")
	private String unit;
	@Value("${item.barcode.corrugated}")
	private String corrugated;
	@Value("${user.provider}")
	private String provider;
	@Value("${user.negotiator}")
	private String negotiator;	
	@Value("${item.active.substance}")
	private String substance;
	@Value("${item.active.substance.1}")
	private String substance1;
	@Value("${item.active.substance.2}")
	private String substance2;
	@Value("${um.concentration.presentation}")
	private String presentation;
	@Value("${um.concentration.farmaceutical.form}")
	private String farmaceuticalForm;
	@Value("${um.concentration.net.cont}")
	private String netCont;
	@Value("${um.concentration.concentration}")
	private String concentrationUm;
	@Value("${um.concentration.unit.mesaure}")
	private String unitMesaure;
	@Value("${um.concentration.unit.mesaure.1}")
	private String unitMesaure1;
	@Value("${um.concentration.unit.mesaure.2}")
	private String unitMesaure2;
	@Value("${fiscal.classification.sale}")
	private String sale;
	@Value("${fiscal.classification.purchase}")
	private String purchase;
	
	private List<ItemJSON> items = null;
	
	/**
	 * Method in charge of convert a json to dto
	 * @param id
	 * @param itemJSON
	 * @return ItemDTO item
	 * @see Integer
	 * @see ItemJSON
	 * @see List
	 * @see ItemDTO
	 * @see String
	 * @see Optional
	 */
	public ItemDTO convertToDTO(final Integer id, final ItemJSON itemJSON) {
		ItemDTO item = new ItemDTO();
		item.setId(id);
		Optional<String> type = Optional.ofNullable(itemJSON.getType());
		type.filter((var t) -> !t.equals("")).ifPresent((var ti) -> {
			item.setType(ti);
		});
		type.filter((var t) -> t.equals("")).ifPresent((var ti) -> {
			item.setType(null);
		});
		Optional<String> division = Optional.ofNullable(itemJSON.getDivision());
		division.filter((var d) -> !d.equals("")).ifPresent((var di) -> {
			item.setDivisionDTO(new DivisionDTO(Integer.parseInt(di)));
		});
		division.filter((var d) -> d.equals("")).ifPresent((var di) -> {
			item.setDivisionDTO(new DivisionDTO());
		});
		Optional<String> itemType = Optional.ofNullable(itemJSON.getItemType());
		itemType.filter((var it) -> !it.equals("")).ifPresent((var iti) -> {
			item.setItemTypeDTO(new ItemTypeDTO(Integer.parseInt(iti)));
		});
		itemType.filter((var it) -> it.equals("")).ifPresent((var iti) -> {
			item.setItemTypeDTO(new ItemTypeDTO());
		});
		Optional<String> brand = Optional.ofNullable(itemJSON.getBrand());
		brand.filter((var b) -> !b.equals("")).ifPresent((var bi) -> {
			item.setBrandDTO(new BrandDTO(Integer.parseInt(bi)));
		});
		brand.filter((var b) -> b.equals("")).ifPresent((var bi) -> {
			item.setBrandDTO(new BrandDTO());
		});
		Optional<String> negotiator = Optional.ofNullable(itemJSON.getNegociator());
		negotiator.filter((var n) -> !n.equals("")).ifPresent((var ni) -> {
			item.setUserNegotiator(new UserDTO(Integer.parseInt(ni)));
		});
		negotiator.filter((var n) -> n.equals("")).ifPresent((var ni) -> {
			item.setUserNegotiator(new UserDTO());
		});
		Optional<String> provider = Optional.ofNullable(itemJSON.getProvider());
		provider.filter((var p) -> !p.equals("")).ifPresent((var pi) -> {
			item.setUserProvider(new UserDTO(Integer.parseInt(pi)));
		});
		provider.filter((var p) -> p.equals("")).ifPresent((var pi) -> {
			item.setUserProvider(new UserDTO());
		});
		Optional<String> divisionCategory = Optional.ofNullable(itemJSON.getDivisionCategory());
		divisionCategory.filter((var dc) -> !dc.equals("")).ifPresent((var dci) -> {
			item.setDivisionCategoryDTO(new DivisionCategoryDTO(Integer.parseInt(dci)));
		});
		divisionCategory.filter((var dc) -> dc.equals("")).ifPresent((var dci) -> {
			item.setDivisionCategoryDTO(new DivisionCategoryDTO());
		});
		Optional<String> country = Optional.ofNullable(itemJSON.getCountry());
		country.filter((var c) -> !c.equals("")).ifPresent((var ci) -> {
			item.setCountryDTO(new CountryDTO(Integer.parseInt(ci)));
		});
		country.filter((var p) -> p.equals("")).ifPresent((var pi) -> {
			item.setCountryDTO(new CountryDTO());
		});
		Optional<String> satCode = Optional.ofNullable(itemJSON.getSatCode());
		satCode.filter((var sc) -> !sc.equals("")).ifPresent((var sci) -> {
			item.setSatCodeDTO(new SatCodeDTO(Integer.parseInt(sci)));
		});
		satCode.filter((var sc) -> sc.equals("")).ifPresent((var sci) -> {
			item.setSatCodeDTO(new SatCodeDTO());
		});
		Optional<String> exclusionList = Optional.ofNullable(itemJSON.getExclusionList());
		exclusionList.filter((var el) -> !el.equals("")).ifPresent((var eli) -> {
			item.setExclusionListDTO(new ExclusionListDTO(Integer.parseInt(eli)));
		});
		exclusionList.filter((var el) -> el.equals("")).ifPresent((var eli) -> {
			item.setExclusionListDTO(new ExclusionListDTO());
		});
		Optional<String> salubtityFraction = Optional.ofNullable(itemJSON.getExclusionList());
		salubtityFraction.filter((var sf) -> !sf.equals("")).ifPresent((var sfi) -> {
			item.setSalubrityFractionDTO(new SalubrityFractionDTO(Integer.parseInt(sfi)));
		});
		salubtityFraction.filter((var sf) -> sf.equals("")).ifPresent((var sfi) -> {
			item.setSalubrityFractionDTO(new SalubrityFractionDTO());
		});
		Optional<String> sales = Optional.ofNullable(itemJSON.getExclusionList());
		sales.filter((var fcs) -> !fcs.equals("")).ifPresent((var fcsi) -> {
			item.setFiscalSale(new FiscalClassificationDTO(Integer.parseInt(fcsi)));
		});
		sales.filter((var fcs) -> fcs.equals("")).ifPresent((var sfsi) -> {
			item.setFiscalSale(new FiscalClassificationDTO());
		});
		Optional<String> purchase = Optional.ofNullable(itemJSON.getFiscalPurchase());
		purchase.filter((var fcp) -> !fcp.equals("")).ifPresent((var fcpi) -> {
			item.setFiscalPurchase(new FiscalClassificationDTO(Integer.parseInt(fcpi)));
		});
		purchase.filter((var fcp) -> fcp.equals("")).ifPresent((var sfpi) -> {
			item.setFiscalPurchase(new FiscalClassificationDTO());
		});
		item.setProduct(itemJSON.getProduct());
		item.setActiveSubstance(itemJSON.getActiveSubstance());
		item.setCommercialName(itemJSON.getCommercialName());
		List<UMConcentrationDTO> umcontrations = new LinkedList<UMConcentrationDTO>();
		Optional.ofNullable(itemJSON.getPresentation()).filter(
				(var p) -> !p.isEmpty()).
		ifPresent((var p) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(p), this.presentation));
		});
		Optional.ofNullable(itemJSON.getFarmaceuticalForm()).filter(
				(var f) -> !f.isEmpty()).
		ifPresent((var f) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(f), this.farmaceuticalForm));
		});
		Optional.ofNullable(itemJSON.getUmNetContent()).filter(
				(var u) -> !u.isEmpty()).
		ifPresent((var u) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(u), this.netCont));
		});
		Optional.ofNullable(itemJSON.getUmConcentration()).filter(
				(var c) -> !c.isEmpty()).
		ifPresent((var c) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(c), this.concentrationUm));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure()).filter(
				(var cum) -> !cum.isEmpty()).
		ifPresent((var cum) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum), this.unitMesaure));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure1()).filter(
				(var cum1) -> !cum1.isEmpty()).
		ifPresent((var cum1) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum1), this.unitMesaure1));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure2()).filter(
				(var cum2) -> !cum2.isEmpty()).
		ifPresent((var cum2) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum2), this.unitMesaure2));
		});
		item.setUmcontrations(umcontrations);
		item.setNetContent(itemJSON.getNetContent());
		item.setConcentration(itemJSON.getConcentration());
		item.setBrandProvider(itemJSON.getBrandProvider());
		item.setVariable(itemJSON.getVariable());
		List<BarcodeDTO> barcodes = new LinkedList<BarcodeDTO>();
		Optional.ofNullable(itemJSON.getUnitBarcode()).filter(
				(var ub) -> !ub.isEmpty()).
		ifPresent((var ub) -> {
			BarcodeDTO unit = new BarcodeDTO();
			unit.setBarcode(ub);
			unit.setType(this.unit);
			unit.setGrossWeight(itemJSON.getGrossWeight());
			unit.setNetWeight(itemJSON.getNetWeight());
			unit.setLongitude(itemJSON.getLongItem());
			unit.setWidth(itemJSON.getWidthItem());
			unit.setHeight(itemJSON.getHeightItem());
			unit.setVolume(itemJSON.getVolumeItem());
			barcodes.add(unit);
		});
		Optional.ofNullable(itemJSON.getBoxBarcode()).filter(
				(var bb) -> !bb.isEmpty()).
		ifPresent((var bb) -> {
			BarcodeDTO box = new BarcodeDTO();
			box.setBarcode(bb);
			box.setType(this.corrugated);
			box.setGrossWeight(itemJSON.getBoxGrossWeight());
			box.setNetWeight(itemJSON.getBoxNetWeight());
			box.setLongitude(itemJSON.getBoxLongItem());
			box.setWidth(itemJSON.getBoxWidthItem());
			box.setHeight(itemJSON.getBoxHeightItem());
			box.setVolume(itemJSON.getBoxVolumeItem());
			barcodes.add(box);
		});
		item.setBarcodes(barcodes);
		item.setPackingUnit(itemJSON.getPackingUnit());
		item.setBrand(itemJSON.getBrandItem());
		List<ActiveSubstanceDTO> activeSubstances =  new LinkedList<ActiveSubstanceDTO>();
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance()).filter(
				(var pas) -> !pas.isEmpty()).
		ifPresent((var pas) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas), this.substance));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance1()).filter(
				(var pas1) -> !pas1.isEmpty()).
		ifPresent((var pas1) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas1), this.substance1));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance2()).filter(
				(var pas2) -> !pas2.isEmpty()).
		ifPresent((var pas2) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas2), this.substance2));
		});
		item.setActiveSubstances(activeSubstances);
		
		
		
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance(itemJSON.getConcentrationActiveSubtance());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance1()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance1(itemJSON.getConcentrationActiveSubtance1());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance2()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance2(itemJSON.getConcentrationActiveSubtance2());
		});
		
		
		
		item.setAntibiotic(itemJSON.getAntibiotic());
		item.setHighEspeciality(itemJSON.getHighEspeciality());
		item.setBrandItem(itemJSON.getBrandItem());
		item.setMaxPrice(itemJSON.getMaxPrice());
		item.setListCost(itemJSON.getListCost());
		return item;
	}
	
	/**
	 * Method in charge of convert a json to dto
	 * @param itemJSON
	 * @return ItemDTO item
	 * @see Integer
	 * @see ItemJSON
	 * @see List
	 * @see ItemDTO
	 * @see String
	 * @see Optional
	 * @see BarcodeDTO
	 */
	public ItemDTO convertToDTO(final ItemJSON itemJSON) {
		ItemDTO item = new ItemDTO();
		item.setType(this.type);
		Optional.ofNullable(itemJSON.getNegociator()).filter((var n) -> !n.equals("")).ifPresent((var v) -> {
			item.setUserNegotiator(new UserDTO(Integer.parseInt(v)));
		});
		Optional.ofNullable(itemJSON.getProvider()).filter((var p) -> !p.equals("")).ifPresent((var v) -> {
			item.setUserProvider(new UserDTO(Integer.parseInt(v)));
		});
		Optional.ofNullable(itemJSON.getBrand()).filter((var b) -> !b.equals("")).ifPresent((v) -> {
			item.setBrandDTO(new BrandDTO(Integer.parseInt(v)));
		});
		Optional.ofNullable(itemJSON.getItemType()).filter((it) -> !it.equals("")).ifPresent((i) -> {
			item.setItemTypeDTO(new ItemTypeDTO(Integer.parseInt(i)));
		});
		Optional.ofNullable(itemJSON.getDivision()).filter((a) -> !a.equals("")).ifPresent((d) -> {
			item.setDivisionDTO(new DivisionDTO(Integer.parseInt(d)));
		});
		
		item.setProduct(itemJSON.getProduct());
		item.setActiveSubstance(itemJSON.getActiveSubstance());
		item.setCommercialName(itemJSON.getCommercialName());
		List<UMConcentrationDTO> umcontrations = new LinkedList<UMConcentrationDTO>();
		Optional.ofNullable(itemJSON.getPresentation()).filter(
				(var p) -> !p.isEmpty()).
		ifPresent((var p) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(p), this.presentation));
		});
		Optional.ofNullable(itemJSON.getFarmaceuticalForm()).filter(
				(var f) -> !f.isEmpty()).
		ifPresent((var f) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(f), this.farmaceuticalForm));
		});
		Optional.ofNullable(itemJSON.getUmNetContent()).filter(
				(var u) -> !u.isEmpty()).
		ifPresent((var u) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(u), this.netCont));
		});
		Optional.ofNullable(itemJSON.getUmConcentration()).filter(
				(var c) -> !c.isEmpty()).
		ifPresent((var c) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(c), this.concentrationUm));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure()).filter(
				(var cum) -> !cum.isEmpty()).
		ifPresent((var cum) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum), this.unitMesaure));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure1()).filter(
				(var cum1) -> !cum1.isEmpty()).
		ifPresent((var cum1) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum1), this.unitMesaure1));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure2()).filter(
				(var cum2) -> !cum2.isEmpty()).
		ifPresent((var cum2) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum2), this.unitMesaure2));
		});
		item.setUmcontrations(umcontrations);
		item.setNetContent(itemJSON.getNetContent());
		item.setConcentration(itemJSON.getConcentration());
		item.setBrandProvider(itemJSON.getBrandProvider());
		item.setVariable(itemJSON.getVariable());
		List<BarcodeDTO> barcodes = new LinkedList<BarcodeDTO>();
		Optional.ofNullable(itemJSON.getUnitBarcode()).filter(
				(var ub) -> !ub.isEmpty()).
		ifPresent((var ub) -> {
			BarcodeDTO unit = new BarcodeDTO();
			unit.setBarcode(ub);
			unit.setType(this.unit);
			unit.setGrossWeight(itemJSON.getGrossWeight());
			unit.setNetWeight(itemJSON.getNetWeight());
			unit.setLongitude(itemJSON.getLongItem());
			unit.setWidth(itemJSON.getWidthItem());
			unit.setHeight(itemJSON.getHeightItem());
			unit.setVolume(itemJSON.getVolumeItem());
			barcodes.add(unit);
		});
		Optional.ofNullable(itemJSON.getBoxBarcode()).filter(
				(var bb) -> !bb.isEmpty()).
		ifPresent((var bb) -> {
			BarcodeDTO box = new BarcodeDTO();
			box.setBarcode(bb);
			box.setType(this.corrugated);
			box.setGrossWeight(itemJSON.getBoxGrossWeight());
			box.setNetWeight(itemJSON.getBoxNetWeight());
			box.setLongitude(itemJSON.getBoxLongItem());
			box.setWidth(itemJSON.getBoxWidthItem());
			box.setHeight(itemJSON.getBoxHeightItem());
			box.setVolume(itemJSON.getBoxVolumeItem());
			barcodes.add(box);
		});
		item.setBarcodes(barcodes);
		item.setPackingUnit(itemJSON.getPackingUnit());
		Optional.ofNullable(itemJSON.getExclusionList()).filter((a) -> !a.equals("")).ifPresent((el) -> {
			item.setExclusionListDTO(new ExclusionListDTO(Integer.parseInt(el)));
		});
		Optional.ofNullable(itemJSON.getDivisionCategory()).filter((a) -> !a.equals("")).ifPresent((dc) -> {
			item.setDivisionCategoryDTO(new DivisionCategoryDTO(Integer.parseInt(dc)));
		});
		item.setBrand(itemJSON.getBrandItem());
		List<ActiveSubstanceDTO> activeSubstances =  new LinkedList<ActiveSubstanceDTO>();
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance()).filter(
				(var pas) -> !pas.isEmpty()).
		ifPresent((var pas) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas), this.substance));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance1()).filter(
				(var pas1) -> !pas1.isEmpty()).
		ifPresent((var pas1) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas1), this.substance1));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance2()).filter(
				(var pas2) -> !pas2.isEmpty()).
		ifPresent((var pas2) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas2), this.substance2));
		});
		item.setActiveSubstances(activeSubstances);
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance(itemJSON.getConcentrationActiveSubtance());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance1()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance1(itemJSON.getConcentrationActiveSubtance1());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance2()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance2(itemJSON.getConcentrationActiveSubtance2());
		});
		item.setAntibiotic(itemJSON.getAntibiotic());
		item.setHighEspeciality(itemJSON.getHighEspeciality());
		item.setBrandItem(itemJSON.getBrandItem());
		item.setMaxPrice(itemJSON.getMaxPrice());
		item.setListCost(itemJSON.getListCost());
		Optional.ofNullable(itemJSON.getFiscalSales()).filter((var fs) -> !fs.isEmpty()).ifPresent((var v) -> {
			item.setFiscalSale(new FiscalClassificationDTO(Integer.parseInt(v)));
		});
		Optional.ofNullable(itemJSON.getFiscalPurchase()).filter((var fp) -> !fp.isEmpty()).ifPresent((var v) -> {
			item.setFiscalPurchase(new FiscalClassificationDTO(Integer.parseInt(v)));
		});
		Optional.ofNullable(itemJSON.getCountry()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setCountryDTO(new CountryDTO(Integer.parseInt(c)));
		});
		Optional.ofNullable(itemJSON.getSalubrityFraction()).filter((a) -> !a.equals("")).ifPresent((sf) -> {
			item.setSalubrityFractionDTO(new SalubrityFractionDTO(Integer.parseInt(sf)));
		});
		Optional.ofNullable(itemJSON.getSatCode()).filter((a) -> !a.equals("")).ifPresent((st) -> {
			item.setSatCodeDTO(new SatCodeDTO(Integer.parseInt(st)));
		});
		return item;
	}
	
	/**
	 * Method in charge of convert a dto to json
	 * @param itemDTO
	 * @return ItemJSON item
	 * @see Integer
	 * @see ItemJSON
	 * @see List
	 * @see ItemDTO
	 * @see String
	 * @see Optional
	 * @see BigDecimal
	 */
	public ItemJSON convertToJSON(final ItemDTO itemDTO) {
		ItemJSON item = new ItemJSON();
		item.setAction("0");
		item.setType(this.replaceNull(itemDTO.getType()));
		Optional.ofNullable(itemDTO.getAnalystCode()).ifPresentOrElse((a) -> item.setAnalystCode(itemDTO.getAnalystCode()), () -> item.setAnalystCode(" "));
		item.setNegociator(" ");
		item.setProvider(" ");
		item.setProvider(itemDTO.getUserProvider().getSap());
		item.setNegociator(itemDTO.getUserNegotiator().getSap());
		Optional.ofNullable(itemDTO.getItemTypeDTO()).ifPresentOrElse((i) -> item.setItemType(i.getSap()), () -> item.setItemType(" "));
		Optional.ofNullable(itemDTO.getDivisionDTO()).ifPresentOrElse((d) -> item.setDivision(d.getSap()), () -> item.setDivision(" "));
		item.setDescription(this.replaceNull(itemDTO.getDescription()));
		Optional.ofNullable(itemDTO.getBrandDTO()).ifPresentOrElse((b) -> item.setBrand(b.getSap()), () -> item.setBrand(" "));
		item.setProduct(this.replaceNull(itemDTO.getProduct()));
		item.setCommercialName(this.replaceNull(itemDTO.getCommercialName()));
		item.setActiveSubstance(this.replaceNull(itemDTO.getActiveSubstance()));
		item.setPresentation(" ");
		item.setFarmaceuticalForm(" ");
		item.setUmNetContent(" ");
		item.setConcentrationUnitMeasure(" ");
		item.setConcentrationUnitMeasure1(" ");
		item.setConcentrationUnitMeasure2(" ");
		item.setUmConcentration(" ");
		itemDTO.getUmcontrations().forEach((um) -> {
			Optional.of(um.getType()).filter((t) -> t.equals(this.presentation)).ifPresent((var p) -> item.setPresentation(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.farmaceuticalForm)).ifPresent((var f) -> item.setFarmaceuticalForm(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.netCont)).ifPresent((var umc) -> item.setUmNetContent(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.unitMesaure)).ifPresent((var cum) -> item.setConcentrationUnitMeasure(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.unitMesaure1)).ifPresent((var cum1) -> item.setConcentrationUnitMeasure1(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.unitMesaure2)).ifPresent((var cum2) -> item.setConcentrationUnitMeasure2(um.getUmConcentrationTypeDTO().getSap()));
			Optional.of(um.getType()).filter((t) -> t.equals(this.concentrationUm)).ifPresent((var umc) -> item.setUmConcentration(um.getUmConcentrationTypeDTO().getSap()));
		});
		item.setNetContent(this.replaceNull(itemDTO.getNetContent()));
		item.setConcentration(this.replaceNull(itemDTO.getConcentration()));
		item.setBrandProvider(this.replaceNull(itemDTO.getBrandProvider()));
		item.setVariable(this.replaceNull(itemDTO.getVariable()));
		item.setUnitBarcode(" ");
		item.setGrossWeight(new BigDecimal(0));
		item.setNetWeight(new BigDecimal(0));
		item.setLongItem(new BigDecimal(0));
		item.setWidthItem(new BigDecimal(0));
		item.setHeightItem(new BigDecimal(0));
		item.setVolumeItem(new BigDecimal(0));
		item.setBoxBarcode(" ");
		item.setBoxGrossWeight(new BigDecimal(0));
		item.setBoxNetWeight(new BigDecimal(0));
		item.setBoxLongItem(new BigDecimal(0));
		item.setBoxWidthItem(new BigDecimal(0));
		item.setBoxHeightItem(new BigDecimal(0));
		item.setBoxVolumeItem(new BigDecimal(0));
		itemDTO.getBarcodes().forEach((b) -> {
			Optional.of(b.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
				item.setUnitBarcode(this.replaceNull(b.getBarcode()));
				item.setGrossWeight(this.replaceZero(b.getGrossWeight()));
				item.setNetWeight(this.replaceZero(b.getNetWeight()));
				item.setLongItem(this.replaceZero(b.getLongitude()));
				item.setWidthItem(this.replaceZero(b.getWidth()));
				item.setHeightItem(this.replaceZero(b.getHeight()));
				item.setVolumeItem(this.replaceZero(b.getVolume()));
			});
			Optional.of(b.getType()).filter((t) -> t.equals(this.corrugated)).ifPresent((var p) -> {
				item.setBoxBarcode(this.replaceNull(b.getBarcode()));
				item.setBoxGrossWeight(this.replaceZero(b.getGrossWeight()));
				item.setBoxNetWeight(this.replaceZero(b.getNetWeight()));
				item.setBoxLongItem(this.replaceZero(b.getLongitude()));
				item.setBoxWidthItem(this.replaceZero(b.getWidth()));
				item.setBoxHeightItem(this.replaceZero(b.getHeight()));
				item.setBoxVolumeItem(this.replaceZero(b.getVolume()));
			});
		});
		item.setPackingUnit(this.replaceZero(itemDTO.getPackingUnit()));
		item.setPrincipalActiveSubstance(" ");
		item.setPrincipalActiveSubstance1(" ");
		item.setPrincipalActiveSubstance2(" ");
		itemDTO.getActiveSubstances().forEach((subs) -> {
			Optional.of(subs.getType()).filter((t) -> t.equals(this.substance)).ifPresent((var as) -> item.setPrincipalActiveSubstance(subs.getActiveSubstanceTypeDTO().getSap()));
			Optional.of(subs.getType()).filter((t) -> t.equals(this.substance1)).ifPresent((var as1) -> item.setPrincipalActiveSubstance1(subs.getActiveSubstanceTypeDTO().getSap()));
			Optional.of(subs.getType()).filter((t) -> t.equals(this.substance2)).ifPresent((var as2) -> item.setPrincipalActiveSubstance2(subs.getActiveSubstanceTypeDTO().getSap()));
		});
		item.setConcentrationActiveSubtance(this.replaceZero(itemDTO.getConcentrationActiveSubtance()));
		item.setConcentrationActiveSubtance1(this.replaceZero(itemDTO.getConcentrationActiveSubtance1()));
		item.setConcentrationActiveSubtance2(this.replaceZero(itemDTO.getConcentrationActiveSubtance2()));
		Optional.ofNullable(itemDTO.getExclusionListDTO()).ifPresentOrElse((el) -> item.setExclusionList(el.getSap()), () -> item.setExclusionList(" "));
		Optional.ofNullable(itemDTO.getDivisionCategoryDTO()).ifPresentOrElse((dc) -> item.setDivisionCategory(dc.getSap()), () -> item.setDivisionCategory(" "));
		item.setBrandItem(this.replaceNull(itemDTO.getBrandItem()));
		Optional.ofNullable(itemDTO.getAntibiotic()).ifPresentOrElse((a) -> item.setAntibiotic(a), () -> item.setAntibiotic(false));
		Optional.ofNullable(itemDTO.getHighEspeciality()).ifPresentOrElse((he) -> item.setHighEspeciality(he), () -> item.setHighEspeciality(false));
		Optional.ofNullable(itemDTO.getSalubrityFractionDTO()).ifPresentOrElse((sf) -> item.setSalubrityFraction(sf.getSap()), () -> item.setSalubrityFraction(" "));
		item.setMaxPrice(this.replaceZero(itemDTO.getMaxPrice()));
		item.setListCost(this.replaceZero(itemDTO.getListCost()));
		item.setFiscalSales(" ");
		item.setFiscalPurchase(" ");
		Optional.ofNullable(itemDTO.getFiscalSale()).ifPresentOrElse((var fs) -> item.setFiscalSales(fs.getSap()), () -> item.setFiscalSales(" "));
		Optional.ofNullable(itemDTO.getFiscalPurchase()).ifPresentOrElse((var fp) -> item.setFiscalPurchase(fp.getSap()), () -> item.setFiscalPurchase(" "));
		Optional.ofNullable(itemDTO.getSatCodeDTO()).ifPresentOrElse((sc) -> item.setSatCode(sc.getSap()), () -> item.setSatCode(" "));
		Optional.ofNullable(itemDTO.getCountryDTO()).ifPresentOrElse((c) -> item.setCountry(c.getSap()), () -> item.setCountry(" "));
		Optional.ofNullable(itemDTO.getProcessStatus()).ifPresentOrElse((ps) -> item.setProcessStatus(ps), () -> item.setProcessStatus(false));
		return item;
	}
	
	public ItemJSON convertToJSON(final Integer id, final ItemDTO itemDTO) {
		ItemJSON item = new ItemJSON();
		item.setId(itemDTO.getId());
		item.setType(itemDTO.getType());
		Optional.ofNullable(itemDTO.getUserProvider()).ifPresent((var u) -> {
			item.setProvider(itemDTO.getUserProvider().getName());
		});
		Optional.ofNullable(itemDTO.getUserNegotiator()).ifPresent((var u) -> {
			item.setNegociator(this.getCompletName(itemDTO.getUserNegotiator().getName(), itemDTO.getUserNegotiator().getSecondName(), 
					itemDTO.getUserNegotiator().getFirstName(), itemDTO.getUserNegotiator().getLastName()));
		});
		Optional.ofNullable(itemDTO.getItemTypeDTO()).ifPresent((var u) -> {
			item.setItemType(itemDTO.getItemTypeDTO().getName());
		});
		Optional.ofNullable(itemDTO.getDivisionDTO()).ifPresent((var u) -> {
			item.setDivision(itemDTO.getDivisionDTO().getName());
		});
		Optional.ofNullable(itemDTO.getBrandDTO()).ifPresent((var u) -> {
			item.setBrand(itemDTO.getBrandDTO().getName());
		});
		Optional.ofNullable(itemDTO.getDivisionCategoryDTO()).ifPresent((var u) -> {
			item.setDivisionCategory(itemDTO.getDivisionCategoryDTO().getName());
		});
		Optional.ofNullable(itemDTO.getCountryDTO()).ifPresent((var u) -> {
			item.setCountry(itemDTO.getCountryDTO().getName());
		});
		Optional.ofNullable(itemDTO.getSatCodeDTO()).ifPresent((var u) -> {
			item.setSatCode(itemDTO.getSatCodeDTO().getName());
		});
		Optional.ofNullable(itemDTO.getExclusionListDTO()).ifPresent((var u) -> {
			item.setExclusionList(itemDTO.getExclusionListDTO().getName());
		});
		Optional.ofNullable(itemDTO.getSalubrityFractionDTO()).ifPresent((var sf) -> {
			item.setSalubrityFraction(sf.getName());
		});
		Optional.ofNullable(itemDTO.getFiscalSale()).ifPresent((var fcs) -> {
			item.setFiscalSales(fcs.getDenomination());
		});
		Optional.ofNullable(itemDTO.getFiscalPurchase()).ifPresent((var fcp) -> {
			item.setFiscalPurchase(fcp.getDenomination());
		});
		item.setDescription(itemDTO.getDescription());
		item.setProduct(itemDTO.getProduct());
		item.setCommercialName(itemDTO.getCommercialName());
		item.setActiveSubstance(itemDTO.getActiveSubstance());
		Optional.ofNullable(itemDTO.getUmcontrations()).ifPresent((var um) -> {
			um.forEach((var u) -> {
				Optional.of(u.getType()).filter((t) -> t.equals(this.presentation)).ifPresent((var p) -> item.setPresentation(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.farmaceuticalForm)).ifPresent((var f) -> item.setFarmaceuticalForm(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.netCont)).ifPresent((var umc) -> item.setUmNetContent(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure)).ifPresent((var cum) -> item.setConcentrationUnitMeasure(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure1)).ifPresent((var cum1) -> item.setConcentrationUnitMeasure1(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure2)).ifPresent((var cum2) -> item.setConcentrationUnitMeasure2(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.concentrationUm)).ifPresent((var umc) -> item.setUmConcentration(u.getUmConcentrationTypeDTO().getUnitMeasured()));
			});
		});
		item.setNetContent(itemDTO.getNetContent());
		item.setConcentration(itemDTO.getConcentration());
		item.setBrandProvider(itemDTO.getBrandProvider());
		item.setVariable(itemDTO.getVariable());
		Optional.ofNullable(itemDTO.getBarcodes()).ifPresent((var b) -> {
			b.forEach((var bc) -> {
				Optional.of(bc.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
					item.setUnitBarcode(this.replaceNull(bc.getBarcode()));
					item.setGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setLongItem(this.replaceZero(bc.getLongitude()));
					item.setWidthItem(this.replaceZero(bc.getWidth()));
					item.setHeightItem(this.replaceZero(bc.getHeight()));
					item.setVolumeItem(this.replaceZero(bc.getVolume()));
				});
				Optional.of(bc.getType()).filter((t) -> t.equals(this.corrugated)).ifPresent((var p) -> {
					item.setBoxBarcode(this.replaceNull(bc.getBarcode()));
					item.setBoxGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setBoxNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setBoxLongItem(this.replaceZero(bc.getLongitude()));
					item.setBoxWidthItem(this.replaceZero(bc.getWidth()));
					item.setBoxHeightItem(this.replaceZero(bc.getHeight()));
					item.setBoxVolumeItem(this.replaceZero(bc.getVolume()));
				});
			});
		});
		item.setPackingUnit(itemDTO.getPackingUnit());
		Optional.ofNullable(itemDTO.getActiveSubstances()).ifPresent((var ac) -> {
			ac.forEach((var subs) -> {
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance)).ifPresent((var as) -> item.setPrincipalActiveSubstance(subs.getActiveSubstanceTypeDTO().getName()));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance1)).ifPresent((var as1) -> item.setPrincipalActiveSubstance1(subs.getActiveSubstanceTypeDTO().getName()));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance2)).ifPresent((var as2) -> item.setPrincipalActiveSubstance2(subs.getActiveSubstanceTypeDTO().getName()));
			});
		});
		item.setConcentrationActiveSubtance(this.replaceZero(itemDTO.getConcentrationActiveSubtance()));
		item.setConcentrationActiveSubtance1(this.replaceZero(itemDTO.getConcentrationActiveSubtance1()));
		item.setConcentrationActiveSubtance2(this.replaceZero(itemDTO.getConcentrationActiveSubtance2()));
		item.setBrandItem(itemDTO.getBrandItem());
		item.setAntibiotic(itemDTO.getAntibiotic());
		item.setHighEspeciality(itemDTO.getHighEspeciality());
		item.setMaxPrice(itemDTO.getMaxPrice());
		item.setListCost(itemDTO.getListCost());
		item.setProcessStatus(itemDTO.getProcessStatus());
		item.setCompleted(itemDTO.getCompleted());
		return item;
	}
	
	public ItemJSON convertToJSON(final DivisionEO division, final ItemDTO itemDTO) {
		ItemJSON item = new ItemJSON();
		item.setId(itemDTO.getId());
		Optional.ofNullable(itemDTO.getType()).filter((var t) -> !t.equals("")).ifPresent((var ti) -> {
			item.setType(ti);
		});
		Optional.ofNullable(itemDTO.getUserProvider()).filter((var p) -> !p.equals("")).ifPresent((var pi) -> {
			item.setProvider(pi.getName());
		});
		Optional.ofNullable(itemDTO.getUserNegotiator()).filter((var n) -> !n.equals("")).ifPresent((var ni) -> {
			item.setNegociator(this.getCompletName(ni.getName(), ni.getSecondName(), ni.getFirstName(), ni.getLastName()));
		});
		Optional.ofNullable(itemDTO.getItemTypeDTO()).filter((var it) -> !it.equals("")).ifPresent((var iti) -> {
			item.setItemType(iti.getName());
		});
		Optional.ofNullable(itemDTO.getDivisionDTO()).filter((var d) -> !d.equals("")).ifPresent((var di) -> {
			item.setDivision(di.getId() + "|" +di.getName());
		});
		Optional.ofNullable(itemDTO.getDescription()).filter((var de) -> !de.equals("")).ifPresent((var dei) -> {
			item.setDescription(dei);
		});
		Optional.ofNullable(itemDTO.getBrandDTO()).filter((var b) -> !b.equals("")).ifPresent((var bi) -> {
			item.setBrand(bi.getName());
		});
		Optional.ofNullable(itemDTO.getProduct()).filter((var p) -> !p.equals("")).ifPresent((var pi) -> {
			item.setProduct(pi);
		});
		Optional.ofNullable(itemDTO.getSalubrityFractionDTO()).filter((var sf) -> !sf.equals("")).ifPresent((var sfi) -> {
			item.setSalubrityFraction(sfi.getName());
		});
		Optional.ofNullable(itemDTO.getCommercialName()).filter((var cn) -> !cn.equals("")).ifPresent((var cni) -> {
			item.setCommercialName(cni);
		});
		Optional.ofNullable(itemDTO.getActiveSubstance()).filter((var as) -> !as.equals("")).ifPresent((var asi) -> {
			item.setActiveSubstance(asi);
		});
		Optional.ofNullable(itemDTO.getNetContent()).filter((var nc) -> !nc.equals("")).ifPresent((var nci) -> {
			item.setNetContent(nci);
		});
		Optional.ofNullable(itemDTO.getConcentration()).filter((var c) -> !c.equals("")).ifPresent((var ci) -> {
			item.setConcentration(ci);
		});
		Optional.ofNullable(itemDTO.getBrandProvider()).filter((var bp) -> !bp.equals("")).ifPresent((var bpi) -> {
			item.setBrandProvider(bpi);
		});
		Optional.ofNullable(itemDTO.getVariable()).filter((var v) -> !v.equals("")).ifPresent((var vi) -> {
			item.setVariable(vi);
		});
		Optional.ofNullable(itemDTO.getPackingUnit()).filter((var pu) -> !pu.equals("")).ifPresent((var pui) -> {
			item.setPackingUnit(pui);
		});
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance()).filter((var ca) -> !ca.equals("")).ifPresent((var cai) -> {
			item.setConcentrationActiveSubtance(this.replaceZero(cai));
		});
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance1()).filter((var ca1) -> !ca1.equals("")).ifPresent((var ca1i) -> {
			item.setConcentrationActiveSubtance1(this.replaceZero(ca1i));
		});
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance2()).filter((var ca2) -> !ca2.equals("")).ifPresent((var ca2i) -> {
			item.setConcentrationActiveSubtance2(this.replaceZero(ca2i));
		});
		Optional.ofNullable(itemDTO.getExclusionListDTO()).filter((var el) -> !el.equals("")).ifPresent((var eli) -> {
			item.setExclusionList(eli.getName());
		});
		Optional.ofNullable(itemDTO.getDivisionCategoryDTO()).filter((var dc) -> !dc.equals("")).ifPresent((var dci) -> {
			item.setDivisionCategory(dci.getName());
		});
		Optional.ofNullable(itemDTO.getBrandItem()).filter((var bi) -> !bi.equals("")).ifPresent((var bii) -> {
			item.setBrandItem(bii);
		});
		Optional.ofNullable(itemDTO.getAntibiotic()).filter((var a) -> !a.equals("")).ifPresent((var ai) -> {
			item.setAntibiotic(ai);
		});
		Optional.ofNullable(itemDTO.getHighEspeciality()).filter((var hs) -> !hs.equals("")).ifPresent((var hsi) -> {
			item.setHighEspeciality(hsi);
		});
		Optional.ofNullable(itemDTO.getMaxPrice()).filter((var mp) -> !mp.equals("")).ifPresent((var mpi) -> {
			item.setMaxPrice(mpi);
		});
		Optional.ofNullable(itemDTO.getListCost()).filter((var lc) -> !lc.equals("")).ifPresent((var lci) -> {
			item.setListCost(lci);
		});
		Optional.ofNullable(itemDTO.getFiscalSale()).filter((var fs) -> !fs.equals("")).ifPresent((var fsi) -> {
			item.setFiscalSales(fsi.getDenomination());
		});
		Optional.ofNullable(itemDTO.getFiscalPurchase()).filter((var fp) -> !fp.equals("")).ifPresent((var fpi) -> {
			item.setFiscalPurchase(fpi.getDenomination());
		});
		Optional.ofNullable(itemDTO.getSatCodeDTO()).filter((var sc) -> !sc.equals("")).ifPresent((var sci) -> {
			item.setSatCode(sci.getName());
		});
		Optional.ofNullable(itemDTO.getCountryDTO()).filter((var c) -> !c.equals("")).ifPresent((var ci) -> {
			item.setCountry(ci.getName());
		});
		Optional.ofNullable(itemDTO.getProcessStatus()).filter((var ps) -> !ps.equals("")).ifPresent((var psi) -> {
			item.setProcessStatus(psi);
		});
		Optional.ofNullable(itemDTO.getUmcontrations()).ifPresent((var um) -> {
			um.forEach((var u) -> {
				Optional.of(u.getType()).filter((t) -> t.equals(this.presentation)).ifPresent((var p) -> item.setPresentation(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.farmaceuticalForm)).ifPresent((var f) -> item.setFarmaceuticalForm(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.netCont)).ifPresent((var umc) -> item.setUmNetContent(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure)).ifPresent((var cum) -> item.setConcentrationUnitMeasure(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure1)).ifPresent((var cum1) -> item.setConcentrationUnitMeasure1(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure2)).ifPresent((var cum2) -> item.setConcentrationUnitMeasure2(u.getUmConcentrationTypeDTO().getUnitMeasured()));
				Optional.of(u.getType()).filter((t) -> t.equals(this.concentrationUm)).ifPresent((var umc) -> item.setUmConcentration(u.getUmConcentrationTypeDTO().getUnitMeasured()));
			});
		});
		Optional.ofNullable(itemDTO.getBarcodes()).ifPresent((var b) -> {
			b.forEach((var bc) -> {
				Optional.of(bc.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
					item.setUnitBarcode(this.replaceNull(bc.getBarcode()));
					item.setGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setLongItem(this.replaceZero(bc.getLongitude()));
					item.setWidthItem(this.replaceZero(bc.getWidth()));
					item.setHeightItem(this.replaceZero(bc.getHeight()));
					item.setVolumeItem(this.replaceZero(bc.getVolume()));
				});
				Optional.of(bc.getType()).filter((t) -> t.equals(this.corrugated)).ifPresent((var p) -> {
					item.setBoxBarcode(this.replaceNull(bc.getBarcode()));
					item.setBoxGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setBoxNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setBoxLongItem(this.replaceZero(bc.getLongitude()));
					item.setBoxWidthItem(this.replaceZero(bc.getWidth()));
					item.setBoxHeightItem(this.replaceZero(bc.getHeight()));
					item.setBoxVolumeItem(this.replaceZero(bc.getVolume()));
				});
			});
		});
		Optional.ofNullable(itemDTO.getActiveSubstances()).ifPresent((var ac) -> {
			ac.forEach((var subs) -> {
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance)).ifPresent((var as) -> item.setPrincipalActiveSubstance(subs.getActiveSubstanceTypeDTO().getName()));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance1)).ifPresent((var as1) -> item.setPrincipalActiveSubstance1(subs.getActiveSubstanceTypeDTO().getName()));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance2)).ifPresent((var as2) -> item.setPrincipalActiveSubstance2(subs.getActiveSubstanceTypeDTO().getName()));
			});
		});
		return item;
	}
	
	public ItemUpdateJSON convertToJSONUpdate(final ItemDTO itemDTO) {
		ItemUpdateJSON item = new ItemUpdateJSON();
		item.setId(itemDTO.getId());
		item.setType(itemDTO.getType());
		Optional.ofNullable(itemDTO.getUserProvider()).ifPresent((var u) -> {
			item.setProvider(String.valueOf(itemDTO.getUserProvider().getId()));
		});
		Optional.ofNullable(itemDTO.getUserProvider()).ifPresent((var u) -> {
			item.setProviderUpdate(new GenericJSON(u.getId(), u.getName()));
		});
		Optional.ofNullable(itemDTO.getUserNegotiator()).ifPresent((var u) -> {
			item.setNegociator(String.valueOf(itemDTO.getUserNegotiator().getId()));
		});
		Optional.ofNullable(itemDTO.getItemTypeDTO()).ifPresent((var u) -> {
			item.setItemType(String.valueOf(itemDTO.getItemTypeDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getDivisionDTO()).ifPresent((var u) -> {
			item.setDivision(String.valueOf(itemDTO.getDivisionDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getBrandDTO()).ifPresent((var u) -> {
			item.setBrand(String.valueOf(itemDTO.getBrandDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getBrandDTO()).ifPresent((var b) -> {
			item.setBrandUpdate(new GenericJSON(b.getId(), b.getName()));
		});
		Optional.ofNullable(itemDTO.getDivisionCategoryDTO()).ifPresent((var u) -> {
			item.setDivisionCategory(String.valueOf(itemDTO.getDivisionCategoryDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getCountryDTO()).ifPresent((var u) -> {
			item.setCountry(String.valueOf(itemDTO.getCountryDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getCountryDTO()).ifPresent((var c) -> {
			item.setCountryUpdate(new GenericJSON(c.getId(), c.getName()));
		});
		Optional.ofNullable(itemDTO.getSatCodeDTO()).ifPresent((var u) -> {
			item.setSatCode(String.valueOf(itemDTO.getSatCodeDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getSatCodeDTO()).ifPresent((var sc) -> {
			item.setSatCodeUpdate(new GenericJSON(sc.getId(), sc.getName()));
		});
		Optional.ofNullable(itemDTO.getExclusionListDTO()).ifPresent((var u) -> {
			item.setExclusionList(String.valueOf(itemDTO.getExclusionListDTO().getId()));
		});
		Optional.ofNullable(itemDTO.getSalubrityFractionDTO()).ifPresent((var sf) -> {
			item.setSalubrityFraction(String.valueOf(sf.getId()));
		});
		Optional.ofNullable(itemDTO.getFiscalSale()).ifPresent((var fcs) -> {
			item.setFiscalSales(String.valueOf(fcs.getId()));
		});
		Optional.ofNullable(itemDTO.getFiscalPurchase()).ifPresent((var fcp) -> {
			item.setFiscalPurchase(String.valueOf(fcp.getId()));
		});
		item.setDescription(itemDTO.getDescription());
		item.setProduct(itemDTO.getProduct());
		item.setCommercialName(itemDTO.getCommercialName());
		item.setActiveSubstance(itemDTO.getActiveSubstance());
		Optional.ofNullable(itemDTO.getUmcontrations()).ifPresent((var um) -> {
			um.forEach((var u) -> {
				Optional.of(u.getType()).filter((t) -> t.equals(this.presentation))
				.ifPresent((var p) -> item.setPresentation(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.farmaceuticalForm))
				.ifPresent((var f) -> item.setFarmaceuticalForm(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.netCont))
				.ifPresent((var umc) -> item.setUmNetContent(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure))
				.ifPresent((var cum) -> item.setConcentrationUnitMeasure(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure1))
				.ifPresent((var cum1) -> item.setConcentrationUnitMeasure1(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.unitMesaure2))
				.ifPresent((var cum2) -> item.setConcentrationUnitMeasure2(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
				Optional.of(u.getType()).filter((t) -> t.equals(this.concentrationUm))
				.ifPresent((var umc) -> item.setUmConcentration(String.valueOf(u.getUmConcentrationTypeDTO().getId())));
			});
		});
		item.setNetContent(itemDTO.getNetContent());
		item.setConcentration(itemDTO.getConcentration());
		item.setBrandProvider(itemDTO.getBrandProvider());
		item.setVariable(itemDTO.getVariable());
		Optional.ofNullable(itemDTO.getBarcodes()).ifPresent((var b) -> {
			b.forEach((var bc) -> {
				Optional.of(bc.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
					item.setUnitBarcode(this.replaceNull(bc.getBarcode()));
					item.setGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setLongItem(this.replaceZero(bc.getLongitude()));
					item.setWidthItem(this.replaceZero(bc.getWidth()));
					item.setHeightItem(this.replaceZero(bc.getHeight()));
					item.setVolumeItem(this.replaceZero(bc.getVolume()));
				});
				Optional.of(bc.getType()).filter((t) -> t.equals(this.corrugated)).ifPresent((var p) -> {
					item.setBoxBarcode(this.replaceNull(bc.getBarcode()));
					item.setBoxGrossWeight(this.replaceZero(bc.getGrossWeight()));
					item.setBoxNetWeight(this.replaceZero(bc.getNetWeight()));
					item.setBoxLongItem(this.replaceZero(bc.getLongitude()));
					item.setBoxWidthItem(this.replaceZero(bc.getWidth()));
					item.setBoxHeightItem(this.replaceZero(bc.getHeight()));
					item.setBoxVolumeItem(this.replaceZero(bc.getVolume()));
				});
			});
		});
		item.setPackingUnit(itemDTO.getPackingUnit());
		Optional.ofNullable(itemDTO.getActiveSubstances()).ifPresent((var ac) -> {
			ac.forEach((var subs) -> {
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance))
				.ifPresent((var as) -> item.setPrincipalActiveSubstance(String.valueOf(subs.getActiveSubstanceTypeDTO().getId())));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance))
				.ifPresent((var as) -> item.setPrincipalActiveSubstanceUpdate(new GenericJSON(subs.getActiveSubstanceTypeDTO().getId(), subs.getActiveSubstanceTypeDTO().getName())));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance1))
				.ifPresent((var as1) -> item.setPrincipalActiveSubstance1(String.valueOf(subs.getActiveSubstanceTypeDTO().getId())));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance1))
				.ifPresent((var as) -> item.setPrincipalActiveSubstance1Update(new GenericJSON(subs.getActiveSubstanceTypeDTO().getId(), subs.getActiveSubstanceTypeDTO().getName())));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance2))
				.ifPresent((var as2) -> item.setPrincipalActiveSubstance2(String.valueOf(subs.getActiveSubstanceTypeDTO().getId())));
				Optional.of(subs.getType()).filter((t) -> t.equals(this.substance2))
				.ifPresent((var as) -> item.setPrincipalActiveSubstance2Update(new GenericJSON(subs.getActiveSubstanceTypeDTO().getId(), subs.getActiveSubstanceTypeDTO().getName())));
			});
		});
		//item.setConcentrationActiveSubtance(new BigDecimal(0));
		//item.setConcentrationActiveSubtance1(new BigDecimal(0));
		//item.setConcentrationActiveSubtance2(new BigDecimal(0));
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance()).ifPresent((var u) -> {
			item.setDivision(String.valueOf(itemDTO.getDivisionDTO().getId()));
			item.setConcentrationActiveSubtance(u);
		});
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance1()).ifPresent((var u1) -> {
			item.setDivision(String.valueOf(itemDTO.getDivisionDTO().getId()));
			item.setConcentrationActiveSubtance1(u1);
		});
		Optional.ofNullable(itemDTO.getConcentrationActiveSubtance2()).ifPresent((var u2) -> {
			item.setDivision(String.valueOf(itemDTO.getDivisionDTO().getId()));
			item.setConcentrationActiveSubtance2(u2);
		});
		item.setBrandItem(itemDTO.getBrandItem());
		item.setAntibiotic(itemDTO.getAntibiotic());
		item.setHighEspeciality(itemDTO.getHighEspeciality());
		item.setMaxPrice(itemDTO.getMaxPrice());
		item.setListCost(itemDTO.getListCost());
		item.setProcessStatus(itemDTO.getProcessStatus());
		return item;
	}
	
	public ItemDTO convertToDTO(final ItemUpdateJSON itemJSON) {
		ItemDTO item = new ItemDTO();
		item.setId(itemJSON.getId());
		Optional<String> type = Optional.ofNullable(itemJSON.getType());
		type.filter((var t) -> !t.equals("")).ifPresent((var ti) -> {
			item.setType(ti);
		});
		type.filter((var t) -> t.equals("")).ifPresent((var ti) -> {
			item.setType(null);
		});
		Optional<String> division = Optional.ofNullable(itemJSON.getDivision());
		division.filter((var d) -> !d.equals("")).ifPresent((var di) -> {
			item.setDivisionDTO(new DivisionDTO(Integer.parseInt(di)));
		});
		division.filter((var d) -> d.equals("")).ifPresent((var di) -> {
			item.setDivisionDTO(new DivisionDTO());
		});
		Optional<String> itemType = Optional.ofNullable(itemJSON.getItemType());
		itemType.filter((var it) -> !it.equals("")).ifPresent((var iti) -> {
			item.setItemTypeDTO(new ItemTypeDTO(Integer.parseInt(iti)));
		});
		itemType.filter((var it) -> it.equals("")).ifPresent((var iti) -> {
			item.setItemTypeDTO(new ItemTypeDTO());
		});
		Optional<String> brand = Optional.ofNullable(itemJSON.getBrand());
		brand.filter((var b) -> !b.equals("")).ifPresent((var bi) -> {
			item.setBrandDTO(new BrandDTO(Integer.parseInt(bi)));
		});
		brand.filter((var b) -> b.equals("")).ifPresent((var bi) -> {
			item.setBrandDTO(new BrandDTO());
		});
		Optional<String> negotiator = Optional.ofNullable(itemJSON.getNegociator());
		negotiator.filter((var n) -> !n.equals("")).ifPresent((var ni) -> {
			item.setUserNegotiator(new UserDTO(Integer.parseInt(ni)));
		});
		negotiator.filter((var n) -> n.equals("")).ifPresent((var ni) -> {
			item.setUserNegotiator(new UserDTO());
		});
		Optional<String> provider = Optional.ofNullable(itemJSON.getProvider());
		provider.filter((var p) -> !p.equals("")).ifPresent((var pi) -> {
			item.setUserProvider(new UserDTO(Integer.parseInt(pi)));
		});
		provider.filter((var p) -> p.equals("")).ifPresent((var pi) -> {
			item.setUserProvider(new UserDTO());
		});
		Optional<String> divisionCategory = Optional.ofNullable(itemJSON.getDivisionCategory());
		divisionCategory.filter((var dc) -> !dc.equals("")).ifPresent((var dci) -> {
			item.setDivisionCategoryDTO(new DivisionCategoryDTO(Integer.parseInt(dci)));
		});
		divisionCategory.filter((var dc) -> dc.equals("")).ifPresent((var dci) -> {
			item.setDivisionCategoryDTO(new DivisionCategoryDTO());
		});
		Optional<String> country = Optional.ofNullable(itemJSON.getCountry());
		country.filter((var c) -> !c.equals("")).ifPresent((var ci) -> {
			item.setCountryDTO(new CountryDTO(Integer.parseInt(ci)));
		});
		country.filter((var p) -> p.equals("")).ifPresent((var pi) -> {
			item.setCountryDTO(new CountryDTO());
		});
		Optional<String> satCode = Optional.ofNullable(itemJSON.getSatCode());
		satCode.filter((var sc) -> !sc.equals("")).ifPresent((var sci) -> {
			item.setSatCodeDTO(new SatCodeDTO(Integer.parseInt(sci)));
		});
		satCode.filter((var sc) -> sc.equals("")).ifPresent((var sci) -> {
			item.setSatCodeDTO(new SatCodeDTO());
		});
		Optional<String> exclusionList = Optional.ofNullable(itemJSON.getExclusionList());
		exclusionList.filter((var el) -> !el.equals("")).ifPresent((var eli) -> {
			item.setExclusionListDTO(new ExclusionListDTO(Integer.parseInt(eli)));
		});
		exclusionList.filter((var el) -> el.equals("")).ifPresent((var eli) -> {
			item.setExclusionListDTO(new ExclusionListDTO());
		});
		Optional<String> salubtityFraction = Optional.ofNullable(itemJSON.getSalubrityFraction());
		salubtityFraction.filter((var sf) -> !sf.equals("")).ifPresent((var sfi) -> {
			item.setSalubrityFractionDTO(new SalubrityFractionDTO(Integer.parseInt(sfi)));
		});
		salubtityFraction.filter((var sf) -> sf.equals("")).ifPresent((var sfi) -> {
			item.setSalubrityFractionDTO(new SalubrityFractionDTO());
		});
		Optional<String> sales = Optional.ofNullable(itemJSON.getFiscalSales());
		sales.filter((var fcs) -> !fcs.equals("")).ifPresent((var fcsi) -> {
			item.setFiscalSale(new FiscalClassificationDTO(Integer.parseInt(fcsi)));
		});
		sales.filter((var fcs) -> fcs.equals("")).ifPresent((var sfsi) -> {
			item.setFiscalSale(new FiscalClassificationDTO());
		});
		Optional<String> purchase = Optional.ofNullable(itemJSON.getFiscalPurchase());
		purchase.filter((var fcp) -> !fcp.equals("")).ifPresent((var fcpi) -> {
			item.setFiscalPurchase(new FiscalClassificationDTO(Integer.parseInt(fcpi)));
		});
		purchase.filter((var fcp) -> fcp.equals("")).ifPresent((var sfpi) -> {
			item.setFiscalPurchase(new FiscalClassificationDTO());
		});
		item.setProduct(itemJSON.getProduct());
		item.setActiveSubstance(itemJSON.getActiveSubstance());
		item.setCommercialName(itemJSON.getCommercialName());
		List<UMConcentrationDTO> umcontrations = new LinkedList<UMConcentrationDTO>();
		Optional.ofNullable(itemJSON.getPresentation()).filter(
				(var p) -> !p.isEmpty()).
		ifPresent((var p) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(p), this.presentation));
		});
		Optional.ofNullable(itemJSON.getFarmaceuticalForm()).filter(
				(var f) -> !f.isEmpty()).
		ifPresent((var f) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(f), this.farmaceuticalForm));
		});
		Optional.ofNullable(itemJSON.getUmNetContent()).filter(
				(var u) -> !u.isEmpty()).
		ifPresent((var u) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(u), this.netCont));
		});
		Optional.ofNullable(itemJSON.getUmConcentration()).filter(
				(var c) -> !c.isEmpty()).
		ifPresent((var c) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(c), this.concentrationUm));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure()).filter(
				(var cum) -> !cum.isEmpty()).
		ifPresent((var cum) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum), this.unitMesaure));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure1()).filter(
				(var cum1) -> !cum1.isEmpty()).
		ifPresent((var cum1) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum1), this.unitMesaure1));
		});
		Optional.ofNullable(itemJSON.getConcentrationUnitMeasure2()).filter(
				(var cum2) -> !cum2.isEmpty()).
		ifPresent((var cum2) -> {
			umcontrations.add(new UMConcentrationDTO(Integer.parseInt(cum2), this.unitMesaure2));
		});
		item.setUmcontrations(umcontrations);
		item.setNetContent(itemJSON.getNetContent());
		item.setConcentration(itemJSON.getConcentration());
		item.setBrandProvider(itemJSON.getBrandProvider());
		item.setVariable(itemJSON.getVariable());
		List<BarcodeDTO> barcodes = new LinkedList<BarcodeDTO>();
		Optional.ofNullable(itemJSON.getUnitBarcode()).filter(
				(var ub) -> !ub.isEmpty()).
		ifPresent((var ub) -> {
			BarcodeDTO unit = new BarcodeDTO();
			unit.setBarcode(ub);
			unit.setType(this.unit);
			unit.setGrossWeight(itemJSON.getGrossWeight());
			unit.setNetWeight(itemJSON.getNetWeight());
			unit.setLongitude(itemJSON.getLongItem());
			unit.setWidth(itemJSON.getWidthItem());
			unit.setHeight(itemJSON.getHeightItem());
			unit.setVolume(itemJSON.getVolumeItem());
			barcodes.add(unit);
		});
		Optional.ofNullable(itemJSON.getBoxBarcode()).filter(
				(var bb) -> !bb.isEmpty()).
		ifPresent((var bb) -> {
			BarcodeDTO box = new BarcodeDTO();
			box.setBarcode(bb);
			box.setType(this.corrugated);
			box.setGrossWeight(itemJSON.getBoxGrossWeight());
			box.setNetWeight(itemJSON.getBoxNetWeight());
			box.setLongitude(itemJSON.getBoxLongItem());
			box.setWidth(itemJSON.getBoxWidthItem());
			box.setHeight(itemJSON.getBoxHeightItem());
			box.setVolume(itemJSON.getBoxVolumeItem());
			barcodes.add(box);
		});
		item.setBarcodes(barcodes);
		item.setPackingUnit(itemJSON.getPackingUnit());
		item.setBrand(itemJSON.getBrandItem());
		List<ActiveSubstanceDTO> activeSubstances =  new LinkedList<ActiveSubstanceDTO>();
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance()).filter(
				(var pas) -> !pas.isEmpty()).
		ifPresent((var pas) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas), this.substance));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance1()).filter(
				(var pas1) -> !pas1.isEmpty()).
		ifPresent((var pas1) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas1), this.substance1));
		});
		Optional.ofNullable(itemJSON.getPrincipalActiveSubstance2()).filter(
				(var pas2) -> !pas2.isEmpty()).
		ifPresent((var pas2) -> {
			activeSubstances.add(new ActiveSubstanceDTO(Integer.parseInt(pas2), this.substance2));
		});
		item.setActiveSubstances(activeSubstances);
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance(itemJSON.getConcentrationActiveSubtance());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance1()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance1(itemJSON.getConcentrationActiveSubtance1());
		});
		Optional.ofNullable(itemJSON.getConcentrationActiveSubtance2()).filter((a) -> !a.equals("")).ifPresent((c) -> {
			item.setConcentrationActiveSubtance2(itemJSON.getConcentrationActiveSubtance2());
		});
		item.setAntibiotic(itemJSON.getAntibiotic());
		item.setHighEspeciality(itemJSON.getHighEspeciality());
		item.setBrandItem(itemJSON.getBrandItem());
		item.setMaxPrice(itemJSON.getMaxPrice());
		item.setListCost(itemJSON.getListCost());
		return item;
	}
	
	private String getCompletName(
			final String name, 
			final String secondName, 
			final String firstName, 
			final String LastName) {
		StringBuilder sb = new StringBuilder();
		Optional.ofNullable(name).ifPresent((var v) -> sb.append(v + " "));
		Optional.ofNullable(secondName).ifPresent((var v) -> sb.append(v + " "));
		Optional.ofNullable(firstName).ifPresent((var v) -> sb.append(v + " "));
		Optional.ofNullable(LastName).ifPresent((var v) -> sb.append(v + " "));
		return sb.toString().trim();
	}
	
	private String replaceNull(
			final String param) {
		String result = null;
		Optional<String> optional = Optional.ofNullable(param);
		if(optional.isPresent()) {
			String value = param.trim();
			if(value.isEmpty() || value.equals(null)) {
				result = " ";
			} else {
				result = value;
			}
		} else {
			result = " ";
		}
		return result;
	}
	
	private BigDecimal replaceZero(BigDecimal param) {
		BigDecimal result = null;
		Optional<BigDecimal> optional = Optional.ofNullable(param);
		if(optional.isPresent()) {
			if(null == param || param.equals(null)) {
				result = new BigDecimal(0);
			} else {
				result = param;
			}
		} else {
			result = new BigDecimal(0);
		}
		return result;
	}
	
	public List<ItemJSON> getItems(
			final List<ItemEO> list,
			final String uuid) {
		this.items = null;
		this.items = new LinkedList<ItemJSON>();
		if (list.size() > 0) {
			list.forEach(
					(i) -> {
						this.items.add(this.convertToJSON(i.getId(), new ItemDTO(i)));
					});
		}
		return this.items;
	}

}