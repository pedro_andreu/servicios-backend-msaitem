/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.aitem.util;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fsp.commonutil.dto.LogDTO;
import com.fsp.commonutil.helper.ExceptionHelper;

import lombok.extern.log4j.Log4j;

/**
 * Utility class responsible for provide the necessary utilities to update connect with aws
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see LogDTO
 * @see String  
 */
@Log4j
@Component("awsUtil")
public class AWSUtil {
	
	private LogDTO logDto;
	
	@Value("${aws.s3.bucket}")
	private String bucket;
	
	@Value("${aws.s3.separator}")
	private String separator;

	/**
	 * Method in charge of uploading a file to a S3 Bucket on AWS
	 * @param count
	 * @param file
	 * @param name
	 * @param extension
	 * @param content
	 * @param barcode
	 * @param uuid
	 * @return String result
	 * @see Integer
	 * @see File
	 * @see String
	 * @see Regions
	 * @see AmazonS3
	 * @see PutObjectRequest
	 * @see ObjectMetadata
	 * @see URL
	 * @see LogDTO
	 * @see SimpleDateFormat
	 * @see Date
	 * @see HttpStatus
	 * @see ExceptionHelper
	 * @throws AmazonServiceException
	 * @throws SdkClientException
	 */
	public String upload(
			int count,
			final File file, 
			final String name, 
			final String extension, 
			final String content,
			final String barcode,
			final String uuid) {
		String result = null;
		Regions clientRegion = Regions.US_EAST_1;
        final String bucketName = this.bucket;
        //String fileObjKeyName = barcode + this.separator + name + extension;
        String fileObjKeyName = "QA/" + barcode + this.separator + name + extension;
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).build();
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, file);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(content);
            request.setMetadata(metadata);
            s3Client.putObject(request);
            URL s3 = s3Client.getUrl(bucketName, fileObjKeyName);
            result = s3.toString();
        } catch (AmazonServiceException e) {
        	log.error("AmazonServiceException");
        	this.logDto = new LogDTO();
    		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
    		.uuid(uuid)
    		.httpCode(HttpStatus.BAD_REQUEST.name())
    		.errorCode(HttpStatus.BAD_REQUEST.value())
    		.message(e.getMessage())
    		.trace(ExceptionHelper.convertStackTraceToString(e));
    		log.error(this.logDto.toString());
        } catch (SdkClientException e) {
        	log.error("SdkClientException");
        	this.logDto = new LogDTO();
    		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
    		.uuid(uuid)
    		.httpCode(HttpStatus.BAD_REQUEST.name())
    		.errorCode(HttpStatus.BAD_REQUEST.value())
    		.message(e.getMessage())
    		.trace(ExceptionHelper.convertStackTraceToString(e));
    		log.error(this.logDto.toString());
        	int i = count + 1;
        	log.error("retry: " + i);
        	if(i <= 2) {
        		this.upload(i, file, name, extension, content, barcode, uuid);
        	}
        }
        return result;
	}
	
	/**
	 * Method in charge of deleting a file to a S3 Bucket on AWS
	 * @param count
	 * @param file
	 * @param name
	 * @param extension
	 * @param content
	 * @param barcode
	 * @param uuid
	 * @return Boolean result
	 * @see Integer
	 * @see File
	 * @see String
	 * @see Regions
	 * @see AmazonS3
	 * @see PutObjectRequest
	 * @see ObjectMetadata
	 * @see URL
	 * @see LogDTO
	 * @see SimpleDateFormat
	 * @see Date
	 * @see HttpStatus
	 * @see ExceptionHelper
	 * @throws AmazonServiceException
	 * @throws SdkClientException
	 */
	public boolean deleteObject(
			final String name, 
			final String content,
			final String barcode,
			final String uuid) {
		boolean result = false;
		try {
			final String bucketName = this.bucket;
			Regions clientRegion = Regions.US_EAST_1;
	        String fileObjKeyName = content + this.separator + name;
			AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion).build();
			s3Client.deleteObject(new DeleteObjectRequest(bucketName, fileObjKeyName));
			result = true;
		} catch (AmazonServiceException e) {
			log.error("AmazonServiceException");
        	this.logDto = new LogDTO();
    		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
    		.uuid(uuid)
    		.httpCode(HttpStatus.BAD_REQUEST.name())
    		.errorCode(HttpStatus.BAD_REQUEST.value())
    		.message(e.getMessage())
    		.trace(ExceptionHelper.convertStackTraceToString(e));
    		log.error(this.logDto.toString());
        } catch (SdkClientException e) {
        	log.error("SdkClientException");
        	this.logDto = new LogDTO();
    		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
    		.uuid(uuid)
    		.httpCode(HttpStatus.BAD_REQUEST.name())
    		.errorCode(HttpStatus.BAD_REQUEST.value())
    		.message(e.getMessage())
    		.trace(ExceptionHelper.convertStackTraceToString(e));
    		log.error(this.logDto.toString());
        }
		return result;
	}

}