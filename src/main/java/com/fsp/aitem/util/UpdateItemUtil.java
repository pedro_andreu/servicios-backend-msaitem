package com.fsp.aitem.util;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.fsp.aitem.dto.ItemDTO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.aitem.repository.UpdateItemRepository;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

@Component("updateItemUtil")
public class UpdateItemUtil {
	
	private Boolean result = false;
	
	public Boolean deleteRelationsItem(
			final ItemRepository itemRepository, final ItemDTO itemDTO, 
			final UpdateItemRepository updateItemRepository, final String uuid) {
		Optional<List<ItemEO>> optional = Optional.of(itemRepository.searchId(itemDTO.getId()));
		optional.ifPresentOrElse((var l) -> {
			if (l.size() > 0) {
				l.forEach(
						(var i) -> {
							if(i.getConcentrations().size() > 0) {
								i.getConcentrations().forEach((var c) -> {
									Optional<Integer> concentration = Optional.of(updateItemRepository.deleteUMConcentration(c.getId(), i.getId()));
									concentration.filter((var um) -> um >= 1).ifPresent((var v) -> this.result = true);
									concentration.filter((var um) -> um == 0).ifPresent((var v) -> {
										throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
										});
								});
							}
							if(i.getActiveSubstances().size() > 0) {
								i.getActiveSubstances().forEach((var a) -> {
									Optional<Integer> substance = Optional.of(updateItemRepository.deleteActiveSubstance(a.getId(), i.getId()));
									substance.filter((var um) -> um >= 1).ifPresent((var v) -> this.result = true);
									substance.filter((var um) -> um == 0).ifPresent((var v) -> {
										throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
										});
								});
							}
							if(i.getBarcodes().size() > 0) {
								i.getBarcodes().forEach((var b) -> {
									Optional<Integer> barcodes = Optional.of(updateItemRepository.deleteBarcode(b.getId(), i.getId()));
									barcodes.filter((var um) -> um >= 1).ifPresent((var v) -> this.result = true);
									barcodes.filter((var um) -> um == 0).ifPresent((var v) -> {
										throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
										});
								});
							}
							this.result = true;
						});
			} else {
				throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
			}
		}, () -> {
			throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
		});
		return this.result;
	}
	
	public Boolean deleteRelationsItem(
			final List<ItemEO> items,
			final UpdateItemRepository updateItemRepository,
			final Integer id,
			final String uuid) {
		if (items.size() > 0) {
			items.stream().forEach(
					(i) -> {
						if(updateItemRepository.deleteItem(id) > 0) {
							Optional.ofNullable(i.getBarcodes())
							.filter((var bc) -> bc.size() > 0)
							.ifPresent((var bci) -> {
								bci.forEach((var b) -> {
									updateItemRepository.deleteItemBarcode(b.getId(), id);
								});
							});
							Optional.ofNullable(i.getActiveSubstances())
							.filter((var as) -> as.size() > 0)
							.ifPresent((var asi) -> {
								asi.forEach((var a) -> {
									updateItemRepository.deleteItemActiveSubstance(a.getId(), id);
								});
							});
							Optional.ofNullable(i.getConcentrations())
							.filter((var c) -> c.size() > 0)
							.ifPresent((var cum) -> {
								cum.forEach((var um) -> {
									updateItemRepository.deleteItemUMConcentration(um.getId(), id);
								});
							});
						} else {
							throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
						}
					});
			this.result = true;
		} else {
			throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
		}
		return this.result;
	}
	
	public Boolean createRelationsItem(
			final ItemEO itemEO, 
			final UpdateItemRepository updateItemRepository,
			final String uuid) {
		Optional.ofNullable(itemEO.getActiveSubstances())
		.filter((var as) -> as.size() > 0).ifPresent((var asi) -> {
			asi.forEach((var a) -> {
				Optional.of(updateItemRepository.createActiveSubstance(itemEO.getId(), a.getActiveSubstanceTypeEO().getId(), a.getType()))
				.ifPresentOrElse((var i) -> {
					this.result = true;
				}, () -> {
					throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
				});
			});
		});
		Optional.ofNullable(itemEO.getConcentrations())
		.filter((var c) -> c.size() > 0).ifPresent((var ci) -> {
			ci.forEach((var um) -> {
				Optional.of(updateItemRepository.createUMConcentration(itemEO.getId(), um.getUmConcentrationTypeEO().getId(), um.getType()))
				.ifPresentOrElse((var umi) -> {
					this.result = true;
				}, () -> {
					throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
				});
			});
		});
		Optional.ofNullable(itemEO.getBarcodes())
		.filter((var b) -> b.size() > 0).ifPresent((var bc) -> {
			bc.forEach((var bci) -> {
				Optional.of(updateItemRepository.createBarcode(itemEO.getId(), bci.getType(), bci.getBarcode(), bci.getGrossWeight(), 
						bci.getNetWeight(), bci.getLongitude(), bci.getHeight(), bci.getWidth(), bci.getVolume()))
				.ifPresentOrElse((var bi) -> {
					this.result = true;
				}, () -> {
					throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
				});
			});
		});
		return this.result;
	}

}