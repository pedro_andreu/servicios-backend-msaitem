/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.aitem.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Utility class responsible for provide the necessary utilities to treat a document
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see LogDTO
 * @see String  
 */
@Component("documentUtil")
public class DocumentUtil {

	/**
	 * Method in charge of get the extension of a document
	 * @param file
	 * @return String extension
	 * @see String
	 * @see MultipartFile
	 */
	public String getFileExtension(final MultipartFile file) {
	    String orgName = file.getOriginalFilename();
	    String[] parts = orgName.split("\\.");
	    String extension = parts[parts.length-1];
	    return extension;
	}
	
	/**
	 * Method in charge of create a new document
	 * @param multipartFile
	 * @return File new file
	 * @see File
	 * @see MultipartFile
	 * @see FileOutputStream
	 * @throws IOException  
	 */
	public File createFile(
			final MultipartFile multipartFile
			) {
		FileOutputStream fos = null;
		File file = null;
		try {
			file = File.createTempFile("temp", multipartFile.getOriginalFilename());
			file.createNewFile();
			fos = new FileOutputStream(file);
			fos.write(multipartFile.getBytes());
			fos.flush();
		} catch (IOException ex) {
			//throw new DBAccessException(ex, uuid);
		} finally {
			try {
				fos.close();
			} catch (IOException ex) {
				//throw new DBAccessException(ex, uuid);
			}
		}
		return file;
	}
	
}