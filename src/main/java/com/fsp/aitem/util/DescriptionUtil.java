/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.aitem.util;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fsp.aitem.dto.DivisionDTO;
import com.fsp.aitem.dto.UMConcentrationDTO;
import com.fsp.aitem.repository.DivisionRepository;
import com.fsp.aitem.repository.UMConcentrationRepository;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Utility class responsible for provide the necessary utilities to description creation
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see DivisionDTO
 * @see String  
 * @see Integer  
 */
@Component("descriptionUtil")
public class DescriptionUtil {
	
	@Autowired
	@Qualifier("divisionDTO")
	private DivisionDTO divisionDTO;
	
	private String result;
	private Integer id;
	
	public static final String C = "C/";
	
	@Value("${division.medicament}")
	private String medicament;
	@Value("${division.consummation}")
	private String consummation;
	@Value("${division.generic.consummation}")
	private String genericConsummation;
	@Value("${division.generic.medicament}")
	private String genericMedicament;
	@Value("${division.hygiene}")
	private String hygiene;
	
	@Value("${um.concentration.presentation}")
	private String presentation;
	@Value("${um.concentration.farmaceutical.form}")
	private String farmaceuticalForm;
	@Value("${um.concentration.net.cont}")
	private String netCont;
	@Value("${um.concentration.concentration}")
	private String concentrationUm;
	
	/**
	 * Method in charge of create the division format
	 * @param division
	 * @param umConcentrationRepository
	 * @param concentrations
	 * @param commercialName
	 * @param netContent
	 * @param concentration
	 * @param brandProvider
	 * @param product
	 * @param activeSustance
	 * @param variable
	 * @param brandItem
	 * @param uuid
	 * @return String description
	 * @see String
	 * @see UMConcentrationRepository
	 * @see List
	 * @see UMConcentrationDTO
	 * @see StringBuilder
	 * @see Optional
	 */
	public String createDescriptionItem(final String division, final UMConcentrationRepository umConcentrationRepository,
			final List<UMConcentrationDTO> concentrations, final String commercialName, final String netContent,
			final String concentration, final String brandProvider, final String product, final String activeSustance,
			final String variable, final String brandItem, final String uuid
			) {
		StringBuilder sb = new StringBuilder();
		Optional<String> optional = Optional.ofNullable(division);
		optional.filter((var d) -> d.isEmpty()).ifPresent((var r) -> {
			sb.append(new String());
			});
		optional.filter((var v) -> v.equals(this.medicament)).ifPresent((var r) -> {
			sb.append(this.isValidString(commercialName) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.farmaceuticalForm), uuid));
			sb.append(this.isValidString(concentration) + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.concentrationUm), uuid));
			sb.append(this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.presentation), uuid));
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.netCont), uuid));
		});
		optional.filter((var v) -> v.equals(this.consummation)).ifPresent((var r) -> {
			sb.append(this.isValidString(brandItem) + " ");
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");	
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.netCont), uuid));
		});
		optional.filter((var v) -> v.equals(this.genericMedicament)).ifPresent((var r) -> {
			sb.append(this.isValidString(activeSustance) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.farmaceuticalForm), uuid));
			sb.append(this.isValidString(concentration).trim() + "" + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.concentrationUm), uuid));
			sb.append(C + this.isValidString(netContent));
			sb.append(this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.netCont), uuid));
			sb.append(this.isValidString(brandProvider));
		});
		optional.filter((var v) -> v.equals(this.genericConsummation)).ifPresent((var r) -> {
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.netCont), uuid));
			sb.append(this.isValidString(brandProvider));
		});
		optional.filter((var v) -> v.equals(this.hygiene)).ifPresent((var r) -> {
			sb.append(this.isValidString(brandItem) + " ");
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.presentation), uuid));
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, this.getIdConcentrationType(concentrations, this.netCont), uuid));
		});
		return sb.toString().trim();
	}
	
	/**
	 * Method in charge of validate the description format
	 * @param presentation
	 * @param umNetContent
	 * @param farmaceuticalForm
	 * @param umConcentration
	 * @param division
	 * @param umConcentrationRepository
	 * @param commercialName
	 * @param netContent
	 * @param concentration
	 * @param brandProvider
	 * @param product
	 * @param activeSustance
	 * @param variable
	 * @param brandItem
	 * @param uuid
	 * @return String description
	 * @see String 
	 * @see Integer
	 * @see UMConcentrationRepository
	 * @see StringBuilder 
	 * @see Optional 
	 */
	public String isValidDescriptionItem(final Integer presentation, final Integer umNetContent, final Integer farmaceuticalForm, 
			final Integer umConcentration, final String division, final UMConcentrationRepository umConcentrationRepository,
			final String commercialName, final String netContent, final String concentration, final String brandProvider, 
			final String product, final String activeSustance, final String variable, final String brandItem, final String uuid
			) {
		StringBuilder sb = new StringBuilder();
		Optional<String> optional = Optional.ofNullable(division);
		optional.filter((var d) -> d.isEmpty()).ifPresent((var r) -> {
			sb.append(new String());
			});
		optional.filter((var v) -> v.equals(this.medicament)).ifPresent((var r) -> {
			sb.append(this.isValidString(commercialName) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, farmaceuticalForm, uuid));
			sb.append(this.isValidString(concentration) + this.getSapUM(umConcentrationRepository, umConcentration, uuid));
            sb.append(this.getSapUM(umConcentrationRepository, presentation, uuid));
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, umNetContent, uuid));			
		});
		optional.filter((var v) -> v.equals(this.consummation)).ifPresent((var r) -> {
			sb.append(this.isValidString(brandItem) + " ");
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");	
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, umNetContent, uuid));
		});
		optional.filter((var v) -> v.equals(this.genericMedicament)).ifPresent((var r) -> {
			sb.append(this.isValidString(activeSustance) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, farmaceuticalForm, uuid) + this.isValidString(concentration).trim() + "" + this.getSapUM(umConcentrationRepository, umConcentration, uuid));
			sb.append(C + this.isValidString(netContent));
			sb.append(this.getSapUM(umConcentrationRepository, umNetContent, uuid));
			sb.append(this.isValidString(brandProvider));
		});
		optional.filter((var v) -> v.equals(this.genericConsummation)).ifPresent((var r) -> {
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");
			sb.append(C + this.isValidString(netContent));
			sb.append(this.getSapUM(umConcentrationRepository, umNetContent, uuid));
			sb.append(this.isValidString(brandProvider));
		});
		optional.filter((var v) -> v.equals(this.hygiene)).ifPresent((var r) -> {
			sb.append(this.isValidString(brandItem) + " ");
			sb.append(this.isValidString(product) + " ");
			sb.append(this.isValidString(variable) + " ");
			sb.append(this.getSapUM(umConcentrationRepository, presentation, uuid));
			sb.append(C + this.isValidString(netContent) + this.getSapUM(umConcentrationRepository, umNetContent, uuid));
		});
		return sb.toString().trim();
	}
	
	/**
	 * Method in charge of search a division by its id
	 * @param divisionRepository
	 * @param id
	 * @param uuid
	 * @return DivisionDTO divisionDTO
	 * @see DivisionDTO
	 * @see DivisionRepository
	 * @see Integer
	 * @see String
	 * @see Optional
	 * @see List
	 * @see Object
	 */
	public DivisionDTO getDivision(final DivisionRepository divisionRepository, final Integer id, final String uuid) {
		Optional<List<Object[]>> optional = Optional.of(divisionRepository.searchById(id));
		optional.filter((var r) -> r.size() > 0).ifPresent((var l) -> {
			l.stream().forEach(
					(var d) -> {
						this.divisionDTO = new DivisionDTO(Integer.parseInt(String.valueOf(d[0])),String.valueOf(d[1]));
					});
		});
		optional.filter((var r) -> r.size() == 0).ifPresent((var l) -> {
			this.divisionDTO = new DivisionDTO();
		});
		return this.divisionDTO;
	}
	
	/**
	 * Method in charge of search for a unit of measure by its id
	 * @param concentrations
	 * @param value
	 * @return Integer id
	 * @see Integer
	 * @see List
	 * @see UMConcentrationDTO
	 * @see String
	 * @see Optional
	 */
	public Integer getIdConcentrationType(final List<UMConcentrationDTO> concentrations, final String value) {
		this.id = null;
		concentrations.forEach((um) -> {
			Optional.ofNullable(um.getType())
			  .filter((t) -> t.equals(value))
			  .ifPresent((var p) -> {
				  this.id = um.getUmConcentrationTypeDTO().getId();
			  });
		});
		return this.id;
	}
	
	/**
	 * Method in charge of search for a unit of measure by its id
	 * @param umConcentrationRepository
	 * @param id
	 * @param uuid
	 * @return String result
	 * @see String
	 * @see UMConcentrationRepository
	 * @see Integer
	 * @see Optional
	 * @throws DBResultQueryException
	 */
	public String getSapUM(
			final UMConcentrationRepository umConcentrationRepository, 
			final Integer id,
			final String uuid
			) {
		this.result = " ";
		Optional.ofNullable(id).filter((var i) -> i != null).ifPresent((var d) -> {
			Optional.ofNullable(umConcentrationRepository.searchById(id))
			.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(i) -> {
							this.result = String.valueOf(i[0]).toUpperCase() + " ";
						});
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		});
		return this.result;
	}

	/**
	 * Method in charge of validate a string and convert to uppercase
	 * @param param
	 * @return String result
	 * @see String
	 * @see Optional
	 */
	public String isValidString(
			final String param
			) {
		this.result = "";
		Optional.ofNullable(param).ifPresent((p) -> this.result = p.toUpperCase());
		return this.result;
	}

}