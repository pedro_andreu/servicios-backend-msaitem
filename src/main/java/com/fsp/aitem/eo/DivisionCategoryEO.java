/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.DivisionCategoryDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the divisions categories table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_DIVISIONS_CATEGORIES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class DivisionCategoryEO extends BaseEO {
	
	private static final long serialVersionUID = 6719542201152655153L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DIVISION_CATEGORY", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "SAP_DIVISION_CATEGORY", nullable = false, length = 20)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param DivisionCategoryDTO divisionCategory
	 * @see DivisionCategoryDTO
	 */
	public DivisionCategoryEO(final DivisionCategoryDTO divisionCategory) {
		this.setId(divisionCategory.getId());
	}

}