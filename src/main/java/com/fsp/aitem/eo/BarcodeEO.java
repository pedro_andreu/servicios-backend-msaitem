/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.BarcodeDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the bar codes table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_BARCODES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class BarcodeEO extends BaseEO {

	private static final long serialVersionUID = -7676571538036901009L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_BARCODE", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "TYPE", nullable = false, length = 20)
	private String type;
	@Column(name = "BARCODE", nullable = false, length = 20)
	private String barcode;
	@Column(name = "GROSS_WEIGHT", nullable = true, precision = 16, scale = 3)
	private BigDecimal grossWeight;
	@Column(name = "NET_WEIGHT", nullable = true, precision = 16, scale = 3)
	private BigDecimal netWeight;
	@Column(name = "LONGITUDE", nullable = true, precision = 16, scale = 3)
	private BigDecimal longitude;
	@Column(name = "HEIGHT", nullable = true, precision = 16, scale = 3)
	private BigDecimal height;
	@Column(name = "WIDTH", nullable = true, precision = 16, scale = 3)
	private BigDecimal width;
	@Column(name = "VOLUME", nullable = true, precision = 16, scale = 3)
	private BigDecimal volume;

	/**
	 * Constructor with parameters
	 * @param BarcodeDTO barcode
	 * @see BarcodeDTO
	 */
	public BarcodeEO(final BarcodeDTO barcode) {
		this.setType(barcode.getType());
		this.setBarcode(barcode.getBarcode());
	    this.setGrossWeight(barcode.getGrossWeight());
		this.setNetWeight(barcode.getNetWeight());
		this.setLongitude(barcode.getLongitude());
		this.setHeight(barcode.getHeight());
		this.setWidth(barcode.getWidth());
		this.setVolume(barcode.getVolume());
		this.setCreationDate(new Date());
		this.setStatus(true);
	}

}