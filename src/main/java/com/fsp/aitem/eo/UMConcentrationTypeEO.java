/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import com.fsp.aitem.dto.UMConcentrationTypeDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the um concentrations table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_UM_CONCENTRATIONS_TYPES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class UMConcentrationTypeEO extends BaseEO {
	
	private static final long serialVersionUID = -5090477403151204622L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_UM_CONCENTRATION_TYPE", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "UNIT_MEASURED", nullable = true, length = 45)
	private String unitMeasured;
	@Column(name = "SAP_UM_CONCENTRATION_TYPE", nullable = false, length = 3)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param UMConcentrationTypeDTO umConcentrationType
	 * @see UMConcentrationTypeDTO
	 */
	public UMConcentrationTypeEO(final UMConcentrationTypeDTO umConcentrationType) {
		this.setId(umConcentrationType.getId());
	}

}