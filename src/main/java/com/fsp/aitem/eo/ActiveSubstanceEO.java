/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fsp.aitem.dto.ActiveSubstanceDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the active substances table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_ACTIVE_SUBSTANCES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ActiveSubstanceEO extends BaseEO {

	private static final long serialVersionUID = 3518099474998164915L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ACTIVE_SUBSTANCE", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "TYPE", nullable = false, length = 45)
	private String type;
	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_ACTIVE_SUBSTANCE_TYPE", referencedColumnName = "ID_ACTIVE_SUBSTANCE_TYPE")
	private ActiveSubstanceTypeEO activeSubstanceTypeEO;
	
	/**
	 * Constructor with parameters
	 * @param ActiveSubstanceDTO activeSubstance
	 * @see ActiveSubstanceDTO
	 */
	public ActiveSubstanceEO(final ActiveSubstanceDTO activeSubstance) {
		this.setType(activeSubstance.getType());
		this.setActiveSubstanceTypeEO(new ActiveSubstanceTypeEO(activeSubstance.getActiveSubstanceTypeDTO()));
		this.setCreationDate(new Date());
		this.setStatus(true);
	}

}