/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fsp.aitem.dto.UMConcentrationDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the um concentrations table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_UM_CONCENTRATIONS")
@Data

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class UMConcentrationEO extends BaseEO {
	
	private static final long serialVersionUID = -5090477403151204622L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_UM_CONCENTRATION", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "TYPE", nullable = false, length = 45)
	private String type;
	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_UM_CONCENTRATION_TYPE", referencedColumnName = "ID_UM_CONCENTRATION_TYPE")
	private UMConcentrationTypeEO umConcentrationTypeEO;
	
	/**
	 * Constructor with parameters
	 * @param MConcentrationDTO umConcentration
	 * @see umConcentration
	 */
	public UMConcentrationEO(final UMConcentrationDTO umConcentration) {
		this.setType(umConcentration.getType());
		this.setUmConcentrationTypeEO(new UMConcentrationTypeEO(umConcentration.getUmConcentrationTypeDTO()));
		this.setCreationDate(new Date());
		this.setStatus(true);
	}

}