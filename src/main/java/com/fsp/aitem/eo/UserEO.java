/*
 * Copyright Farmacias San Pablo
 * 04-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the users table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Long
 * @see String
 */
@Entity
@Table(name="TBL_CAT_USERS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class UserEO extends BaseEO {

	private static final long serialVersionUID = 3056592451779610376L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_USER", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = true, length = 70)
	private String name;
	@Column(name = "SECOND_NAME", nullable = true, length = 45)
	private String secondName;
	@Column(name = "FIRST_NAME", nullable = true, length = 45)
	private String firstName;
	@Column(name = "LAST_NAME", nullable = true, length = 45)
	private String lastName;
	@Column(name = "TYPE", nullable = false, length = 45)
	private String type;
	@Column(name = "ANALYST_CODE", nullable = true, length = 12)
	private String analystCode;
	@Column(name = "SAP_USER", nullable = true, length = 45)
	private String sap;

	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public UserEO(final Integer id) {
		this.setId(id);
	}
	
}