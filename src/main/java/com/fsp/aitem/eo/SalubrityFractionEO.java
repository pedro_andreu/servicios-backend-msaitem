/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.SalubrityFractionDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the health fraction table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_SALUBRITYS_FRACTIONS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class SalubrityFractionEO extends BaseEO {

	private static final long serialVersionUID = -8932744406417632593L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_SALUBRITY_FRACTION", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "SAP_SALUBRITY_FRACTION", nullable = false, length = 3)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param SalubrityFractionDTO salubrityFraction
	 * @see SalubrityFractionDTO
	 */
	public SalubrityFractionEO(final SalubrityFractionDTO salubrityFraction) {
		this.setId(salubrityFraction.getId());
	}

}