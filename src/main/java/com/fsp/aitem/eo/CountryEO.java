/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.CountryDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the countries table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_COUNTRIES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class CountryEO extends BaseEO {

	private static final long serialVersionUID = -1500580013843814626L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_COUNTRY", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "SAP_COUNTRY", nullable = false, length = 3)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param CountryDTO country
	 * @see CountryDTO
	 */
	public CountryEO(final CountryDTO country) {
		this.setId(country.getId());
	}

}