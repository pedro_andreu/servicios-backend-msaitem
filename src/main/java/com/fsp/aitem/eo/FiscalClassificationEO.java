/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.FiscalClassificationDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the fiscal classification table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_FISCALS_CLASSIFICATIONS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class FiscalClassificationEO extends BaseEO {

	private static final long serialVersionUID = 3589696611852147967L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_FISCAL_CLASSIFICATION", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "TYPE", nullable = false, length = 20)
	private String type;
	@Column(name = "DENOMINATION", nullable = false, length = 45)
	private String denomination;
	@Column(name = "SAP_FISCAL_CLASSIFICATION", nullable = false, length = 3)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param FiscalClassificationDTO fiscalClassification
	 * @see FiscalClassificationDTO
	 */
	public FiscalClassificationEO(final FiscalClassificationDTO fiscalClassification) {
		this.setId(fiscalClassification.getId());
	}

}