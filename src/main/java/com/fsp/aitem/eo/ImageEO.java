/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.eo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the images table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Long
 * @see String
 */
@Entity
@Table(name="TBL_CAT_IMAGES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ImageEO extends BaseEO {

	private static final long serialVersionUID = 1007223770696723019L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_IMAGE", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "LINK", nullable = false, length = 250)
	private String link;
	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_ITEM", referencedColumnName = "ID_ITEM")
	private ItemEO itemEO;
	
	public ImageEO(final Integer id, final String name, final String link) {
		this.setItemEO(new ItemEO(id));
		this.setName(name);
		this.setLink(link);
		this.setCreationDate(new Date());
		this.setStatus(true);
	}

}