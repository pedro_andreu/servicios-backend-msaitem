/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.ExclusionListDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the exclusions list table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_EXCLUSIONS_LIST")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ExclusionListEO extends BaseEO {

	private static final long serialVersionUID = -8364574710702054606L;
	@Id
	@Column(name = "ID_EXCLUSION_LIST", unique = true, nullable = false, length = 9)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "SAP_EXCLUSION_LIST", nullable = false, length = 3)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param ExclusionListDTO exclusionList
	 * @see ExclusionListDTO
	 */
	public ExclusionListEO(final ExclusionListDTO exclusionList) {
		this.setId(exclusionList.getId());
	}

}