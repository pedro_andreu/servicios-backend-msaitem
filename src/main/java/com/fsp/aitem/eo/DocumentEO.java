/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.eo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fsp.aitem.dto.DocumentDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the documents table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Long
 * @see String
 */
@Entity
@Table(name="TBL_CAT_DOCUMENTS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class DocumentEO extends BaseEO {

	private static final long serialVersionUID = -8316393960961876517L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_DOCUMENT", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "LINK", nullable = false, length = 250)
	private String link;
	@ManyToOne(optional = false)
	@JoinColumn(name = "FK_ITEM", referencedColumnName = "ID_ITEM")
	private ItemEO itemEO;
	
	/**
	 * Constructor with parameters
	 * @param DocumentDTO document
	 * @see DocumentDTO
	 */
	public DocumentEO(final DocumentDTO document) {
		this.setName(document.getName());
		this.setItemEO(new ItemEO(id));
		this.setCreationDate(new Date());
		this.setStatus(true);
	}
	
	//  TODO:
	public DocumentEO(final Integer id, final String name, final String link) {
		this.setName(name);
		this.setLink(link);
		this.setItemEO(new ItemEO(id));
		this.setCreationDate(new Date());
		this.setStatus(true);
	}

}