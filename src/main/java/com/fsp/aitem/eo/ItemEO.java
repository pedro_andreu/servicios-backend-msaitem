/*
 * Copyright Farmacias San Pablo
 * 16-10-2019
 */
package com.fsp.aitem.eo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fsp.aitem.dto.ActiveSubstanceDTO;
import com.fsp.aitem.dto.BarcodeDTO;
import com.fsp.aitem.dto.ItemDTO;
import com.fsp.aitem.dto.UMConcentrationDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the items table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 * @see DivisionEO
 * @see ItemTypeEO
 */
@Entity
@Table(name="TBL_CAT_ITEMS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ItemEO extends BaseEO {
	
	private static final long serialVersionUID = -5063953757710495561L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ITEM", nullable = false, length = 9)
	private Integer id;
	@Column(name = "TYPE", nullable = true, length = 4)
	private String type;
	@JoinColumn(name = "FK_USER_CREATION", nullable = true)
	@ManyToOne(optional = false)
    private UserEO userCreation;
	@JoinColumn(name = "FK_USER_NEGOTIATOR", nullable = true)
	@ManyToOne(optional = false)
    private UserEO userNegotiator;
	@JoinColumn(name = "FK_USER_PROVIDER", nullable = true)
	@ManyToOne(optional = false)
    private UserEO userProvider;
	@JoinColumn(name = "FK_ITEM_TYPE", nullable = true)
	@ManyToOne(optional = false)
    private ItemTypeEO itemTypeEO;
	@JoinColumn(name = "FK_DIVISION", nullable = true)
	@ManyToOne(optional = false)
    private DivisionEO divisionEO;
	@Column(name = "DESCRIPTION", nullable = true, length = 100)
	private String description;
	@JoinColumn(name = "FK_BRAND", nullable = true)
	@ManyToOne(optional = false)
    private BrandEO brandEO;
	@Column(name = "PRODUCT", nullable = true, length = 20)
	private String product;
	@Column(name = "ACTIVE_SUBSTANCE", nullable = true, length = 20)
	private String activeSubstance;
	@Column(name = "COMMERCIAL_NAME", nullable = true, length = 20)
	private String commercialName;
	@Column(name = "NET_CONTENT", nullable = true, length = 20)
	private String netContent;
	@Column(name = "CONCENTRATION", nullable = true, length = 20)
	private String concentration;
	@Column(name = "BRAND_PROVIDER", nullable = true, length = 20)
	private String brandProvider;
	@Column(name = "VARIABLE", nullable = true, length = 20)
	private String variable;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ITEM", nullable = false)
	private List<BarcodeEO> barcodes;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ITEM", nullable = false)
	private List<UMConcentrationEO> concentrations;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ITEM", nullable = false)
	private List<ActiveSubstanceEO> activeSubstances;
	@Column(name = "PACKING_UNIT", nullable = true, precision = 5, scale = 0)
	private BigDecimal packingUnit;
	@JoinColumn(name = "FK_EXCLUSION_LIST", nullable = true)
	@ManyToOne(optional = false)
    private ExclusionListEO exclusionListEO;
	@JoinColumn(name = "FK_DIVISION_CATEGORY", nullable = true)
	@ManyToOne(optional = false)
    private DivisionCategoryEO divisionCategoryEO;
	@Column(name = "CONC_SUST_ACT_PRINT", nullable = true, length = 3)
	private BigDecimal concentrationActiveSubtance;
	@Column(name = "CONC_SUST_ACT_PRINT_1", nullable = true, length = 3)
	private BigDecimal concentrationActiveSubtance1;
	@Column(name = "CONC_SUST_ACT_PRINT_2", nullable = true, length = 3)
	private BigDecimal concentrationActiveSubtance2;
	@Column(name = "ANTIBIOTIC", nullable = true, columnDefinition = "TINYINT(1)")
	private Boolean antibiotic;
	@Column(name = "HIGH_ESPECIALITY", nullable = true, columnDefinition = "TINYINT(1)")
	private Boolean highEspeciality;
	@Column(name = "BRAND", nullable = true, length = 20)
	private String brandItem;
	@Column(name = "MAX_PRICE", nullable = true, precision = 16, scale = 3)
	private BigDecimal maxPrice;
	@Column(name = "LIST_COST", nullable = true, precision = 16, scale = 3)
	private BigDecimal listCost;
	@JoinColumn(name = "FK_FISCAL_SALE", nullable = false)
	@ManyToOne(optional = false)
    private FiscalClassificationEO fiscalSale;
	@JoinColumn(name = "FK_FISCAL_PURCHASE", nullable = false)
	@ManyToOne(optional = false)
    private FiscalClassificationEO fiscalPurchase;
	@JoinColumn(name = "FK_COUNTRY", nullable = false)
	@ManyToOne(optional = false)
    private CountryEO countryEO;
	@JoinColumn(name = "FK_SALUBRITY_FRACTION", nullable = false)
	@ManyToOne(optional = false)
    private SalubrityFractionEO salubrityFractionEO;
	@JoinColumn(name = "FK_SAT_CODE", nullable = false)
	@ManyToOne(optional = false)
    private SatCodeEO satCodeEO;
	@Column(name = "PROCESS_STATUS", nullable = false)
	private Boolean processStatus;
	@Column(name = "COMPLETED", nullable = false)
	private Boolean completed;
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see id
	 */
	public ItemEO(final Integer id) {
		this.setId(id);
	}
	
	/**
	 * Constructor with parameters
	 * @param ItemDTO item
	 * @see ItemDTO
	 */
	public ItemEO(final ItemDTO item) {
		System.out.println(item.getId());
		Optional.ofNullable(item.getId()).ifPresent((var id) -> {
			this.setId(id);
		});
		Optional.ofNullable(item.getType()).ifPresent((var i) -> {
			this.setType(i);
		});
		Optional.ofNullable(item.getUserCreation()).ifPresent((var uc) -> {
			this.setUserCreation(new UserEO(uc.getId()));
		});
		Optional.ofNullable(item.getUserNegotiator()).ifPresent((var un) -> {
			this.setUserNegotiator(new UserEO(un.getId()));
		});
		Optional.ofNullable(item.getUserProvider()).ifPresent((var up) -> {
			this.setUserProvider(new UserEO(up.getId()));
		});
		Optional.ofNullable(item.getDivisionDTO()).ifPresent((var d) -> {
			this.setDivisionEO(new DivisionEO(d));
		});
		Optional.ofNullable(item.getItemTypeDTO()).ifPresent((var it) -> {
			this.setItemTypeEO(new ItemTypeEO(it));
		});
		Optional.ofNullable(item.getDescription()).ifPresent((var d) -> {
			this.setDescription(d);
		});
		Optional.ofNullable(item.getBrandDTO()).ifPresent((var b) -> {
			this.setBrandEO(new BrandEO(b));
		});
		Optional.ofNullable(item.getProduct()).ifPresent((var p) -> {
			this.setProduct(p);
		});
		Optional.ofNullable(item.getCommercialName()).ifPresent((var cn) -> {
			this.setCommercialName(cn);
		});
		Optional.ofNullable(item.getActiveSubstance()).ifPresent((var as) -> {
			this.setActiveSubstance(as);
		});
		Optional.ofNullable(item.getUmcontrations()).ifPresent((var um) -> {
			this.addUmConcentration(um);
		});
		Optional.ofNullable(item.getNetContent()).ifPresent((var nc) -> {
			this.setNetContent(nc);
		});
		Optional.ofNullable(item.getConcentration()).ifPresent((var c) -> {
			this.setConcentration(c);
		});
		Optional.ofNullable(item.getBrandProvider()).ifPresent((var bp) -> {
			this.setBrandProvider(bp);
		});
		Optional.ofNullable(item.getVariable()).ifPresent((var v) -> {
			this.setVariable(v);
		});
		Optional.ofNullable(item.getBarcodes()).ifPresent((var b) -> {
			this.addBarcodes(b);
		});
		Optional.ofNullable(item.getPackingUnit()).ifPresent((var pu) -> {
			this.setPackingUnit(pu);
		});
		Optional.ofNullable(item.getExclusionListDTO()).ifPresent((var el) -> {
			this.setExclusionListEO(new ExclusionListEO(el));
		});
		Optional.ofNullable(item.getDivisionCategoryDTO()).ifPresent((var dc) -> {
			this.setDivisionCategoryEO(new DivisionCategoryEO(dc));
		});
		Optional.ofNullable(item.getBrandItem()).ifPresent((var bi) -> {
			this.setBrandItem(bi);
		});
		Optional.ofNullable(item.getActiveSubstances()).ifPresent((var as) -> {
			this.addActiveSubstances(as);
		});
		Optional.ofNullable(item.getConcentrationActiveSubtance()).ifPresent((var cas) -> {
			this.setConcentrationActiveSubtance(cas);
		});
		Optional.ofNullable(item.getConcentrationActiveSubtance1()).ifPresent((var cas1) -> {
			this.setConcentrationActiveSubtance1(cas1);
		});
		Optional.ofNullable(item.getConcentrationActiveSubtance2()).ifPresent((var cas2) -> {
			this.setConcentrationActiveSubtance2(cas2);
		});
		Optional.ofNullable(item.getAntibiotic()).ifPresent((var a) -> {
			this.setAntibiotic(a);
		});
		Optional.ofNullable(item.getHighEspeciality()).ifPresent((var hs) -> {
			this.setHighEspeciality(hs);
		});
		Optional.ofNullable(item.getMaxPrice()).ifPresent((var mp) -> {
			this.setMaxPrice(mp);
		});
		Optional.ofNullable(item.getListCost()).ifPresent((var ls) -> {
			this.setListCost(ls);
		});
		Optional.ofNullable(item.getFiscalSale()).ifPresent((var fs) -> {
			this.setFiscalSale(new FiscalClassificationEO(fs));
		});
		Optional.ofNullable(item.getFiscalPurchase()).ifPresent((var fp) -> {
			this.setFiscalPurchase(new FiscalClassificationEO(fp));
		});
		Optional.ofNullable(item.getCountryDTO()).ifPresent((var c) -> {
			this.setCountryEO(new CountryEO(c));
		});
		Optional.ofNullable(item.getSalubrityFractionDTO()).ifPresent((var sf) -> {
			this.setSalubrityFractionEO(new SalubrityFractionEO(sf));
		});
		Optional.ofNullable(item.getSatCodeDTO()).ifPresent((var sc) -> {
			this.setSatCodeEO(new SatCodeEO(sc));
		});
		this.setProcessStatus(false);
		this.setCreationDate(new Date());
		this.setStatus(true);
		this.setCompleted(false);
	}
	
	/**
	 * Method in charge of adding um concentrations
	 * @param List<ActiveSubstanceDTO> UMconcentrations
	 */
	public void addUmConcentration(List<UMConcentrationDTO> ums) {
		this.concentrations = new LinkedList<UMConcentrationEO>();
		ums.forEach((var um) -> {
			this.concentrations.add(new UMConcentrationEO(um));
		});
	}
	
	/**
	 * Method in charge of adding bar codes
	 * @param List<BarcodeDTO> bar codes
	 */
	public void addBarcodes(final List<BarcodeDTO> bcs) {
		this.barcodes = new LinkedList<BarcodeEO>();
		bcs.forEach((b) -> {
			this.barcodes.add(new BarcodeEO(b));
		});
	}
	
	/**
	 * Method in charge of adding active substances
	 * @param List<ActiveSubstanceDTO> activeSubstance
	 */
	public void addActiveSubstances(final List<ActiveSubstanceDTO> ass) {
		this.activeSubstances = new LinkedList<ActiveSubstanceEO>();
		ass.forEach((var as) -> {
			this.activeSubstances.add(new ActiveSubstanceEO(as));
		});
	}

}