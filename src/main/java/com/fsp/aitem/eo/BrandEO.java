/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.BrandDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the brands table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_BRANDS")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class BrandEO extends BaseEO {

	private static final long serialVersionUID = 3106419320955997201L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_BRAND", unique = true, nullable = true, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 45)
	private String name;
	@Column(name = "SAP_BRAND", nullable = false, length = 6)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param BrandDTO brand
	 * @see BrandDTO
	 */
	public BrandEO(final BrandDTO brand) {
		this.setId(brand.getId());
	}

}