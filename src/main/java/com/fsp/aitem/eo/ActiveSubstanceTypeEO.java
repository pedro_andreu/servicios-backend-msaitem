/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.eo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsp.aitem.dto.ActiveSubstanceTypeDTO;
import com.fsp.commonutil.eo.BaseEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Type class entity responsible for mapping to the active substance type table
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseEO
 * @see Integer
 * @see String
 */
@Entity
@Table(name="TBL_CAT_ACTIVE_SUBSTANCES_TYPES")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ActiveSubstanceTypeEO extends BaseEO {

	private static final long serialVersionUID = 3518099474998164915L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ACTIVE_SUBSTANCE_TYPE", unique = true, nullable = false, length = 9)
	private Integer id;
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;
	@Column(name = "SAP_ACTIVE_SUBSTANCE_TYPE", nullable = false, length = 6)
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param ActiveSubstanceTypeDTO activeSubstanceType
	 * @see ActiveSubstanceTypeDTO
	 */
	public ActiveSubstanceTypeEO(final ActiveSubstanceTypeDTO activeSubstanceType) {
		this.setId(activeSubstanceType.getId());
	}

}