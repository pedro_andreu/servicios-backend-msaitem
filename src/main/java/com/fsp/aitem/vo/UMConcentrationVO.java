/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.vo;

/**
 * VO type class responsible for storing values for concentration unit of measure
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class UMConcentrationVO {
	
	public static final String PRESENTATION = "Presentaci&oacute;n";
	public static final String FARMACEUTICAL_FORM = "Forma farmaceutica";
	public static final String UM_NET_CONT = "UM de contenido neto";
	public static final String UM_CONCENTRATION = "UM de concentraci&oacute;n";
	public static final String CONCENTRATION_UNIT_MEASURE = "UM de concentraci&oacute;n principal";
	public static final String CONCENTRATION_UNIT_MEASURE_1 = "UM de concentraci&oacute;n principal 1";
	public static final String CONCENTRATION_UNIT_MEASURE_2 = "UM de concentraci&oacute;n principal 2";
	
	/**
	 * Private constructor without parameters
	 */
	private UMConcentrationVO() { }

}