/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.vo;

/**
 * VO type class responsible for storing values for fiscal classifications
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class FiscalClassificationVO {
	
	public static final String SALE = "venta";
	public static final String PURCHASE = "compra";
	
	/**
	 * Private constructor without parameters
	 */
	private FiscalClassificationVO() {}

}