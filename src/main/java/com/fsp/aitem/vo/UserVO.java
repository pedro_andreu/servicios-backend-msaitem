/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.vo;

/**
 * VO type class responsible for storing values for users
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class UserVO {
	
	public static final String PROVIDER = "Proveedor";
	public static final String NEGOTIATOR = "Negociador";
	
	/**
	 * Private constructor without parameters
	 */
	private UserVO() {}

}