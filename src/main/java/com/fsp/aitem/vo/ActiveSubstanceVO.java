/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.vo;

/**
 * VO type class responsible for storing values for active substance
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class ActiveSubstanceVO {
	
	public static final String PRINCIPAL_ACTIVE_SUBSTANCE = "Substancia activa principal";
	public static final String ACTIVE_SUBSTANCE_1 = "Substancia activa 1";
	public static final String ACTIVE_SUBSTANCE_2 = "Substancia activa 2";
	
	/**
	 * Private constructor without parameters
	 */
	private ActiveSubstanceVO() {}

}