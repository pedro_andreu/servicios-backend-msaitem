/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.UMConcentrationEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the um concentration entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
*/
@Data
@NoArgsConstructor
@Component("umConcentrationDTO")
public class UMConcentrationDTO implements Serializable {

	private static final long serialVersionUID = -2855006663730898578L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "type")
	private String type;
	@JsonIgnore
	private UMConcentrationTypeDTO umConcentrationTypeDTO;

	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public UMConcentrationDTO(final Integer id) {
		this.setUmConcentrationTypeDTO(new UMConcentrationTypeDTO(id));
	}
	
	public UMConcentrationDTO(final Integer id, final String type) {
		this.setUmConcentrationTypeDTO(new UMConcentrationTypeDTO(id));
		this.setType(type);
	}
	
	/**
	 * Constructor with parameters
	 * @param UMConcentrationEO umConcentrationEO
	 * @see UMConcentrationEO
	 */
	public UMConcentrationDTO(final UMConcentrationEO umConcentrationEO) {
		this.setId(umConcentrationEO.getId());
		this.setType(umConcentrationEO.getType());
		this.setUmConcentrationTypeDTO(new UMConcentrationTypeDTO(umConcentrationEO.getUmConcentrationTypeEO()));
	}

}