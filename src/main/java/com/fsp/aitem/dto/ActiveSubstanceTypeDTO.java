/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.springframework.stereotype.Component;

import com.fsp.aitem.eo.ActiveSubstanceTypeEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the active substance type entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see Integer
* @see String
*/
@Data
@NoArgsConstructor
@Component("activeSubstanceTypeDTO")
public class ActiveSubstanceTypeDTO implements Serializable {
	
	private static final long serialVersionUID = -7525893247497799192L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param ActiveSubstanceTypeEO activeSubstanceTypeEO
	 * @see ActiveSubstanceTypeEO
	 */
	public ActiveSubstanceTypeDTO(final ActiveSubstanceTypeEO activeSubstanceTypeEO) {
		this.setId(activeSubstanceTypeEO.getId());
		this.setName(activeSubstanceTypeEO.getName());
		this.setSap(activeSubstanceTypeEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public ActiveSubstanceTypeDTO(final Integer id) {
		this.setId(id);
	}

}