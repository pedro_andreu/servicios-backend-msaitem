/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fsp.aitem.eo.SalubrityFractionEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the fraction salubrity entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
* @see Integer
*/
@Data
@NoArgsConstructor
@Component("salubrityFractionDTO")
public class SalubrityFractionDTO implements Serializable {

	private static final long serialVersionUID = -6797569922240368796L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param FractionSalubrityEO fractionSalubrityEO
	 * @see FractionSalubrityEO
	 */
	public SalubrityFractionDTO(final SalubrityFractionEO salubrityFractionEO) {
		this.setId(salubrityFractionEO.getId());
		this.setName(salubrityFractionEO.getName());
		this.setSap(salubrityFractionEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public SalubrityFractionDTO(final Integer id) {
		this.setId(id);
	}

}