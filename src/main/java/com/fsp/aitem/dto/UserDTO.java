/*
 * Copyright Farmacias San Pablo
 * 04-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.UserEO;
import com.fsp.commonutil.dto.BaseDTO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transport type class responsible for encapsulating the properties of the user entity
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BaseDTO
 * @see String
 */
@Data
@NoArgsConstructor
@Component("userDTO")
public class UserDTO implements Serializable {

	private static final long serialVersionUID = -7525893247497799192L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonProperty(access = Access.READ_ONLY, value = "second_name")
	private String secondName;
	@JsonProperty(access = Access.READ_ONLY, value = "first_name")
	private String firstName;
	@JsonProperty(access = Access.READ_ONLY, value = "last_name")
	private String lastName;
	@JsonIgnore
	private String type;
	@JsonIgnore
	private String analystCode;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param UserEO userEO
	 * @see UserEO
	 */
	public UserDTO(UserEO userEO) {
		this.setId(userEO.getId());
		this.setName(userEO.getName());
		this.setSecondName(userEO.getSecondName());
		this.setFirstName(userEO.getFirstName());
		this.setLastName(userEO.getLastName());
		this.setType(userEO.getType());
		this.setAnalystCode(userEO.getAnalystCode());
		this.setSap(userEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public UserDTO(final Integer id) {
		this.setId(id);
	}
	
}