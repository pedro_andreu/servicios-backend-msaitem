/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.FiscalClassificationEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the fiscal classifications entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
* @see Integer
*/
@Data
@NoArgsConstructor
@Component("fiscalClassificationDTO")
public class FiscalClassificationDTO implements Serializable {

	private static final long serialVersionUID = 7169798448077088043L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonIgnore
	private String type;
	@JsonProperty(access = Access.READ_ONLY, value = "denomination")
	private String denomination;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param FiscalClassificationEO fiscalClassificationEO
	 * @see FiscalClassificationEO
	 */
	public FiscalClassificationDTO(final FiscalClassificationEO fiscalClassificationEO) {
		this.setId(fiscalClassificationEO.getId());
		this.setType(fiscalClassificationEO.getType());
		this.setDenomination(fiscalClassificationEO.getDenomination());
		this.setSap(fiscalClassificationEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public FiscalClassificationDTO(final Integer id) {
		this.setId(id);
	}

}