/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import com.fsp.aitem.eo.BarcodeEO;
import com.fsp.commonutil.dto.BaseDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the bar code entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see BaseDTO
* @see String
*/
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Component("barcodeDTO")
public class BarcodeDTO extends BaseDTO {

	private static final long serialVersionUID = -7110123104661518717L;
	private String type;
	private String barcode;
	private BigDecimal grossWeight;
	private BigDecimal netWeight;
	private BigDecimal longitude;
	private BigDecimal height;
	private BigDecimal width;
	private BigDecimal volume;
	
	public BarcodeDTO(final BarcodeEO barcodeEO) {
		this.setType(barcodeEO.getType());
		this.setBarcode(barcodeEO.getBarcode());
	    this.setGrossWeight(barcodeEO.getGrossWeight());
		this.setNetWeight(barcodeEO.getNetWeight());
		this.setLongitude(barcodeEO.getLongitude());
		this.setHeight(barcodeEO.getHeight());
		this.setWidth(barcodeEO.getWidth());
		this.setVolume(barcodeEO.getVolume());
		this.setCreationDate(barcodeEO.getCreationDate());
		this.setStatus(barcodeEO.getStatus());
	}

}