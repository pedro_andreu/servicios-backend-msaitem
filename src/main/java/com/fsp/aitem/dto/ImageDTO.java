package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Component("imageDTO")
public class ImageDTO implements Serializable {

	private static final long serialVersionUID = 8170370439588662601L;
	private Integer id;
	private String name;
	private MultipartFile file;
	
	public ImageDTO(final String name, final MultipartFile file) {
		this.setName(name);
		this.setFile(file);
	}
	
	public ImageDTO(final Integer id, final String name, final MultipartFile file) {
		this.setId(id);
		this.setName(name);
		this.setFile(file);
	}
	
}