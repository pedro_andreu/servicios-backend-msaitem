/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.DivisionCategoryEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transport type class responsible for encapsulating the properties of the division category entity
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see Integer
 * @see String
 */
@Data
@NoArgsConstructor
@Component("divisionCategoryDTO")
public class DivisionCategoryDTO implements Serializable {

	private static final long serialVersionUID = -5043953054410995587L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param DivisionCategoryEO divisionCategoryEO
	 * @see DivisionCategoryEO
	 */
	public DivisionCategoryDTO(final DivisionCategoryEO divisionCategoryEO) {
		this.setId(divisionCategoryEO.getId());
		this.setName(divisionCategoryEO.getName());
		this.setSap(divisionCategoryEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public DivisionCategoryDTO(final Integer id) {
		this.setId(id);
	}

}