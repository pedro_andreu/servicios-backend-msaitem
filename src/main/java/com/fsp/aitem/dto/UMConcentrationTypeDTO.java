/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.UMConcentrationTypeEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the um concentration entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
*/
@Data
@NoArgsConstructor
@Component("umConcentrationTypeDTO")
public class UMConcentrationTypeDTO implements Serializable {

	private static final long serialVersionUID = -2855006663730898578L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "unit_measured")
	private String unitMeasured;
	@JsonIgnore
	private String sap;

	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public UMConcentrationTypeDTO(final Integer id) {
		this.setId(id);
	}
	
	/**
	 * Constructor with parameters
	 * @param IUMConcentrationTypeEO umConcentrationTypeEO
	 * @see IUMConcentrationTypeEO
	 */
	public UMConcentrationTypeDTO(final UMConcentrationTypeEO umConcentrationTypeEO) {
		this.setId(umConcentrationTypeEO.getId());
		this.setUnitMeasured(umConcentrationTypeEO.getUnitMeasured());
		this.setSap(umConcentrationTypeEO.getSap());
    }

}