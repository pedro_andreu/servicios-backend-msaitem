/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.springframework.stereotype.Component;

import com.fsp.aitem.eo.CountryEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the country entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see Integer
* @see String
*/
@Data
@NoArgsConstructor
@Component("countryDTO")
public class CountryDTO implements Serializable {

	private static final long serialVersionUID = -5967018422874254651L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param CountryEO countryEO
	 * @see CountryEO
	 */
	public CountryDTO(final CountryEO countryEO) {
		this.setId(countryEO.getId());
		this.setName(countryEO.getName());
		this.setSap(countryEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public CountryDTO(final Integer id) {
		this.setId(id);
	}

}