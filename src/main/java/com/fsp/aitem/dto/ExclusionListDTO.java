/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.ExclusionListEO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the fraction salubrity entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
* @see Integer
*/
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Component("exclusionListDTO")
public class ExclusionListDTO implements Serializable {

	private static final long serialVersionUID = -1561757013443522049L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param ExclusionListEO exclusionListEO
	 * @see ExclusionListEO
	 */
	public ExclusionListDTO(final ExclusionListEO exclusionListEO) {
		this.setId(exclusionListEO.getId());
		this.setName(exclusionListEO.getName());
		this.setSap(exclusionListEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public ExclusionListDTO(final Integer id) {
		this.setId(id);
	}

}