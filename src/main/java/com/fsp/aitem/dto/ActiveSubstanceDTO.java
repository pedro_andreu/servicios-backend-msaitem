/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.springframework.stereotype.Component;

import com.fsp.aitem.eo.ActiveSubstanceEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the active substance entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see Integer
* @see String
* @see ActiveSubstanceTypeDTO
*/
@Data
@NoArgsConstructor
@Component("activeSubstanceDTO")
public class ActiveSubstanceDTO implements Serializable {
	
	private static final long serialVersionUID = -7525893247497799192L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "type")
	private String type;
	@JsonIgnore
	private ActiveSubstanceTypeDTO activeSubstanceTypeDTO;

	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @param String type
	 * @see Integer
	 * @see String
	 * @see ActiveSubstanceTypeDTO
	 */
	public ActiveSubstanceDTO(final Integer id, final String type) {
		this.setActiveSubstanceTypeDTO(new ActiveSubstanceTypeDTO(id));
		this.setType(type);
	}
	
	/**
	 * Constructor with parameters
	 * @param ActiveSubstanceEO activeSubstanceEO
	 * @see ActiveSubstanceEO
	 * @see ActiveSubstanceTypeDTO
	 */
	public ActiveSubstanceDTO(final ActiveSubstanceEO activeSubstanceEO) {
		this.setId(activeSubstanceEO.getId());
		this.setType(activeSubstanceEO.getType());
		this.setActiveSubstanceTypeDTO(new ActiveSubstanceTypeDTO(activeSubstanceEO.getActiveSubstanceTypeEO()));		
	}

}