/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.SatCodeEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the sat code entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see Serializable
* @see String
*/
@Data
@NoArgsConstructor
@Component("satCodeDTO")
public class SatCodeDTO implements Serializable {

	private static final long serialVersionUID = -3825889968971030315L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param SatCodeEO satCodeEO
	 * @see SatCodeEO
	 */
	public SatCodeDTO(final SatCodeEO satCodeEO) {
		this.setId(satCodeEO.getId());
		this.setName(satCodeEO.getName());
		this.setSap(satCodeEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public SatCodeDTO(final Integer id) {
		this.setId(id);
	}

}