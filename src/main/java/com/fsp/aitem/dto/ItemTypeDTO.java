/*
 * Copyright Farmacias San Pablo
 * 15-10-2019
 */
package com.fsp.aitem.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.fsp.aitem.eo.ItemTypeEO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the item type entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see BaseDTO
* @see String
*/
@Data
@NoArgsConstructor
@Component("itemTypeDTO")
public class ItemTypeDTO implements Serializable {

	private static final long serialVersionUID = -1343647318893599162L;
	@JsonProperty(access = Access.READ_ONLY, value = "id")
	private Integer id;
	@JsonProperty(access = Access.READ_ONLY, value = "name")
	private String name;
	@JsonIgnore
	private String sap;
	
	/**
	 * Constructor with parameters
	 * @param ItemTypeEO itemTypeEO
	 * @see ItemTypeEO
	 */
	public ItemTypeDTO(final ItemTypeEO itemTypeEO) {
		this.setId(itemTypeEO.getId());
		this.setName(itemTypeEO.getName());
		this.setSap(itemTypeEO.getSap());
	}
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @see Integer
	 */
	public ItemTypeDTO(final Integer id) {
		this.setId(id);
	}

}