/*
 * Copyright Farmacias San Pablo
 * 16-10-2019
 */
package com.fsp.aitem.dto;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.fsp.commonutil.dto.BaseDTO;
import com.fsp.aitem.eo.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* Transport type class responsible for encapsulating the properties of the item entity
* @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
* @version 1.0
* @since 12
* @see BaseDTO
* @see String
* @see DivisionDTO
*/
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Component("itemDTO")
public class ItemDTO extends BaseDTO {
	
	private static final long serialVersionUID = -902569341074479726L;
	private String type;
	private UserDTO userCreation;
	private UserDTO userNegotiator;
	private UserDTO userProvider;
	private String analystCode;
	private ItemTypeDTO itemTypeDTO;
	private DivisionDTO divisionDTO;
	private String description;
	private BrandDTO brandDTO;
	private String product;
	private String activeSubstance;
	private String commercialName;
	private List<UMConcentrationDTO> umcontrations;
	private String netContent;
	private String concentration;
	private String brandProvider;
	private String variable;
	private List<BarcodeDTO> barcodes;
	private BigDecimal packingUnit;
	private ExclusionListDTO exclusionListDTO;
	private DivisionCategoryDTO divisionCategoryDTO;
	private String brand;
	private List<ActiveSubstanceDTO> activeSubstances;
	private BigDecimal concentrationActiveSubtance;
	private BigDecimal concentrationActiveSubtance1;
	private BigDecimal concentrationActiveSubtance2;
	private Boolean antibiotic;
	private Boolean highEspeciality;
	private String brandItem;
	private BigDecimal maxPrice;
	private BigDecimal listCost;
	private FiscalClassificationDTO fiscalSale;
	private FiscalClassificationDTO fiscalPurchase;
	private CountryDTO countryDTO;
	private SalubrityFractionDTO salubrityFractionDTO;
	private SatCodeDTO satCodeDTO;
	private Boolean processStatus;
	private Boolean completed;

	/**
	 * Constructor with parameters
	 * @param ItemEO itemEO
	 * @see ItemEO
	 */
	public ItemDTO(final ItemEO itemEO) {
		this.setId(itemEO.getId());
		this.setType(itemEO.getType());
		this.setUserCreation(new UserDTO(itemEO.getUserCreation()));
		Optional.ofNullable(itemEO.getUserNegotiator()).ifPresent((var n) -> {
			this.setUserNegotiator(new UserDTO(n));
			this.setAnalystCode(this.getUserNegotiator().getAnalystCode());
		});
		Optional.ofNullable(itemEO.getUserProvider()).ifPresent((var p) -> {
			this.setUserProvider(new UserDTO(p));
		});
		Optional.ofNullable(itemEO.getDivisionEO()).ifPresent((var d) -> {
			this.setDivisionDTO(new DivisionDTO(d));
		});
		Optional.ofNullable(itemEO.getItemTypeEO()).ifPresent((var it) -> {
			this.setItemTypeDTO(new ItemTypeDTO(it));
		});
		Optional.ofNullable(itemEO.getBrandEO()).ifPresent((var b) -> {
			this.setBrandDTO(new BrandDTO(b));
		});
		Optional.ofNullable(itemEO.getDivisionCategoryEO()).ifPresent((var dc) -> {
			this.setDivisionCategoryDTO(new DivisionCategoryDTO(dc));
		});
		Optional.ofNullable(itemEO.getCountryEO()).ifPresent((var c) -> {
			this.setCountryDTO(new CountryDTO(c));
		});
		Optional.ofNullable(itemEO.getSatCodeEO()).ifPresent((var sc) -> {
			this.setSatCodeDTO(new SatCodeDTO(sc));
		});
		Optional.ofNullable(itemEO.getExclusionListEO()).ifPresent((var el) -> {
			this.setExclusionListDTO(new ExclusionListDTO(el));
		});	
		Optional.ofNullable(itemEO.getSalubrityFractionEO()).ifPresent((var sf) -> {
			this.setSalubrityFractionDTO(new SalubrityFractionDTO(sf));
		});
		Optional.ofNullable(itemEO.getFiscalSale()).ifPresent((var fs) -> {
			this.setFiscalSale(new FiscalClassificationDTO(fs));
		});
		Optional.ofNullable(itemEO.getFiscalPurchase()).ifPresent((var fp) -> {
			this.setFiscalPurchase(new FiscalClassificationDTO(fp));
		});
		this.setDescription(itemEO.getDescription());
		this.setProduct(itemEO.getProduct());
		this.setCommercialName(itemEO.getCommercialName());
		this.setActiveSubstance(itemEO.getActiveSubstance());
		this.addUMConcentration(itemEO.getConcentrations());
		this.setNetContent(itemEO.getNetContent());
		this.setConcentration(itemEO.getConcentration());
		this.setBrandProvider(itemEO.getBrandProvider());
		this.setVariable(itemEO.getVariable());
		this.addBarcodes(itemEO.getBarcodes());
		this.setPackingUnit(itemEO.getPackingUnit());
		this.setBrandItem(itemEO.getBrandItem());
		this.addActiveSubstance(itemEO.getActiveSubstances());
		this.setConcentrationActiveSubtance(itemEO.getConcentrationActiveSubtance());
		this.setConcentrationActiveSubtance1(itemEO.getConcentrationActiveSubtance1());
		this.setConcentrationActiveSubtance2(itemEO.getConcentrationActiveSubtance2());
		this.setAntibiotic(itemEO.getAntibiotic());
		this.setHighEspeciality(itemEO.getHighEspeciality());
		this.setMaxPrice(itemEO.getMaxPrice());
		this.setListCost(itemEO.getListCost());
		this.setProcessStatus(itemEO.getProcessStatus());
		this.setCreationDate(itemEO.getCreationDate());
		this.setStatus(itemEO.getStatus());
		this.setCompleted(itemEO.getCompleted());
	}
	
	/**
	 * Method in charge of add um concentrations
	 * @param List<UMConcentrationDTO> umConcentrationDTO
	 */
	public void addUMConcentration(final List<UMConcentrationEO> ums) {
		this.umcontrations = new LinkedList<UMConcentrationDTO>();
		ums.forEach((um) -> {
			this.umcontrations.add(new UMConcentrationDTO(um));
		});
	}
	
	/**
	 * Method in charge of adding bar codes
	 * @param List<BarcodeDTO> bar codes
	 */
	public void addBarcodes(final List<BarcodeEO> ums) {
		this.barcodes = new LinkedList<BarcodeDTO>();
		ums.forEach((b) -> {
			this.barcodes.add(new BarcodeDTO(b));
		});
	}

	/**
	 * Method in charge of adding active substances
	 * @param List<ActiveSubstanceDTO> activeSubstance
	 */
	public void addActiveSubstance(List<ActiveSubstanceEO> ass) {
		this.activeSubstances = new LinkedList<ActiveSubstanceDTO>();
		ass.forEach((as) -> {
			this.activeSubstances.add(new ActiveSubstanceDTO(as));
		});
	}

}