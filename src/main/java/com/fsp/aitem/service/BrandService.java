/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.aitem.dto.BrandDTO;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a brands
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface BrandService {
	
	/**
	 * Method responsible for performing the logic to search for all brands
	 * @param String UUI
	 * @return ResponseEntity which contains the all brands and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see BrandDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<?> searchBrands(
			final String letter,
			final String uuid) 
					throws DBAccessException;

}