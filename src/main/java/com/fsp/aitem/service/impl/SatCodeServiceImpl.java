/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.SatCodeDTO;
import com.fsp.aitem.repository.SatCodeRepository;
import com.fsp.aitem.service.SatCodeService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type interface responsible for performing the logic for the transactions of a salubrity fraction
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see SatCodeService
 * @see SatCodeRepository
 * @see SatCodeDTO
 * @see ResponseEntity
 */
@Service("satCodeService")
public class SatCodeServiceImpl implements SatCodeService {
	
	@Autowired
	@Qualifier("satCodeRepository")
	private SatCodeRepository satCodeRepository;
	@Autowired
	@Qualifier("satCodeDTO")
	private SatCodeDTO satCodeDTO;
	
	private ResponseEntity<List<SatCodeDTO>> response = null;

	/**
	 * Method responsible for performing the logic to search for all sat codes
	 * @param String UUI
	 * @return ResponseEntity which contains the all sat codes and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see Object
	 * @see SatCodeDTO
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<List<SatCodeDTO>> searchSatCode(final String code, final String uuid) throws DBAccessException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.satCodeRepository.searchByCode(code));
			List<SatCodeDTO> satCodes = new LinkedList<SatCodeDTO>();
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var sc) -> {
							this.satCodeDTO = new SatCodeDTO();
							this.satCodeDTO.setId(Integer.parseInt(String.valueOf(sc[0])));
							this.satCodeDTO.setSap(String.valueOf(sc[1]));	
							this.satCodeDTO.setName(String.valueOf(sc[2]));		
							satCodes.add(this.satCodeDTO);
						});
			    this.response = new ResponseEntity<List<SatCodeDTO>>(new ArrayList<>(satCodes), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}  
		return this.response;
	}

}