/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.FiscalClassificationDTO;
import com.fsp.aitem.repository.FiscalClassificationRepository;
import com.fsp.aitem.service.FiscalClassificationService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a fiscal classification
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see FiscalClassificationService
 * @see FiscalClassificationDTO
 * @see FiscalClassificationRepository
 * @see ResponseEntity
 */
@Service("fiscalClassificationService")
public class FiscalClassificationServiceImpl implements FiscalClassificationService {
	
	@Autowired
	@Qualifier("fiscalClassificationRepository")
	private FiscalClassificationRepository fiscalClassificationRepository;
	@Autowired
	@Qualifier("fiscalClassificationDTO")
	private FiscalClassificationDTO fiscalClassificationDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all sales fiscals classifications
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllSalesFiscalClassifications(String uuid)
			throws DBAccessException, DBResultQueryException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.fiscalClassificationRepository.searchSales());
			this.response = getResponseFiscalClassification(optional, uuid);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

	/**
	 * Method responsible for performing the logic to search for all purchase fiscals classifications
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllPurchaseFiscalClassifications(String uuid)
			throws DBAccessException, DBResultQueryException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.fiscalClassificationRepository.searchPurchases());
			this.response = getResponseFiscalClassification(optional, uuid);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}
	
	/**
	 * Method responsible for performing the iteration and validation of an optional object
	 * @param Optional<List<Object[]>>
	 * @param String UUID 
	 * @return ResponseEntity which contains the result of transaction in the data base
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see FiscalClassificationDTO
	 * @see ResponseMessageVO
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	private ResponseEntity<?> getResponseFiscalClassification(
			final Optional<List<Object[]>> optional, 
			final String uuid) {
		List<FiscalClassificationDTO> fiscalClasifications = new LinkedList<FiscalClassificationDTO>();
		optional.ifPresentOrElse((var l) -> {
			l.stream().forEach(
					(var f) -> {
						this.fiscalClassificationDTO = new FiscalClassificationDTO();
						this.fiscalClassificationDTO.setId(Integer.parseInt(String.valueOf(f[0])));		
						this.fiscalClassificationDTO.setDenomination(String.valueOf(f[1]));		
						fiscalClasifications.add(this.fiscalClassificationDTO);
					});
			this.response = new ResponseEntity<List<FiscalClassificationDTO>>(new ArrayList<>(fiscalClasifications), HttpStatus.OK);
		}, () -> {
			throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
		});
		return this.response;
	}

}