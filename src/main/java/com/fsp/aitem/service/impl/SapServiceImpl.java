package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.ItemDTO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.SapJSON;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.aitem.service.SapService;
import com.fsp.aitem.util.BarcodeUtil;
import com.fsp.aitem.util.ItemUtil;
import com.fsp.aitem.util.RestTemplateUtil;
import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.exception.SAProcessException;
import com.fsp.commonutil.vo.ResponseMessageVO;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("sapService")
public class SapServiceImpl implements SapService {

	@Autowired
	@Qualifier("itemRepository")
	private ItemRepository itemRepository;

	@Autowired
	@Qualifier("restTemplateUtil")
	private RestTemplateUtil restTemplateUtil;
	@Autowired
	@Qualifier("itemUtil")
	private ItemUtil itemUtil;
	@Autowired
	@Qualifier("barcodeUtil")
	private BarcodeUtil barcodeUtil;

	private ResponseEntity<ResponseIdDTO> responseId = null;
	private ResponseEntity<List<ResponseIdDTO>> responseList = null;

	private ResponseIdDTO responseIdDto = null;

	@Override
	public ResponseEntity<List<ResponseIdDTO>> sendItem(final List<Integer> items, final String uuid)
			throws DBAccessException {
		try {
			List<ResponseIdDTO> responseList = new LinkedList<ResponseIdDTO>();
			if (items.size() != 0) {
				items.forEach((var id) -> {
					Optional<ItemEO> optional = this.itemRepository.findById(id);
					optional.ifPresentOrElse((var i) -> {
						if (i.getCompleted()) {
							ItemJSON itemJSON = this.itemUtil.convertToJSON(new ItemDTO(i));
							ResponseEntity<SapJSON> entitySap = this.restTemplateUtil
									.createRestTemplate(itemJSON, uuid);
							log.info("convertToJSON(): " + itemJSON.toString());
							Optional<Integer> optionalEntity = Optional.of(entitySap.getStatusCode().value())
									.filter((c) -> c == HttpStatus.OK.value());
							optionalEntity.ifPresent((var r) -> {
								SapJSON sap = entitySap.getBody();
								log.info("getBody(): " + sap.toString());
								Optional<String> optionalType = Optional.of(sap.getType());
								optionalType.filter((t) -> t.equals("S")).ifPresent((var t) -> {
									Optional<Integer> optionalUpdate = Optional.of(
											this.itemRepository.updateProcessStatusById(i.getId(), true, new Date()));
									optionalUpdate.filter((res) -> res != 0)
											.ifPresent((type) -> responseList.add(new ResponseIdDTO(true, i.getId())));
								});
								optionalType.filter((t) -> t.equals("E")).ifPresent((var t) -> {
									responseList.add(new ResponseIdDTO(false, i.getId()));
								});
								optionalEntity.orElseGet(() -> {
									throw new SAProcessException(ResponseMessageVO.ERROR_MESSAGE, uuid);
								});
							});
							optionalEntity.orElseGet(() -> {
								throw new SAProcessException(ResponseMessageVO.ERROR_MESSAGE, uuid);
							});
						} else {
							responseList.add(new ResponseIdDTO(false, 0));
						}
						this.responseList = new ResponseEntity<List<ResponseIdDTO>>(
								new ArrayList<ResponseIdDTO>(responseList), HttpStatus.OK);
					}, () -> {
						throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
					});
				});
			} else {
				throw new SAProcessException(ResponseMessageVO.ERROR_MESSAGE, uuid);
			}
		} catch (DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseList;
	}

	@Override
	public ResponseEntity<ResponseIdDTO> searchBarcode(final String barcode, final String uuid)
			throws DBAccessException {
		this.responseId = null;
		String bc = null;
		bc = this.barcodeUtil.isValidBarcode(this.itemRepository, barcode, uuid);
		if (bc == null) {
			ResponseEntity<SapJSON> entitySap = this.restTemplateUtil
					.createRestTemplate(new ItemJSON(barcode, "VALIDATE"), uuid);
			Optional<Integer> optionalEntity = Optional.of(entitySap.getStatusCode().value())
					.filter((c) -> c == HttpStatus.OK.value());
			optionalEntity.ifPresent((var r) -> {
				SapJSON sap = entitySap.getBody();
				log.info("SAP Response: " + sap.toString());
				Optional<String> optionalType = Optional.of(sap.getType());
				optionalType.filter((t) -> t.equals("S")).ifPresent((var t) -> {
					log.info("El cogigo de barras: " + barcode + " no esta registrado");
					this.responseIdDto = new ResponseIdDTO(false, 0);
				});
				optionalType.filter((t) -> t.equals("E")).ifPresent((var t) -> {
					log.info("El cogigo de barras: " + barcode + " ya esta registrado en sap");
					this.responseIdDto = new ResponseIdDTO(true, 0);
				});
				optionalEntity.orElseGet(() -> {
					throw new SAProcessException(ResponseMessageVO.ERROR_MESSAGE, uuid);
				});
				this.responseId = new ResponseEntity<ResponseIdDTO>(this.responseIdDto, HttpStatus.OK);
			});
		} else {
			log.info("El cogigo de barras: " + barcode + " ya esta registrado en la base de datos");
			this.responseId = new ResponseEntity<>(new ResponseIdDTO(true, 0), HttpStatus.OK);
		}
		return this.responseId;
	}

}