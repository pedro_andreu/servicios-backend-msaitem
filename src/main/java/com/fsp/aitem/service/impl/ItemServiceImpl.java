/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.DivisionDTO;
import com.fsp.aitem.dto.ItemDTO;
import com.fsp.aitem.dto.UserDTO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.ItemUpdateJSON;
import com.fsp.aitem.repository.DivisionRepository;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.aitem.repository.UMConcentrationRepository;
import com.fsp.aitem.repository.UpdateItemRepository;
import com.fsp.aitem.repository.UserRepository;
import com.fsp.aitem.service.ItemService;
import com.fsp.aitem.util.BarcodeUtil;
import com.fsp.aitem.util.DescriptionUtil;
import com.fsp.aitem.util.ItemUtil;
import com.fsp.aitem.util.RestTemplateUtil;
import com.fsp.aitem.util.UpdateItemUtil;
import com.fsp.commonutil.dto.ResponseMessageDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a items
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ItemService
 * @see ItemRepository
 * @see UMConcentrationRepository
 * @see ItemDTO
 * @see ItemUtil
 * @see ResponseEntity
 */
@Service("itemService")
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	@Qualifier("itemRepository")
	private ItemRepository itemRepository;
	@Autowired
	@Qualifier("umConcentrationRepository")
	private UMConcentrationRepository umConcentrationRepository;
	@Autowired
	@Qualifier("divisionRepository")
	private DivisionRepository divisionRepository;
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;
	@Autowired
	@Qualifier("updateItemRepository")
	private UpdateItemRepository updateItemRepository;
	
	@Autowired
	@Qualifier("itemDTO")
	private ItemDTO itemDTO;
	
	@Autowired
	@Qualifier("itemUtil")
	private ItemUtil itemUtil;
	@Autowired
	@Qualifier("descriptionUtil")
	private DescriptionUtil descriptionUtil;
	@Autowired
	@Qualifier("barcodeUtil")
	private BarcodeUtil barcodeUtil;
	@Autowired
	@Qualifier("restTemplateUtil")
	private RestTemplateUtil restTemplateUtil;
	@Autowired
	@Qualifier("updateItemUtil")
	private UpdateItemUtil updateItemUtil;
	
	@Value("${response.item.equals.barcode}")
	private String errorEqualsBarcode;
	@Value("${response.item.search.barcode.db}")
	private String errorBarcodeDB;
	@Value("${response.item.search.barcode.sap}")
	private String errorBarcodeSap;
	@Value("${user.provider}")
	private String provider;
	@Value("${user.negotiator}")
	private String negotiator;
	@Value("${user.role.name.admin}")
	private String admin;
	@Value("${user.role.name.aitem}")
	private String aitem;
	@Value("${user.role.name.autorization}")
	private String autorization;
	@Value("${user.role.name.marketing}")
	private String marketing;
	@Value("${user.role.name.data}")
	private String data;

	private ResponseEntity<ResponseMessageDTO> responseMessage = null;
	private ResponseEntity<ItemJSON> responseObject = null;
	private ResponseEntity<ItemUpdateJSON> responseUpdateObject = null;
	private ResponseEntity<List<ItemJSON>> responseList = null;
	private ItemJSON item = null;
	private ItemUpdateJSON itemUpdate = null;
	private String sapUser = null;
	private String type = null;
	private String role = null;
	
	@Override
	public ResponseEntity<ResponseMessageDTO> createDescription(
			final Integer idDivision, final String product, final Integer presentation,
			final String netContent, final Integer umNetContent, final String brandProvider, final String variable,
			final String activeSubstance, final Integer farmaceuticalForm, final String concentration, final Integer umConcentration,
			final String brandItem, final String commercialName, final String uuid) 
			throws DBAccessException {
		try {
			Optional<DivisionDTO> division = Optional.ofNullable(this.descriptionUtil.getDivision(this.divisionRepository, idDivision, uuid));
			division.ifPresent((var d) -> {
				if(null != division.get().getName()) {
					String description = this.descriptionUtil.isValidDescriptionItem(
							presentation, umNetContent, farmaceuticalForm, umConcentration,
							division.get().getName(), this.umConcentrationRepository, commercialName,
							netContent, concentration, brandProvider, product, activeSubstance, variable, brandItem, uuid);
					this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(true, description.length(), description), HttpStatus.OK);
				} else {
					this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED), HttpStatus.OK);
				}
			});
		} catch(NullPointerException nex) {
			throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
		}
		return this.responseMessage;
	}

	/**
	 * Method responsible for performing the logic to add item
	 * @param String UUI
	 * @return ResponseEntity which contains the item id and code HTTP
	 * @see ResponseEntity
	 * @see Optional
	 * @see ItemEO
	 * @see HttpStatus
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<ResponseMessageDTO> addItem(
			final ItemJSON itemJSON, 
			final Integer userId,
			final String uuid) 
					throws DBAccessException {
		try {
			Optional.ofNullable(this.userRepository.findById(userId)).
			ifPresent((var u) -> {
				if(itemJSON.getUnitBarcode().equals(itemJSON.getBoxBarcode())) {
					this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, this.errorEqualsBarcode), HttpStatus.OK);
				} else {
					this.itemDTO = this.itemUtil.convertToDTO(itemJSON);
					this.itemDTO.setUserCreation(new UserDTO(u.get().getId()));
					Optional<DivisionDTO> division = Optional.ofNullable(this.descriptionUtil.getDivision(this.divisionRepository, 
							this.itemDTO.getDivisionDTO().getId(), uuid));
					division.ifPresent(
							(var d) -> {
								Optional.ofNullable(this.itemDTO.getBarcodes()).
								filter((var b) -> b.size() != 0).
								ifPresent((var bcs) -> {
									if(!this.barcodeUtil.isValidBarcode(this.itemRepository, bcs, uuid)) {
										this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, this.errorBarcodeDB), HttpStatus.OK);
									} else {
										if(this.barcodeUtil.isValidBarcodeSap(this.restTemplateUtil, bcs, uuid)) {
											this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, this.errorBarcodeSap), HttpStatus.OK);
										} else {
											String divisionName = d.getName();
											String description = this.descriptionUtil.createDescriptionItem(divisionName, this.umConcentrationRepository, this.itemDTO.getUmcontrations(), 
													this.itemDTO.getCommercialName(), this.itemDTO.getNetContent(), this.itemDTO.getConcentration(), this.itemDTO.getBrandProvider(), 
													this.itemDTO.getProduct(), this.itemDTO.getActiveSubstance(),this.itemDTO.getVariable(), this.itemDTO.getBrandItem(), uuid);
											this.itemDTO.setDescription(description);
											if(description.length() > 40) {
												this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, "Los campos del apartado descripci&oacute;n del articulo componen el campo descripci&oacute;n, que no debe superar los 40 caracteres"), HttpStatus.OK);
											} else {
												Optional<ItemEO> item = Optional.of(this.itemRepository.saveAndFlush(new ItemEO(this.itemDTO)));
												item.ifPresentOrElse((var i)-> {
													this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(true, i.getId(), ResponseMessageVO.THE_OPERATION_HAS_BEEN_PROCESSED), HttpStatus.CREATED);
												}, () -> {
													throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
												});
											}
										}
									}
								});
							});
				}
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} catch(NullPointerException nex) {
			throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
		}
		return this.responseMessage;
	}
	
	@Override
	public ResponseEntity<ItemJSON> searchItem(
			final Integer id,
			final String uuid) 
					throws DBAccessException {
		try {
			Optional<List<ItemEO>> optional = Optional.of(this.itemRepository.searchId(id));
			optional.ifPresentOrElse((var l) -> {
				if (l.size() > 0) {
					l.stream().forEach(
							(i) -> {
								this.item = this.itemUtil.convertToJSON(i.getDivisionEO(), new ItemDTO(i));
							});
					this.responseObject = new ResponseEntity<ItemJSON>(this.item, HttpStatus.OK);
				} else {
					throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
				}
				
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseObject;
	}

	@Override
	public ResponseEntity<List<ItemJSON>> searchItems(
			final Integer id,
			final String uuid) 
					throws DBAccessException {
		try {
			this.responseList = null;
			this.sapUser = null;
			this.type = null;
			this.role = null;
			Optional<List<Object[]>> optional = Optional.ofNullable(this.userRepository.searchUserAndRole(id));
			optional.filter((var l) -> l.size() != 0)
			.ifPresent((var ul) -> {
				ul.forEach((var u) -> {
					this.sapUser = String.valueOf(u[1]);
					this.type = String.valueOf(u[2]);
					this.role = String.valueOf(u[3]);
				});
				Optional<String> optType = Optional.ofNullable(this.type);
				optType.filter((var t) -> t.equals(this.provider)).ifPresent((var v) -> {
					if(this.role.equals(this.aitem)) {
						Optional<List<ItemEO>> list = Optional.ofNullable(this.itemRepository.searchByProvider(this.sapUser));
						this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(this.itemUtil.getItems(list.get(), uuid)), HttpStatus.OK);
					} else {
						this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(), HttpStatus.OK);
					}
				});
                optType.filter((var t) -> t.equals(this.negotiator)).ifPresent((var v) -> {
                	if(this.role.equals(this.autorization)) {
                		Optional<List<ItemEO>> list = Optional.ofNullable(this.itemRepository.searchByNegotiator(this.sapUser));
						List<ItemJSON> result = this.itemUtil.getItems(list.get(), uuid);
						this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(result), HttpStatus.OK);
                	} else if(this.role.equals(this.aitem)) {
						Optional<List<ItemEO>> list = Optional.ofNullable(this.itemRepository.searchByNegotiator(this.sapUser));
		                this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(this.itemUtil.getItems(list.get(), uuid)), HttpStatus.OK);
                	} else if(!this.role.equals(this.aitem)) {
                		Optional<List<ItemEO>> list = Optional.ofNullable(this.itemRepository.searchAll());
						this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(this.itemUtil.getItems(list.get(), uuid)), HttpStatus.OK);
                	} else {
                		this.responseList = new ResponseEntity<List<ItemJSON>>(new ArrayList<>(), HttpStatus.OK);
                	}
				}); 
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseList;
	}
	
	@Override
	public ResponseEntity<ItemUpdateJSON> searchItemUpdate(
			final Integer id,
			final String uuid) 
					throws DBAccessException {
		try {
			Optional<List<ItemEO>> optional = Optional.of(this.itemRepository.searchId(id));
			optional.ifPresentOrElse((var l) -> {
				if (l.size() > 0) {
					l.stream().forEach(
							(i) -> {
								this.itemUpdate = this.itemUtil.convertToJSONUpdate(new ItemDTO(i));
							});
					this.responseUpdateObject = new ResponseEntity<ItemUpdateJSON>(this.itemUpdate, HttpStatus.OK);
				} else {
					throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
				}
				
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseUpdateObject;
	}
	
	@Override
	public ResponseEntity<ResponseMessageDTO> updateItem(final ItemUpdateJSON itemJSON, final String uuid)
			throws DBAccessException {
		try {
			this.itemDTO = null;
			this.itemDTO = this.itemUtil.convertToDTO(itemJSON);
			Optional.ofNullable(this.descriptionUtil.getDivision(this.divisionRepository,
					this.itemDTO.getDivisionDTO().getId(), uuid)).ifPresent((var d) -> {
						String divisionName = d.getName();
						Optional<String> optDivisoin = Optional.ofNullable(divisionName);
						optDivisoin.filter((var dn) -> dn != null).ifPresent((var di) -> {
							String description = this.descriptionUtil.createDescriptionItem(divisionName,
									this.umConcentrationRepository, this.itemDTO.getUmcontrations(),
									this.itemDTO.getCommercialName(), this.itemDTO.getNetContent(),
									this.itemDTO.getConcentration(), this.itemDTO.getBrandProvider(),
									this.itemDTO.getProduct(), this.itemDTO.getActiveSubstance(),
									this.itemDTO.getVariable(), this.itemDTO.getBrandItem(), uuid);
							this.itemDTO.setDescription(description);
						});
						optDivisoin.filter((var dn) -> dn.isEmpty()).ifPresent((var di) -> {
							this.itemDTO.setDescription("");
						});
						if(this.itemDTO.getDescription().length() > 40) {
							this.responseMessage = new ResponseEntity<ResponseMessageDTO>(new ResponseMessageDTO(false, 0, "Los campos del apartado descripci&oacute;n del articulo componen el campo descripci&oacute;n, que no debe superar los 40 caracteres"), HttpStatus.OK);
						} else {
							Optional.ofNullable(this.itemDTO.getBarcodes()).filter((var b) -> b.size() != 0)
							.ifPresent((var bcs) -> {
								if (this.barcodeUtil.isValidBarcodeSap(restTemplateUtil, bcs, uuid)) {
									this.responseMessage = new ResponseEntity<ResponseMessageDTO>(
											new ResponseMessageDTO(false, 0, this.errorBarcodeSap), HttpStatus.OK);
								} else {
									ItemEO itemEO = new ItemEO(this.itemDTO);
									Optional<Integer> optItem = Optional
											.of(this.updateItemRepository.updateItem(itemEO));
									optItem.ifPresentOrElse((var r) -> {
										if (r > 0) {
											if (this.updateItemUtil.deleteRelationsItem(this.itemRepository,
													this.itemDTO, this.updateItemRepository, uuid)) {
												if (this.updateItemUtil.createRelationsItem(
														new ItemEO(this.itemDTO), this.updateItemRepository,
														uuid)) {
													this.responseMessage = new ResponseEntity<ResponseMessageDTO>(
															new ResponseMessageDTO(true, this.itemDTO.getId(),
																	ResponseMessageVO.THE_OPERATION_HAS_BEEN_PROCESSED),
															HttpStatus.OK);
												} else {
													throw new DBResultQueryException(
															ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED,
															uuid);
												}
											} else {
												throw new DBResultQueryException(
														ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
											}
										} else {
											throw new DBResultQueryException(
													ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
										}
									}, () -> {
										throw new DBResultQueryException(
												ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
									});
								}
							});
						}
					});
		} catch (DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseMessage;
	}
	
	public ResponseEntity<ResponseMessageDTO> deleteItem(
			final Integer id, 
			final String uuid) 
					throws DBAccessException {
		try {
			Optional<List<ItemEO>> optional = Optional.of(this.itemRepository.searchId(id));
			optional.ifPresentOrElse((var l) -> {
				if (l.size() > 0) {
					l.stream().forEach(
							(i) -> {
								if(this.updateItemRepository.deleteItem(id) > 0) {
									Optional.ofNullable(i.getBarcodes())
									.filter((var bc) -> bc.size() > 0)
									.ifPresent((var bci) -> {
										bci.forEach((var b) -> {
											this.updateItemRepository.deleteItemBarcode(b.getId(), id);
										});
									});
									Optional.ofNullable(i.getActiveSubstances())
									.filter((var as) -> as.size() > 0)
									.ifPresent((var asi) -> {
										asi.forEach((var a) -> {
											this.updateItemRepository.deleteItemActiveSubstance(a.getId(), id);
										});
									});
									Optional.ofNullable(i.getConcentrations())
									.filter((var c) -> c.size() > 0)
									.ifPresent((var cum) -> {
										cum.forEach((var um) -> {
											this.updateItemRepository.deleteItemUMConcentration(um.getId(), id);
										});
									});
								} else {
									throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
								}
							});
					this.responseObject = new ResponseEntity<ItemJSON>(this.item, HttpStatus.OK);
				} else {
					throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
				}
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseMessage;
	}

}