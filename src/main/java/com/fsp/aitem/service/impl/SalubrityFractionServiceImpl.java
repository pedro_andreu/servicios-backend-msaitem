/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.SalubrityFractionDTO;
import com.fsp.aitem.repository.SalubrityFractionRepository;
import com.fsp.aitem.service.SalubrityFractionService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type interface responsible for performing the logic for the transactions of a salubrity fraction
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see SalubrityFractionService
 * @see salubrityFractionRepository
 * @see SalubrityFractionDTO
 * @see ResponseEntity
 */
@Service("salubrityFractionService")
public class SalubrityFractionServiceImpl implements SalubrityFractionService {
	
	@Autowired
	@Qualifier("salubrityFractionRepository")
	private SalubrityFractionRepository salubrityFractionRepository;
	@Autowired
	@Qualifier("salubrityFractionDTO")
	private SalubrityFractionDTO salubrityFractionDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all salubrity fraction
	 * @param String UUI
	 * @return ResponseEntity which contains the all salubrity fractions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see Object
	 * @see SalubrityFractionDTO
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllSalubrityFractions(String uuid) throws DBAccessException, DBResultQueryException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.salubrityFractionRepository.search());
			List<SalubrityFractionDTO> salubrityFractions = new LinkedList<SalubrityFractionDTO>();
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var sf) -> {
							this.salubrityFractionDTO = new SalubrityFractionDTO();
							this.salubrityFractionDTO.setId(Integer.parseInt(String.valueOf(sf[0])));
							this.salubrityFractionDTO.setName(String.valueOf(sf[1]));		
							salubrityFractions.add(this.salubrityFractionDTO);
						});
			    this.response = new ResponseEntity<List<SalubrityFractionDTO>>(new ArrayList<>(salubrityFractions), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}