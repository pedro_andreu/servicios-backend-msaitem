/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.ActiveSubstanceTypeDTO;
import com.fsp.aitem.repository.ActiveSubstanceTypeRepository;
import com.fsp.aitem.service.ActiveSubstanceTypeService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a active substances types
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ActiveSubstanceTypeService
 * @see ActiveSubstanceTypeDTO
 * @see ActiveSubstanceTypeRepository
 * @see ResponseEntity
 */
@Service("activeSubstanceTypeService")
public class ActiveSubstanceTypeServiceImpl implements ActiveSubstanceTypeService {
	
	@Autowired
	@Qualifier("activeSubstanceTypeRepository")
	private ActiveSubstanceTypeRepository activeSubstanceTypeRepository;
	@Autowired
	@Qualifier("activeSubstanceTypeDTO")
	private ActiveSubstanceTypeDTO activeSubstanceTypeDTO;
	
	private ResponseEntity<List<ActiveSubstanceTypeDTO>> response = null;

	/**
	 * Method responsible for performing the logic to search for all active substances types
	 * @param String UUI
	 * @return ResponseEntity which contains the all active substances and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see ActiveSubstanceTypeDTO
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchActiveSubstancesTypes(
			final String letter,
			final String uuid) 
					throws DBAccessException {
		try {
			List<ActiveSubstanceTypeDTO> activeSubstancesTypes = new LinkedList<ActiveSubstanceTypeDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.activeSubstanceTypeRepository.searchByLetter(letter));
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var a) -> {
							this.activeSubstanceTypeDTO = new ActiveSubstanceTypeDTO();
							this.activeSubstanceTypeDTO.setId(Integer.parseInt(String.valueOf(a[0])));		
							this.activeSubstanceTypeDTO.setName(String.valueOf(a[1]));		
							activeSubstancesTypes.add(this.activeSubstanceTypeDTO);
						});
				this.response = new ResponseEntity<List<ActiveSubstanceTypeDTO>>(new ArrayList<>(activeSubstancesTypes), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}