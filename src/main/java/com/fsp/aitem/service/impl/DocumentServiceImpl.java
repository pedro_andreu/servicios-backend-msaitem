package com.fsp.aitem.service.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fsp.aitem.dto.DocumentDTO;
import com.fsp.aitem.eo.DocumentEO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.json.DocumentJSON;
import com.fsp.aitem.json.ItemDocumentsJSON;
import com.fsp.aitem.repository.DocumentRepository;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.aitem.service.DocumentService;
import com.fsp.aitem.util.AWSUtil;
import com.fsp.aitem.util.BarcodeUtil;
import com.fsp.aitem.util.DocumentUtil;
import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("documentService")
public class DocumentServiceImpl implements DocumentService {
	
	@Autowired
	@Qualifier("documentRepository")
	private DocumentRepository documentRepository;
	@Autowired
	@Qualifier("itemRepository")
	private ItemRepository itemRepository;
	
	@Autowired
	@Qualifier("documentUtil")
	private DocumentUtil documentUtil;
	@Autowired
	@Qualifier("barcodeUtil")
	private BarcodeUtil barcodeUtil;
	@Autowired
	@Qualifier("awsUtil")
	private AWSUtil awsUtil;
	
	@Value("${doc.content.type}")
	private String docContent;
	@Value("${item.barcode.unit}")
	private String unit;
	
	private ResponseEntity<ResponseIdDTO> responseId = null;
	private String barcode = null;
	private ResponseEntity<ItemDocumentsJSON> responseObject = null;

	@Override
	public ResponseEntity<ResponseIdDTO> addDoc(final Integer id, final String name1, final MultipartFile multipartFile1, 
			final String name2, final MultipartFile multipartFile2, final String name3,final MultipartFile multipartFile3, 
			final String name4, final MultipartFile multipartFile4, final String name5, final MultipartFile multipartFile5, 
			final String name6, final MultipartFile multipartFile6, final String name7, final MultipartFile multipartFile7, 
			final String name8, final MultipartFile multipartFile8, final String name9, final MultipartFile multipartFile9, 
			final String name10, final MultipartFile multipartFile10, final String uuid) 
					throws DBAccessException {
		try {
			List<DocumentDTO> docs = new LinkedList<DocumentDTO>();
			Optional.ofNullable(multipartFile1).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name1, multipartFile1));
			});
			Optional.ofNullable(multipartFile2).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name2, multipartFile2));
			});
			Optional.ofNullable(multipartFile3).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name3, multipartFile3));
			});
			Optional.ofNullable(multipartFile4).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name4, multipartFile4));
			});
			Optional.ofNullable(multipartFile5).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name5, multipartFile5));
			});
			Optional.ofNullable(multipartFile6).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name6, multipartFile6));
			});
			Optional.ofNullable(multipartFile7).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name7, multipartFile7));
			});
			Optional.ofNullable(multipartFile8).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name8, multipartFile8));
			});
			Optional.ofNullable(multipartFile9).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name9, multipartFile9));
			});
			Optional.ofNullable(multipartFile10).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				docs.add(new DocumentDTO(name10, multipartFile10));
			});
			this.barcode = this.barcodeUtil.isValidBarcode(this.itemRepository, id, uuid);
            if(null != this.barcode) {
            	log.info("Barcode: " + this.barcode);
            	log.info("Documents: " + docs.size());
            	if(!docs.isEmpty()) {
            		docs.forEach((i) -> {
    					String ext = this.documentUtil.getFileExtension(i.getFile());
    					int count = 0;
    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.docContent, this.barcode + "/docs", uuid);
    					if(uri != null) {
    						this.documentRepository.saveAndFlush(new DocumentEO(id, i.getName() + "." + ext, uri));
    					}
    				});
            	}
            	this.itemRepository.updateCompletedById(id, true, new Date());
				this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(true, id), HttpStatus.CREATED);
            } else {
            	this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(false, id), HttpStatus.NOT_FOUND);
            }
		} catch(Exception ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseId;
	}

	@Override
	public ResponseEntity<ResponseIdDTO> updateDoc(final Integer id, final Integer id1, final String name1, final MultipartFile multipartFile1,
			final Integer id2, final String name2, final MultipartFile multipartFile2, final Integer id3, final String name3,
			final MultipartFile multipartFile3, final Integer id4, final String name4, final MultipartFile multipartFile4, final Integer id5,
			final String name5, final MultipartFile multipartFile5, final Integer id6, String name6, final MultipartFile multipartFile6,
			final Integer id7, final String name7, final MultipartFile multipartFile7, final Integer id8, final String name8,
			final MultipartFile multipartFile8, final Integer id9, final String name9, final MultipartFile multipartFile9, final Integer id10,
			final String name10, final MultipartFile multipartFile10, final String uuid) 
					throws DBAccessException {
		try {
			List<DocumentDTO> documentsUpdate = new LinkedList<DocumentDTO>();
			List<DocumentDTO> documentsInsert = new LinkedList<DocumentDTO>();
			Optional.ofNullable(multipartFile1).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id1 == null) {
					documentsInsert.add(new DocumentDTO(name1, multipartFile1));
				} else {
					documentsUpdate.add(new DocumentDTO(id1, name1, multipartFile1));
				}
			});
			Optional.ofNullable(multipartFile2).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id2 == null) {
					documentsInsert.add(new DocumentDTO(name2, multipartFile2));
				} else {
					documentsUpdate.add(new DocumentDTO(id2, name2, multipartFile2));
				}
			});
			Optional.ofNullable(multipartFile3).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id3 == null) {
					documentsInsert.add(new DocumentDTO(name3, multipartFile3));
				} else {
					documentsUpdate.add(new DocumentDTO(id3, name3, multipartFile3));
				}
			});
			Optional.ofNullable(multipartFile4).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id4 == null) {
					documentsInsert.add(new DocumentDTO(name4, multipartFile4));
				} else {
					documentsUpdate.add(new DocumentDTO(id4, name4, multipartFile4));
				}
			});
			Optional.ofNullable(multipartFile5).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id5 == null) {
					documentsInsert.add(new DocumentDTO(name5, multipartFile5));
				} else {
					documentsUpdate.add(new DocumentDTO(id5, name5, multipartFile5));
				}
			});
			Optional.ofNullable(multipartFile6).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id6 == null) {
					documentsInsert.add(new DocumentDTO(name6, multipartFile6));
				} else {
					documentsUpdate.add(new DocumentDTO(id6, name6, multipartFile6));
				}
			});
			Optional.ofNullable(multipartFile7).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id7 == null) {
					documentsInsert.add(new DocumentDTO(name7, multipartFile7));
				} else {
					documentsUpdate.add(new DocumentDTO(id7, name7, multipartFile7));
				}
			});
			Optional.ofNullable(multipartFile8).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id8 == null) {
					documentsInsert.add(new DocumentDTO(name8, multipartFile8));
				} else {
					documentsUpdate.add(new DocumentDTO(id8, name8, multipartFile8));
				}
			});
			Optional.ofNullable(multipartFile9).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id9 == null) {
					documentsInsert.add(new DocumentDTO(name9, multipartFile9));
				} else {
					documentsUpdate.add(new DocumentDTO(id9, name9, multipartFile9));
				}
			});
			Optional.ofNullable(multipartFile10).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id10 == null) {
					documentsInsert.add(new DocumentDTO(name10, multipartFile10));
				} else {
					documentsUpdate.add(new DocumentDTO(id10, name10, multipartFile10));
				}
			});
			this.barcode = this.barcodeUtil.isValidBarcode(this.itemRepository, id, uuid);
			log.info("Barcode: " + this.barcode);
        	log.info("Insert Documents: " + documentsInsert.size());
        	log.info("Update Documents: " + documentsUpdate.size());
			if(null != this.barcode) {
				if(!documentsInsert.isEmpty()) {
					if(!documentsInsert.isEmpty()) {
						documentsInsert.forEach((i) -> {
							log.info(i.getName());
							String ext = this.documentUtil.getFileExtension(i.getFile());
	    					int count = 0;
	    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.docContent, this.barcode + "/docs", uuid);
	    					if(uri != null) {
	    						this.documentRepository.saveAndFlush(new DocumentEO(id, i.getName() + "." + ext, uri));
	    	    			}
						});
					}
				} 
				if (!documentsUpdate.isEmpty()) {
					documentsUpdate.forEach((i) -> {
						log.info(i.getName());
						String ext = this.documentUtil.getFileExtension(i.getFile());
						int count = 0;
    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.docContent, this.barcode + "/docs", uuid);
    					if(uri != null) {
    						this.documentRepository.updateDocumentById(i.getId(), i.getName()  + "." + ext, uri);
    	    			}
					});
				}
				this.itemRepository.updateCompletedById(id, true, new Date());
				this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(true, id), HttpStatus.CREATED);
			} else {
				this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(false, id), HttpStatus.NOT_FOUND);
			}
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseId;
	}

	@Override
	public ResponseEntity<ItemDocumentsJSON> searchDocumentByIdItem(
			final Integer idItem, 
			final String uuid) 
					throws DBAccessException {
		try {
			ItemDocumentsJSON itemDocumentsJSON = new ItemDocumentsJSON();
			Optional<List<Object[]>> optional = Optional.of(this.documentRepository.searchDocumentById(idItem));
			optional.ifPresentOrElse((var l) -> {
				List<DocumentJSON> documents =  new LinkedList<DocumentJSON>();
				l.stream().forEach(
						(im) -> {
							String name = String.valueOf(im[1]);
							int position = name.indexOf('.');
							String newName = name.substring(0,position);
							DocumentJSON document = new DocumentJSON(Integer.parseInt( String.valueOf(im[0])), newName, String.valueOf(im[2]));
							documents.add(document);
						});
				itemDocumentsJSON.setDocuments(documents);
				Optional<ItemEO> item = this.itemRepository.findById(idItem);
				item.ifPresent(
						(it) -> {
							it.getBarcodes().forEach((b) -> {
								Optional.of(b.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
									itemDocumentsJSON.setBarcode(b.getBarcode());
								});
							});
							itemDocumentsJSON.setId(it.getId());
				        });
				this.responseObject = new ResponseEntity<ItemDocumentsJSON>(itemDocumentsJSON, HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(Exception ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseObject;
	}

	@Override
	public ResponseEntity<ResponseIdDTO> deleteDocuments(
			final Integer idDocument, 
			final String uuid) 
					throws DBAccessException {
		try {
			List<Object[]> images = this.documentRepository.getDocument(idDocument);
		    if(!images.isEmpty()) {
		    	images.forEach((var i) -> {
		    		boolean result = this.awsUtil.deleteObject(String.valueOf(i[1]), String.valueOf(i[2]) + "/docs", String.valueOf(i[2]), uuid);
		    		if(result) {
		    			this.documentRepository.deleteById(idDocument);
		    			this.responseId = new ResponseEntity<>(new ResponseIdDTO(true, 0), HttpStatus.OK);
		    		} else {
		    			throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
				    }
			    });
		} else {
			throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
		}
	} catch(DataAccessException ex) {
		throw new DBAccessException(ex, uuid);
	}
	return this.responseId;
}

}