/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.DivisionDTO;
import com.fsp.aitem.repository.DivisionRepository;
import com.fsp.aitem.service.DivisionService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a division
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see DivisionService
 * @see DivisionDTO
 * @see ResponseEntity
 */
@Service("divisionService")
public class DivisionServiceImpl implements DivisionService {
	
	@Autowired
	@Qualifier("divisionRepository")
	private DivisionRepository divisionRepository;
	@Autowired
	@Qualifier("divisionDTO")
	private DivisionDTO divisionDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all divisions
	 * @param long id for the division
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see DivisionDTO
	 * @see Object
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllDivisions(final String uuid) 
			throws DBAccessException, DBResultQueryException {
		try {
			List<DivisionDTO> divisions = new LinkedList<DivisionDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.divisionRepository.search());
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var d) -> {
							this.divisionDTO = new DivisionDTO();
							this.divisionDTO.setId(Integer.parseInt(String.valueOf(d[0])));		
							this.divisionDTO.setName(String.valueOf(d[1]));		
							divisions.add(this.divisionDTO);
						});
				this.response = new ResponseEntity<List<DivisionDTO>>(new ArrayList<>(divisions), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}
	
}