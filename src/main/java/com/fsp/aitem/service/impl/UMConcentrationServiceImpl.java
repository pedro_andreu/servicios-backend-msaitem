/*
 * Copyright Farmacias San Pablo
 * 23-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.UMConcentrationTypeDTO;
import com.fsp.aitem.repository.UMConcentrationRepository;
import com.fsp.aitem.service.UMConcentrationService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type interface responsible for performing the logic for the transactions of a um concentrations
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see UMConcentrationService
 * @see UMConcentrationRepository
 * @see UMConcentrationTypeDTO
 * @see ResponseEntity
 * @see List
 */
@Service("umConcentrationService")
public class UMConcentrationServiceImpl implements UMConcentrationService {
	
	@Autowired
	@Qualifier("umConcentrationRepository")
	private UMConcentrationRepository umConcentrationRepository;
	@Autowired
	@Qualifier("umConcentrationTypeDTO")
	private UMConcentrationTypeDTO umConcentrationType;
	
	private ResponseEntity<List<UMConcentrationTypeDTO>> response = null;

	/**
	 * Method responsible for performing the logic to search for all um concentrations of type presentation
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see UMConcentrationDTO
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@Override
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllPresentations(final String uuid)
			throws DBAccessException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.umConcentrationRepository.searchPresentations());
			this.response = getConcentrations(uuid, optional);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

	/**
	 * Method responsible for performing the logic to search for all um concentrations of type farmaceutical form
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see UMConcentrationDTO
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@Override
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllFarmaceuticalForms(final String uuid)
			throws DBAccessException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.umConcentrationRepository.searchFarmaceuticalForm());
			this.response = getConcentrations(uuid, optional);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

	/**
	 * Method responsible for performing the logic to search for all um concentrations of um net content
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see UMConcentrationDTO
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@Override
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllNetContens(final String uuid)
			throws DBAccessException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.umConcentrationRepository.searchUmNetContent());
			this.response = getConcentrations(uuid, optional);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

	/**
	 * Method responsible for performing the logic to search for all um concentrations of concentrations
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see UMConcentrationDTO
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@Override
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllConcentrations(final String uuid)
			throws DBAccessException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.umConcentrationRepository.searchUmConcentration());
			this.response = getConcentrations(uuid, optional);
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}
	
	/**
	 * Method responsible for performing the iteration and validation of an optional object
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see String
	 * @see Optional
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see UMConcentrationDTO
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UMConcentrationTypeDTO>> getConcentrations(final String uuid, final Optional<List<Object[]>> optional) 
			throws DBAccessException, DBResultQueryException {
		try {
			List<UMConcentrationTypeDTO> umConcentrations = new LinkedList<UMConcentrationTypeDTO>();
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(d) -> {
							this.umConcentrationType = new UMConcentrationTypeDTO();
							this.umConcentrationType.setId(Integer.parseInt(String.valueOf(d[0])));		
							this.umConcentrationType.setUnitMeasured(String.valueOf(d[1]));		
							umConcentrations.add(this.umConcentrationType);
						});
				this.response = new ResponseEntity<List<UMConcentrationTypeDTO>>(new ArrayList<>(umConcentrations), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}