/*
 * Copyright Farmacias San Pablo
 * 22-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.ItemTypeDTO;
import com.fsp.aitem.repository.ItemTypeRepository;
import com.fsp.aitem.service.ItemTypeService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type interface responsible for performing the logic for the transactions of a item types
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ItemTypeService
 * @see ItemTypeRepository
 * @see ItemTypeDTO
 * @see ResponseEntity
 */
@Service("itemTypeService")
public class ItemTypeServiceImpl implements ItemTypeService {
	
	@Autowired
	@Qualifier("itemTypeRepository")
	private ItemTypeRepository itemTypeRepository;
	@Autowired
	@Qualifier("itemTypeDTO")
	private ItemTypeDTO itemTypeDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all item types
	 * @param String UUI
	 * @return ResponseEntity which contains the all item types and code HTTP
	 * @see ResponseEntity
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see Object
	 * @see ItemTypeDTO
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllItemTypes(final String uuid) 
			throws DBAccessException, DBResultQueryException {
		try {
			Optional<List<Object[]>> optional = Optional.of(this.itemTypeRepository.search());
			List<ItemTypeDTO> itemTypes = new LinkedList<ItemTypeDTO>();
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var it) -> {
							this.itemTypeDTO = new ItemTypeDTO();
							this.itemTypeDTO.setId(Integer.parseInt(String.valueOf(it[0])));
							this.itemTypeDTO.setName(String.valueOf(it[1]));		
							itemTypes.add(this.itemTypeDTO);
						});
			    this.response = new ResponseEntity<List<ItemTypeDTO>>(new ArrayList<>(itemTypes), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}