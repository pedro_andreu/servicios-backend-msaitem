package com.fsp.aitem.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fsp.aitem.dto.ImageDTO;
import com.fsp.aitem.eo.ImageEO;
import com.fsp.aitem.eo.ItemEO;
import com.fsp.aitem.json.ImageJSON;
import com.fsp.aitem.json.ItemImagesJSON;
import com.fsp.aitem.repository.ImageRepository;
import com.fsp.aitem.repository.ItemRepository;
import com.fsp.aitem.service.ImageService;
import com.fsp.aitem.util.AWSUtil;
import com.fsp.aitem.util.BarcodeUtil;
import com.fsp.aitem.util.DocumentUtil;
import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("imageService")
public class ImageServiceImpl implements ImageService {
	
	@Autowired
	@Qualifier("imageRepository")
	private ImageRepository imageRepository;
	@Autowired
	@Qualifier("itemRepository")
	private ItemRepository itemRepository;
	
	@Autowired
	@Qualifier("documentUtil")
	private DocumentUtil documentUtil;
	@Autowired
	@Qualifier("barcodeUtil")
	private BarcodeUtil barcodeUtil;
	@Autowired
	@Qualifier("awsUtil")
	private AWSUtil awsUtil;
	
	@Value("${image.content.type}")
	private String imgContent;
	@Value("${item.barcode.unit}")
	private String unit;

	private ResponseEntity<ResponseIdDTO> responseId = null;
	private ResponseEntity<ItemImagesJSON> responseObject = null;
	
	private String barcode = null;

	@Override
	public ResponseEntity<ResponseIdDTO> addImage(final Integer id, final String name1, final MultipartFile multipartFile1, 
			final String name2, final MultipartFile multipartFile2, final String name3, final MultipartFile multipartFile3,
			final String name4, final MultipartFile multipartFile4, final String name5, final MultipartFile multipartFile5,
			final String name6, final MultipartFile multipartFile6, final String name7, final MultipartFile multipartFile7,
			final String name8, final MultipartFile multipartFile8, final String name9, final MultipartFile multipartFile9,
			final String name10, final MultipartFile multipartFile10, String uuid) throws DBAccessException {
		try {
			List<ImageDTO> images = new LinkedList<ImageDTO>();
			Optional.ofNullable(multipartFile1).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name1, multipartFile1));
			});
			Optional.ofNullable(multipartFile2).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name2, multipartFile2));
			});
			Optional.ofNullable(multipartFile3).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name3, multipartFile3));
			});
			Optional.ofNullable(multipartFile4).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name4, multipartFile4));
			});
			Optional.ofNullable(multipartFile5).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name5, multipartFile5));
			});
			Optional.ofNullable(multipartFile6).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name6, multipartFile6));
			});
			Optional.ofNullable(multipartFile7).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name7, multipartFile7));
			});
			Optional.ofNullable(multipartFile8).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name8, multipartFile8));
			});
			Optional.ofNullable(multipartFile9).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name9, multipartFile9));
			});
			Optional.ofNullable(multipartFile10).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				images.add(new ImageDTO(name10, multipartFile10));
			});
            this.barcode = this.barcodeUtil.isValidBarcode(this.itemRepository, id, uuid);
            if(null != this.barcode) {
            	log.info("Barcode: " + this.barcode);
            	log.info("Documents: " + images.size());
            	if(!images.isEmpty()) {
            		images.forEach((i) -> {
    					String ext = this.documentUtil.getFileExtension(i.getFile());
    					int count = 0;
    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.imgContent, this.barcode + "/images", uuid);
    					if(uri != null) {
    						this.imageRepository.saveAndFlush(new ImageEO(id, i.getName() + "." + ext, uri));
    					}
    				});	
            		
            	}
            	this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(true, id), HttpStatus.CREATED);
            } else {
            	this.responseId = new ResponseEntity<ResponseIdDTO>(new ResponseIdDTO(false, id), HttpStatus.NOT_FOUND);
            }
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.responseId;
	}
	
	@Override
	public ResponseEntity<ResponseIdDTO> modifyImage(final Integer id, final Integer id1, final String name1, final MultipartFile multipartFile1, 
			final Integer id2, final String name2, final MultipartFile multipartFile2, final Integer id3, final String name3, final MultipartFile multipartFile3,
			final Integer id4, final String name4, final MultipartFile multipartFile4, final Integer id5, final String name5, final MultipartFile multipartFile5,
			final Integer id6, final String name6, final MultipartFile multipartFile6, final Integer id7, final String name7, final MultipartFile multipartFile7,
			final Integer id8, final String name8, final MultipartFile multipartFile8, final Integer id9, final String name9, final MultipartFile multipartFile9,
			final Integer id10, final String name10, final MultipartFile multipartFile10, String uuid) throws DBAccessException {
		try {
			List<ImageDTO> imagesUpdate = new LinkedList<ImageDTO>();
			List<ImageDTO> imagesInsert = new LinkedList<ImageDTO>();
			Optional.ofNullable(multipartFile1).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id1 == null) {
					imagesInsert.add(new ImageDTO(name1, multipartFile1));
				} else {
					imagesUpdate.add(new ImageDTO(id1, name1, multipartFile1));
				}
			});
			Optional.ofNullable(multipartFile2).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id2 == null) {
					imagesInsert.add(new ImageDTO(name2, multipartFile2));
				} else {
					imagesUpdate.add(new ImageDTO(id2, name2, multipartFile2));
				}
			});
			Optional.ofNullable(multipartFile3).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id3 == null) {
					imagesInsert.add(new ImageDTO(name3, multipartFile3));
				} else {
					imagesUpdate.add(new ImageDTO(id3, name3, multipartFile3));
				}
			});
			Optional.ofNullable(multipartFile4).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id4 == null) {
					imagesInsert.add(new ImageDTO(name4, multipartFile4));
				} else {
					imagesUpdate.add(new ImageDTO(id4, name4, multipartFile4));
				}
			});
			Optional.ofNullable(multipartFile5).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id5 == null) {
					imagesInsert.add(new ImageDTO(name5, multipartFile5));
				} else {
					imagesUpdate.add(new ImageDTO(id5, name5, multipartFile5));
				}
			});
			Optional.ofNullable(multipartFile6).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id6 == null) {
					imagesInsert.add(new ImageDTO(name6, multipartFile6));
				} else {
					imagesUpdate.add(new ImageDTO(id6, name6, multipartFile6));
				}
			});
			Optional.ofNullable(multipartFile7).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id7 == null) {
					imagesInsert.add(new ImageDTO(name7, multipartFile7));
				} else {
					imagesUpdate.add(new ImageDTO(id7, name7, multipartFile7));
				}
			});
			Optional.ofNullable(multipartFile8).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id8 == null) {
					imagesInsert.add(new ImageDTO(name8, multipartFile8));
				} else {
					imagesUpdate.add(new ImageDTO(id8, name8, multipartFile8));
				}
			});
			Optional.ofNullable(multipartFile9).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id9 == null) {
					imagesInsert.add(new ImageDTO(name9, multipartFile9));
				} else {
					imagesUpdate.add(new ImageDTO(id9, name9, multipartFile9));
				}
			});
			Optional.ofNullable(multipartFile10).filter((var n) -> !n.isEmpty()).ifPresent((var n) -> {
				if(id10 == null) {
					imagesInsert.add(new ImageDTO(name10, multipartFile10));
				} else {
					imagesUpdate.add(new ImageDTO(id10, name10, multipartFile10));
				}
			});
			this.barcode = this.barcodeUtil.isValidBarcode(this.itemRepository, id, uuid);
            if(null != this.barcode) {
            	log.info("Barcode: " + this.barcode);
            	log.info("Insert Images: " + imagesInsert.size());
            	log.info("Update Images: " + imagesUpdate.size());
            	if(!imagesInsert.isEmpty()) {
            		imagesInsert.forEach((i) -> {
    					String ext = this.documentUtil.getFileExtension(i.getFile());
    					int count = 0;
    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.imgContent, this.barcode + "/images", uuid);
    					if(uri != null) {
    						this.imageRepository.saveAndFlush(new ImageEO(id, i.getName() + "." + ext, uri));
    					}
    				});	
            	}
            	if(!imagesUpdate.isEmpty()) {
            		imagesUpdate.forEach((i) -> {
    					String ext = this.documentUtil.getFileExtension(i.getFile());
    					int count = 0;
    					String uri = this.awsUtil.upload(count++, this.documentUtil.createFile(i.getFile()), i.getName(), "." + ext, this.imgContent, this.barcode + "/images", uuid);
    					if(uri != null) {
    						this.imageRepository.updateById(i.getId(), i.getName() + "." + ext, uri);
    					}
    				});	
            	}
				this.responseId = new ResponseEntity<>(new ResponseIdDTO(true, id), HttpStatus.OK);
            } else {
            	this.responseId = new ResponseEntity<>(new ResponseIdDTO(false, id), HttpStatus.NOT_FOUND);
            }	
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseId;
	}
	
	@Override
	public ResponseEntity<ItemImagesJSON> searchImageByIdItem(final Integer idItem, final String uuid) throws DBAccessException {
		try {
			ItemImagesJSON itemImagesJSON = new ItemImagesJSON();
			Optional<List<Object[]>> optional = Optional.of(this.imageRepository.searchByItemId(idItem));
			optional.ifPresentOrElse((var l) -> {
				List<ImageJSON> images =  new LinkedList<ImageJSON>();
				l.stream().forEach(
						(im) -> {					
							String name = String.valueOf(im[1]);
							int position = name.indexOf('.');
							String newName = name.substring(0,position);
							ImageJSON image = new ImageJSON(Integer.parseInt( String.valueOf(im[0])), newName, String.valueOf(im[2]));
							images.add(image);
						});
				itemImagesJSON.setImages(images);
				Optional<ItemEO> item = this.itemRepository.findById(idItem);
				item.ifPresent(
						(it) -> {
							it.getBarcodes().forEach((b) -> {
								Optional.of(b.getType()).filter((t) -> t.equals(this.unit)).ifPresent((var p) -> {
									itemImagesJSON.setBarcode(b.getBarcode());
								});
							});
							itemImagesJSON.setId(it.getId());
				        });
				this.responseObject = new ResponseEntity<ItemImagesJSON>(itemImagesJSON, HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} catch(StringIndexOutOfBoundsException sibex) {
			throw new DBAccessException(sibex, uuid);
		}
		return this.responseObject;
	}

	@Override
	public ResponseEntity<ResponseIdDTO> deleteImage(
			final Integer idImage, 
			final String uuid) 
					throws DBAccessException {
		try {
			List<Object[]> images = this.imageRepository.getImage(idImage);
			if(!images.isEmpty()) {
				images.forEach((var i) -> {
					boolean result = this.awsUtil.deleteObject(String.valueOf(i[1]), String.valueOf(i[2]) + "/images", String.valueOf(i[2]), uuid);
					if(result) {
						this.imageRepository.deleteById(idImage);
						this.responseId = new ResponseEntity<>(new ResponseIdDTO(true, 0), HttpStatus.OK);
					} else {
						throw new DBResultQueryException(ResponseMessageVO.THE_OPERATION_WAS_NOT_PROCESSED, uuid);
					}
				});
			} else {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			}
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		}
		return this.responseId;
	}

}