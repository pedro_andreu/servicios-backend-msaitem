/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.CountryDTO;
import com.fsp.aitem.repository.CountryRepository;
import com.fsp.aitem.service.CountryService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a active substance
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see CountryService
 * @see CountryDTO
 * @see CountryRepository
 * @see ResponseEntity
 */
@Service("countryService")
public class CountryServiceImpl implements CountryService {

	@Autowired
	@Qualifier("countryRepository")
	private CountryRepository countryRepository;
	@Autowired
	@Qualifier("countryDTO")
	private CountryDTO countryDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all countries
	 * @param String UUI
	 * @return ResponseEntity which contains the all countries and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see CountryDTO
	 * @see Object
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchCountries(
			final String letter,
			final String uuid) 
					throws DBAccessException {
		try {
			List<CountryDTO> countries = new LinkedList<CountryDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.countryRepository.searchByLetter(letter));
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var c) -> {
							this.countryDTO = new CountryDTO();
							this.countryDTO.setId(Integer.parseInt(String.valueOf(c[0])));		
							this.countryDTO.setName(String.valueOf(c[1]));		
							countries.add(this.countryDTO);
						});
				this.response = new ResponseEntity<List<CountryDTO>>(new ArrayList<>(countries), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}