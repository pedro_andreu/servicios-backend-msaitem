/*
 * Copyright Farmacias San Pablo
 * 18-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.UserDTO;
import com.fsp.aitem.repository.UserRepository;
import com.fsp.aitem.service.UserService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type interface responsible for performing the logic for the transactions of a users
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see UserService
 * @see UserRepository
 * @see UserDTO
 * @see ResponseEntity
 * @see List
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;
	@Autowired
	@Qualifier("userDTO")
	private UserDTO userDTO;
	
	@Value("${user.provider}")
	private String provider;
	@Value("${user.negotiator}")
	private String negotiator;
	
	private ResponseEntity<List<UserDTO>> response = null;
	
	/**
	 * Method responsible to perform the logic for user query
	 * @param String type
	 * @param String UUI
	 * @return ResponseEntity which contains the all user and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Object
	 * @see Optional
	 * @see UserDTO
	 * @see String
	 * @see HttpStatus
	 * @see ResponseMessageVO
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @exception DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<List<UserDTO>> searchUserBytype(
			final String type, 
			final String uuid) 
					throws DBAccessException {
		try {
			List<UserDTO> users = new LinkedList<UserDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.userRepository.searchUser(type));
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(d) -> {
							System.out.println("XXX: ");
							this.userDTO = new UserDTO();
							this.userDTO.setId(Integer.parseInt(String.valueOf(d[0])));		
							this.userDTO.setName(String.valueOf(d[1]));		
							this.userDTO.setSecondName(String.valueOf(d[2]));
							this.userDTO.setFirstName(String.valueOf(d[3]));
							this.userDTO.setLastName(String.valueOf(d[4]));
							users.add(this.userDTO);
						});
				this.response = new ResponseEntity<List<UserDTO>>(new ArrayList<>(users), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}
	
	public ResponseEntity<List<UserDTO>> searchUserBytypeAndLetter(
			final String type, 
			final String letter,
			final String uuid) 
					throws DBAccessException {
		try {
			List<UserDTO> users = new LinkedList<UserDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.userRepository.searchUserByLetter(type, letter));
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(d) -> {
							this.userDTO = new UserDTO();
							this.userDTO.setId(Integer.parseInt(String.valueOf(d[0])));		
							this.userDTO.setName(String.valueOf(d[1]));		
							users.add(this.userDTO);
						});
				this.response = new ResponseEntity<List<UserDTO>>(new ArrayList<>(users), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}