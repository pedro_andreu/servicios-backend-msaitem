/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.BrandDTO;
import com.fsp.aitem.repository.BrandRepository;
import com.fsp.aitem.service.BrandService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a brands
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see BrandService
 * @see BrandDTO
 * @see BrandRepository
 * @see ResponseEntity
 */
@Service("brandService")
public class BrandServiceImpl implements BrandService {
	
	@Autowired
	@Qualifier("brandRepository")
	private BrandRepository brandRepository;
	@Autowired
	@Qualifier("brandDTO")
	private BrandDTO brandDTO;
	
	private ResponseEntity<List<BrandDTO>> response = null;

	/**
	 * Method responsible for performing the logic to search for all brands
	 * @param String UUI
	 * @return ResponseEntity which contains the all brands and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see BrandDTO
	 * @see Object
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchBrands(
			final String letter,
			final String uuid) 
					throws DBAccessException {
		try {
			List<BrandDTO> brands = new LinkedList<BrandDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.brandRepository.searchByLetter(letter));
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var b) -> {
							this.brandDTO = new BrandDTO();
							this.brandDTO.setId(Integer.parseInt(String.valueOf(b[0])));		
							this.brandDTO.setName(String.valueOf(b[1]));		
							brands.add(this.brandDTO);
						});
				this.response = new ResponseEntity<List<BrandDTO>>(new ArrayList<>(brands), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}