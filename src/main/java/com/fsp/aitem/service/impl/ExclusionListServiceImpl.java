/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fsp.aitem.dto.ExclusionListDTO;
import com.fsp.aitem.repository.ExclusionListRepository;
import com.fsp.aitem.service.ExclusionListService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Service type class responsible for performing the logic for the transactions of a division
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ExclusionListService
 * @see ExclusionListRepository
 * @see ExclusionListDTO
 * @see ResponseEntity
 */
@Service("exclusionListService")
public class ExclusionListServiceImpl implements ExclusionListService {
	
	@Autowired
	@Qualifier("exclusionListRepository")
	private ExclusionListRepository exclusionListRepository;
	@Autowired
	@Qualifier("exclusionListDTO")
	private ExclusionListDTO exclusionListDTO;
	
	private ResponseEntity<?> response = null;

	/**
	 * Method responsible for performing the logic to search for all exclusions list
	 * @param long id for the exclusions list
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see LinkedList
	 * @see ArrayList
	 * @see Optional
	 * @see ExclusionListDTO
	 * @see Object
	 * @see ResponseMessageVO
	 * @see Exception 
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	@Override
	public ResponseEntity<?> searchAllExclusionList(String uuid) throws DBAccessException, DBResultQueryException {
		try {
			List<ExclusionListDTO> exclusionsList = new LinkedList<ExclusionListDTO>();
			Optional<List<Object[]>> optional = Optional.of(this.exclusionListRepository.search());
			optional.ifPresentOrElse((var l) -> {
				l.stream().forEach(
						(var e) -> {
							this.exclusionListDTO = new ExclusionListDTO();
							this.exclusionListDTO.setId(Integer.parseInt(String.valueOf(e[0])));		
							this.exclusionListDTO.setName(String.valueOf(e[1]));		
							exclusionsList.add(this.exclusionListDTO);
						});
				this.response = new ResponseEntity<List<ExclusionListDTO>>(new ArrayList<>(exclusionsList), HttpStatus.OK);
			}, () -> {
				throw new DBResultQueryException(ResponseMessageVO.NO_RESULTS_FOUND, uuid);
			});
		} catch(DataAccessException ex) {
			throw new DBAccessException(ex, uuid);
		} 
		return this.response;
	}

}