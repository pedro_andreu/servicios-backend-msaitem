package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;

public interface SapService {
	
	public ResponseEntity<List<ResponseIdDTO>> sendItem(
			final List<Integer> items, 
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ResponseIdDTO> searchBarcode(
			final String barcode, 
			final String uuid)
					throws DBAccessException;

}