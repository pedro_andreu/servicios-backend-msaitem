/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.ItemUpdateJSON;
import com.fsp.commonutil.dto.ResponseMessageDTO;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a item
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface ItemService {
	
	public ResponseEntity<ResponseMessageDTO> createDescription(
			final Integer idDivision, final String product, final Integer presentation,
			final String netContent, final Integer umNetContent, final String brandProvider, final String variable,
			final String activeSubstance, final Integer farmaceuticalForm, final String concentration, final Integer umConcentration,
			final String brandItem, final String commercialName, final String uuid) 
			throws DBAccessException;
	
	public ResponseEntity<ResponseMessageDTO> addItem(
			final ItemJSON itemJSON, 
			final Integer userId,
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ItemJSON> searchItem(
			final Integer id, 
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<List<ItemJSON>> searchItems(
			final Integer idUser,
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ItemUpdateJSON> searchItemUpdate(
			final Integer id, 
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ResponseMessageDTO> updateItem(
			final ItemUpdateJSON itemUpdateJSON, 
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ResponseMessageDTO> deleteItem(
			final Integer id, 
			final String uuid) 
					throws DBAccessException;
	
}