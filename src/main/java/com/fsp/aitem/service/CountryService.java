/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.service;

import org.springframework.http.ResponseEntity;

import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a countries
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface CountryService {
	
	/**
	 * Method responsible for performing the logic to search for all countries
	 * @param String UUI
	 * @return ResponseEntity which contains the all countries and code HTTP
	 * @see ResponseEntity
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<?> searchCountries(
			final String letter,
			final String uuid) 
					throws DBAccessException;

}