/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.aitem.dto.SatCodeDTO;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a sat codes
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface SatCodeService {
	
	/**
	 * Method responsible for performing the logic to search for all sat codes
	 * @param String UUI
	 * @return ResponseEntity which contains the all countries and code HTTP
	 * @see ResponseEntity
	 * @see SatCodeDTO
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<SatCodeDTO>> searchSatCode(final String code, final String uuid) throws DBAccessException;

}