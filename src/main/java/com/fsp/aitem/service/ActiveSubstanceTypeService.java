/*
 * Copyright Farmacias San Pablo
 * 16-10-2019
 */
package com.fsp.aitem.service;

import org.springframework.http.ResponseEntity;

import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a active substances types
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface ActiveSubstanceTypeService {
	
	/**
	 * Method responsible for performing the logic to search for all active substances types
	 * @param String UUI
	 * @return ResponseEntity which contains the all active substances and code HTTP
	 * @see ResponseEntity
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<?> searchActiveSubstancesTypes(
			final String letter,
			final String uuid) 
					throws DBAccessException;
	
}