/*
 * Copyright Farmacias San Pablo
 * 18-10-2019
 */
package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.aitem.dto.UserDTO;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a users
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface UserService {
	
	/**
	 * Method responsible to perform the logic for user query
	 * @param String type
	 * @param String UUI
	 * @return ResponseEntity which contains the all user and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see UserDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UserDTO>> searchUserBytype(
			final String type, 
			final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<List<UserDTO>> searchUserBytypeAndLetter(
			final String type, 
			final String letter,
			final String uuid) 
					throws DBAccessException;

}