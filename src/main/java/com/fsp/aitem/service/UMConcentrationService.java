/*
 * Copyright Farmacias San Pablo
 * 23-10-2019
 */
package com.fsp.aitem.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fsp.aitem.dto.UMConcentrationTypeDTO;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Service type interface responsible for performing the logic for the transactions of a um concentrations
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface UMConcentrationService {
	
	/**
	 * Method responsible for performing the logic to search for all um concentrations of a type presentations
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see UMConcentrationTypeDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllPresentations(
			final String uuid) 
					throws DBAccessException;
	
	/**
	 * Method responsible for performing the logic to search for all um concentrations of a type farmaucetical form
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see UMConcentrationTypeDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllFarmaceuticalForms(
			final String uuid) 
					throws DBAccessException;
	
	/**
	 * Method responsible for performing the logic to search for all um concentrations of a type um net content
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see UMConcentrationTypeDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllNetContens(
			final String uuid) 
					throws DBAccessException;
	
	/**
	 * Method responsible for performing the logic to search for all um concentrations of a type concentrations
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @see List
	 * @see UMConcentrationTypeDTO
	 * @see String
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	public ResponseEntity<List<UMConcentrationTypeDTO>> searchAllConcentrations(
			final String uuid) 
					throws DBAccessException;

}