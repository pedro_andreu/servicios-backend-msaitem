/*
 * Copyright Farmacias San Pablo
 * 05-11-2019
 */
package com.fsp.aitem.service;

import org.springframework.http.ResponseEntity;

import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;

/**
 * Service type interface responsible for performing the logic for the transactions of a exclusion list
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public interface ExclusionListService {
	
	/**
	 * Method responsible for performing the logic to search for all exclusion list
	 * @param String UUI
	 * @return ResponseEntity which contains the all divisions and code HTTP
	 * @see ResponseEntity
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 */
	public ResponseEntity<?> searchAllExclusionList(final String uuid) 
			throws DBAccessException, DBResultQueryException;

}