package com.fsp.aitem.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.fsp.aitem.json.ItemImagesJSON;
import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;

public interface ImageService {
	
	public ResponseEntity<ResponseIdDTO> addImage(
			final Integer id, final String name1, final MultipartFile multipartFile1, 
			final String name2, final MultipartFile multipartFile2, final String name3, 
			final MultipartFile multipartFile3, final String name4, final MultipartFile multipartFile4, 
			final String name5, final MultipartFile multipartFile5, final String name6, final MultipartFile multipartFile6, 
			final String name7, final MultipartFile multipartFile7, final String name8, final MultipartFile multipartFile8, 
			final String name9, final MultipartFile multipartFile9, final String name10, final MultipartFile multipartFile10, final String uuid) 
					throws DBAccessException;
	
	public ResponseEntity<ResponseIdDTO> modifyImage(final Integer id, final Integer id1, final String name1, final MultipartFile multipartFile1, 
			final Integer id2, final String name2, final MultipartFile multipartFile2, final Integer id3, final String name3, final MultipartFile multipartFile3,
			final Integer id4, final String name4, final MultipartFile multipartFile4, final Integer id5, final String name5, final MultipartFile multipartFile5,
			final Integer id6, final String name6, final MultipartFile multipartFile6, final Integer id7, final String name7, final MultipartFile multipartFile7,
			final Integer id8, final String name8, final MultipartFile multipartFile8, final Integer id9, final String name9, final MultipartFile multipartFile9,
			final Integer id10, final String name10, final MultipartFile multipartFile10, String uuid) throws DBAccessException;
	
	public ResponseEntity<ItemImagesJSON> searchImageByIdItem(final Integer idItem, final String uuid) throws DBAccessException;
	
	public ResponseEntity<ResponseIdDTO> deleteImage(final Integer idImage, final String uuid) throws DBAccessException;

}