/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import com.fsp.commonutil.dto.LogDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.exception.SAProcessException;
import com.fsp.commonutil.helper.ExceptionHelper;
import com.fsp.commonutil.util.LogUtil;
import com.fsp.commonutil.util.UUIDUtil;
import com.fsp.commonutil.vo.ResponseMessageVO;

/**
 * Controller type class responsible for managing all exceptions that occur during application flows
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ResponseEntityExceptionHandler
 * @see GlobalDTOException
 * @see GlobalLogException
 */
@ControllerAdvice
@RestController
public class GlobalHandlerException extends ResponseEntityExceptionHandler {
	
	private static final Logger log = Logger.getLogger(GlobalHandlerException.class);
	
	@Autowired
	@Qualifier("globalTransportException")
	private GlobalTransportException globalTransportException;
	
	private LogDTO logDto;
	
	/**
	 * Method responsible for processing the exception that is generated when an error occurs in the database
	 * @param Exception ex
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see DBAccessException
	 * @see ResponseEntity
	 * @see Exception
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 */
	@ExceptionHandler({DBAccessException.class, InvalidDataAccessApiUsageException.class, SAProcessException.class})
	public final ResponseEntity<?> handleDataAccessException(
			final DBAccessException ex, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.INTERNAL_SERVER_ERROR.name(), 
				HttpStatus.INTERNAL_SERVER_ERROR.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
		.uuid(ex.geUuid())
		.httpCode(HttpStatus.INTERNAL_SERVER_ERROR.name())
		.errorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
	    return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({java.net.ConnectException.class, java.lang.IllegalStateException.class})
	public final ResponseEntity<?> handleConnectException(
			final Exception ex) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.GATEWAY_TIMEOUT.name(), 
				HttpStatus.GATEWAY_TIMEOUT.value(), 
				"N/A", 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()))
		.uuid("N/A")
		.httpCode(HttpStatus.GATEWAY_TIMEOUT.name())
		.errorCode(HttpStatus.GATEWAY_TIMEOUT.value())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
	    return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.GATEWAY_TIMEOUT);
	}
	
	/**
	 * Method responsible for processing the exception that is generated when an error occurs during a transaction to the database
	 * @param Exception ex
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see DBResultQueryException
	 * @see ResponseEntity
	 * @see Exception
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 */
	@ExceptionHandler(DBResultQueryException.class)
	public final ResponseEntity<Object> handleException(
			final DBResultQueryException ex, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.NOT_FOUND.name(), 
				HttpStatus.NOT_FOUND.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.NOT_FOUND.name())
		.uuid(ex.geUuid())
		.errorCode(HttpStatus.NOT_FOUND.value())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.NOT_FOUND);
	}
	
	/**
	 * Method in charge of processing the exception that is generated when the request parameters are incorrect
	 * @param Exception ex
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see MethodArgumentTypeMismatchException
	 * @see MissingRequestHeaderException
	 * @see InvalidDefinitionException
	 * @see ResponseEntity
	 * @see Object
	 * @see Exception
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 * @see HttpStatus
	 */
	@ExceptionHandler({
		MethodArgumentTypeMismatchException.class,
		MissingRequestHeaderException.class,
		InvalidDefinitionException.class,
		NumberFormatException.class
		})
	public final ResponseEntity<Object> handleDefinitionException(
			final Exception ex, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.BAD_REQUEST.name())
		.errorCode(HttpStatus.BAD_REQUEST.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Method responsible for processing the exception that is generated when the request is not supported
	 * @param HttpRequestMethodNotSupportedException ex
	 * @param HttpHeaders headers
	 * @param HttpStatus status
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see ResponseEntity
	 * @see Object
	 * @see HttpRequestMethodNotSupportedException
	 * @see HttpHeaders
	 * @see HttpStatus
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 */
	@Override
	public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			final HttpRequestMethodNotSupportedException ex, 
			final HttpHeaders headers, 
			final HttpStatus status, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.METHOD_NOT_ALLOWED.name(), 
				HttpStatus.METHOD_NOT_ALLOWED.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.METHOD_NOT_ALLOWED.name())
		.errorCode(HttpStatus.METHOD_NOT_ALLOWED.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.METHOD_NOT_ALLOWED);
	}
	
	/**
	 * Method responsible for processing the exception that is generated when the fields of the input 
	 * object do not meet their respective validation expression 
	 * @param MethodArgumentNotValidException ex
	 * @param HttpHeaders headers
	 * @param HttpStatus status
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see ResponseEntity
	 * @see Object
	 * @see MethodArgumentNotValidException
	 * @see HttpHeaders
	 * @see HttpStatus
	 * @see WebRequest
	 * @see ResponseMessageVO
	 */
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(
			final MethodArgumentNotValidException ex, 
			final HttpHeaders headers, 
			final HttpStatus status, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.BAD_REQUEST.name())
		.errorCode(HttpStatus.BAD_REQUEST.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Method responsible for processing the exception that is generated when the url is malformed
	 * @param MissingServletRequestParameterException ex
	 * @param HttpHeaders headers
	 * @param HttpStatus status
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see ResponseEntity
	 * @see Object
	 * @see MissingServletRequestParameterException
	 * @see HttpHeaders
	 * @see HttpStatus
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 */
	@Override
	public ResponseEntity<Object> handleMissingServletRequestParameter(
			final MissingServletRequestParameterException ex, 
			final HttpHeaders headers, 
			final HttpStatus status, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.BAD_REQUEST.name())
		.errorCode(HttpStatus.BAD_REQUEST.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Method responsible for processing the exception that is generated when the request format is incorrect
	 * @param HttpMediaTypeNotSupportedException ex
	 * @param HttpHeaders headers
	 * @param HttpStatus status
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see ResponseEntity
	 * @see Object
	 * @see HttpMediaTypeNotSupportedException
	 * @see HttpHeaders
	 * @see HttpStatus
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 */
	@Override
	public ResponseEntity<Object> handleHttpMediaTypeNotSupported(
			final HttpMediaTypeNotSupportedException ex, 
			final HttpHeaders headers, 
			final HttpStatus status, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.BAD_REQUEST.name())
		.errorCode(HttpStatus.BAD_REQUEST.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Method responsible for processing the exception that is generated when the body of a petition is not present
	 * @param HttpMessageNotReadableException ex
	 * @param HttpHeaders headers
	 * @param HttpStatus status
	 * @param WebRequest request
	 * @return ResponseEntity<Object> object which contains the result
	 * @see ResponseEntity
	 * @see Object
	 * @see HttpMessageNotReadableException
	 * @see HttpHeaders
	 * @see HttpStatus
	 * @see WebRequest
	 * @see GlobalTransportException
	 * @see ResponseMessageVO
	 */
	@Override	
	public ResponseEntity<Object> handleHttpMessageNotReadable(
			final HttpMessageNotReadableException ex, 
			final HttpHeaders headers, 
			final HttpStatus status, 
			final WebRequest request) {
		this.globalTransportException = new GlobalTransportException(
				ResponseMessageVO.ERROR_MESSAGE, 
				HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), 
				request.getDescription(false), 
				ex.getMessage());
		this.logDto = new LogDTO();
		this.logDto.date(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())).
		httpCode(HttpStatus.BAD_REQUEST.name())
		.errorCode(HttpStatus.BAD_REQUEST.value())
		.uuid(UUIDUtil.getUUID())
		.message(ex.getMessage())
		.trace(ExceptionHelper.convertStackTraceToString(ex));
		LogUtil.error(log, this.logDto.toString());
		return new ResponseEntity<Object>(this.globalTransportException, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	public ResponseEntity<Object> handleAsyncRequestTimeoutException(
			AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
		return new ResponseEntity<Object>("test AsyncRequestTimeoutException", HttpStatus.BAD_REQUEST);
	}
	
}