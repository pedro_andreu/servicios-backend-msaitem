/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.aitem.exception;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Transport type class responsible for encapsulating the properties of the Exception
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see String
 * @see Integer
 */
@Data
@AllArgsConstructor
@Component("globalTransportException")
public class GlobalTransportException implements Serializable {
	
	private static final long serialVersionUID = -5137597154759427386L;
	@JsonProperty(value = "type")
	private String type;
	@JsonProperty(value = "status")
	private String status;
	@JsonProperty(value = "code")
	private Integer code;
	@JsonProperty(value = "help_url")
	private String helpUrl;
	@JsonProperty(value = "message")
	private String message;
	
	/**
	 * Constructor without parameters
	 */
	@SuppressWarnings("unused")
	private GlobalTransportException() { }

}