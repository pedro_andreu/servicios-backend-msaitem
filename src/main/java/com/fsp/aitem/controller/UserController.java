/*
 * Copyright Farmacias San Pablo
 * 17-10-2019
 */
package com.fsp.aitem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.dto.UserDTO;
import com.fsp.aitem.service.UserService;
import com.fsp.commonutil.exception.DBAccessException;

/**
 * Controller type class responsible for managing the http requests of the user
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see UserService
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	@Qualifier("userService")
	private UserService userService;
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all providers
	 * @param String UUI
	 * @see ResponseEntity
	 * @see List
	 * @see UserDTO
	 * @see String
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@GetMapping(value = "/providers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getProviders(
			@RequestParam(value = "query", required = false) final String letter,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return userService.searchUserBytypeAndLetter("Proveedor", letter, uuid);
	}
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all negotiators
	 * @param String UUI
	 * @see ResponseEntity
	 * @see List
	 * @see UserDTO
	 * @see String
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 */
	@GetMapping(value = "/negotiators", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getNegotiators(
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return userService.searchUserBytype("Negociador", uuid);
	}

}