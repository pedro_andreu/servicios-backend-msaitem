/*
 * Copyright Farmacias San Pablo
 * 21-10-2019
 */
package com.fsp.aitem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.json.ItemJSON;
import com.fsp.aitem.json.ItemUpdateJSON;
import com.fsp.aitem.service.ItemService;
import com.fsp.aitem.util.ItemUtil;
import com.fsp.commonutil.dto.ResponseMessageDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

/**
 * Controller type class responsible for managing the http requests of the items catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ItemService
 * @see ItemUtil
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	@Qualifier("itemService")
	private ItemService itemService;
	
	@GetMapping(value = "/create/description", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessageDTO> createDescription(
			@RequestParam(value = "id_division", required = true) final Integer idDivision,
			@RequestParam(value = "product", required = false) final String product,
			@RequestParam(value = "presentation", required = false) final Integer presentation,
			@RequestParam(value = "net_content", required = false) final String netContent,
			@RequestParam(value = "um_net_content", required = false) final Integer umNetContent,
			@RequestParam(value = "brand_provider", required = false) final String brandProvider,
			@RequestParam(value = "variable", required = false) final String variable,
			@RequestParam(value = "active_substance", required = false) final String activeSubstance,
			@RequestParam(value = "farmaceutical_form", required = false) final Integer farmaceuticalForm,
			@RequestParam(value = "concentration", required = false) final String concentration,
			@RequestParam(value = "um_concentration", required = false) final Integer umConcentration,
			@RequestParam(value = "brand_item", required = false) final String brandItem,
			@RequestParam(value = "commercial_name", required = false) final String commercialName
			) 
					throws DBAccessException {
		return this.itemService.createDescription(idDivision, product, presentation, netContent, umNetContent, brandProvider, 
				variable, activeSubstance, farmaceuticalForm, concentration, umConcentration, brandItem, commercialName, UUIDUtil.getUUID());
	}

	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessageDTO> createItem(
			@Valid @RequestBody final ItemJSON itemJSON,
			@RequestParam(value = "user_id", required = true) final Integer id) 
					throws DBAccessException {
		return this.itemService.addItem(itemJSON, id, UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemJSON> getItem(
			@PathVariable(name =  "id", required = true) final Integer id,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.itemService.searchItem(id, uuid);
	}

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ItemJSON>> getItems(
			@RequestParam(value = "user_id", required = true) final Integer idUser,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.itemService.searchItems(idUser, UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/edit/{id}", produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<ItemUpdateJSON> searchModifyItem(
			@PathVariable(name =  "id", required = true) final Integer id) 
					throws DBAccessException {
		return this.itemService.searchItemUpdate(id, UUIDUtil.getUUID());
	}
	
	@PutMapping(value = "/edit/modify/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<ResponseMessageDTO> modifyItem(
			@PathVariable(name =  "id", required = true) final Integer id,
			@Valid @RequestBody final ItemUpdateJSON itemJSON) 
					throws DBAccessException {
		return this.itemService.updateItem(itemJSON, UUIDUtil.getUUID());
	}
	
	@PostMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessageDTO> deleteItem(
			@PathVariable(name =  "id", required = true) final Integer id)
					throws DBAccessException {
		return this.itemService.deleteItem(id, UUIDUtil.getUUID());
	}

}