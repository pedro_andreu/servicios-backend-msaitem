/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.service.SalubrityFractionService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.util.UUIDUtil;

/**
 * Controller type class responsible for managing the http requests of the salubrity fraction catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see SalubrityFractionService
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/salubrity-fraction")
public class SalubrityFractionController {
	
	@Autowired
	@Qualifier("salubrityFractionService")
	private SalubrityFractionService salubrityFractionService;
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all salubrity fraction
	 * @param String UUI
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 * @see ResponseEntity
	 * @see UUIDUtil
	 */
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getSalubrityFractions() throws DBAccessException, DBResultQueryException {
		return this.salubrityFractionService.searchAllSalubrityFractions(UUIDUtil.getUUID());
	}

}