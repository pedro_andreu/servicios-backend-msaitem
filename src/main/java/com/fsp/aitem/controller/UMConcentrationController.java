/*
 * Copyright Farmacias San Pablo
 * 23-10-2019
 */
package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.service.UMConcentrationService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

/**
 * Controller type class responsible for managing the http requests of the um concentration catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see UMConcentrationService
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/um-concentration")
public class UMConcentrationController {
	
	@Autowired
	@Qualifier("umConcentrationService")
	private UMConcentrationService umConcentrationService;
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all division
	 * @param String UUI
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @see ResponseEntity
	 * @see UUIDUtil
	 */
	@GetMapping(value = "/presentations", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPresentations() throws DBAccessException {
		return this.umConcentrationService.searchAllPresentations(UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/farmaceutical-form", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getFarmaceuticalForm() throws DBAccessException {
		return this.umConcentrationService.searchAllFarmaceuticalForms(UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/net-content", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getNetContens() throws DBAccessException {
		return this.umConcentrationService.searchAllNetContens(UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/concentrations", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getConcentrations() throws DBAccessException {
		return this.umConcentrationService.searchAllConcentrations(UUIDUtil.getUUID());
	}

}