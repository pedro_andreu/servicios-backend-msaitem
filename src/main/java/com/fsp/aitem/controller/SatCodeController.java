/*
 * Copyright Farmacias San Pablo
 * 06-11-2019
 */
package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.service.SatCodeService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

/**
 * Controller type class responsible for managing the http requests of the sat code catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see SatCodeService
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/sat-code")
public class SatCodeController {
	
	@Autowired
	@Qualifier("satCodeService")
	private SatCodeService satCodeService;
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all sat codes
	 * @param String UUI
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @see ResponseEntity
	 * @see UUIDUtil
	 */
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getSatCode(@RequestParam(value = "query", required = false) final String code) throws DBAccessException {
		return this.satCodeService.searchSatCode(code, UUIDUtil.getUUID());
	}
	
}