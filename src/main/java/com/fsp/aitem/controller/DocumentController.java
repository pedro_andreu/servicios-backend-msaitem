package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fsp.aitem.service.DocumentService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/document")
public class DocumentController {
	
	@Autowired
	@Qualifier("documentService")
	private DocumentService documentService;
	
	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createDoc(
			@RequestParam(value = "documentName1", required = true) final String name1,
			@RequestParam(value = "documentData1", required = true) final MultipartFile multipartFile1,
			@RequestParam(value = "documentName2", required = false) final String name2,
			@RequestParam(value = "documentData2", required = false) final MultipartFile multipartFile2, 
			@RequestParam(value = "documentName3", required = false) final String name3,
			@RequestParam(value = "documentData3", required = false) final MultipartFile multipartFile3, 
			@RequestParam(value = "documentName4", required = false) final String name4,
			@RequestParam(value = "documentData4", required = false) final MultipartFile multipartFile4, 
			@RequestParam(value = "documentName5", required = false) final String name5,
			@RequestParam(value = "documentData5", required = false) final MultipartFile multipartFile5, 
			@RequestParam(value = "documentName6", required = false) final String name6,
			@RequestParam(value = "documentData6", required = false) final MultipartFile multipartFile6, 
			@RequestParam(value = "documentName7", required = false) final String name7,
			@RequestParam(value = "documentData7", required = false) final MultipartFile multipartFile7, 
			@RequestParam(value = "documentName8", required = false) final String name8,
			@RequestParam(value = "documentData8", required = false) final MultipartFile multipartFile8,
			@RequestParam(value = "documentName9", required = false) final String name9,
			@RequestParam(value = "documentData9", required = false) final MultipartFile multipartFile9,
			@RequestParam(value = "documentName10", required = false) final String name10,
			@RequestParam(value = "documentData10", required = false) final MultipartFile multipartFile10,
            @RequestParam(value = "id", required = true) final Integer id
            ) throws DBAccessException {
		return this.documentService.addDoc(id, name1, multipartFile1, name2, multipartFile2, name3, multipartFile3,
				name4, multipartFile4, name5, multipartFile5, name6, multipartFile6, name7, multipartFile7,
				name8, multipartFile8, name9, multipartFile9, name10, multipartFile10, UUIDUtil.getUUID());
	}
	
	@PostMapping(value = "/modify", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> updateImage(
			@RequestParam(value = "id1", required = false) final Integer id1,
			@RequestParam(value = "nameDocument1", required = false) final String name1,
			@RequestParam(value = "documentData1", required = false) final MultipartFile multipartFile1,
			@RequestParam(value = "id2", required = false) final Integer id2,
			@RequestParam(value = "nameDocument2", required = false) final String name2,
			@RequestParam(value = "documentData2", required = false) final MultipartFile multipartFile2, 
			@RequestParam(value = "id3", required = false) final Integer id3,
			@RequestParam(value = "nameDocument3", required = false) final String name3,
			@RequestParam(value = "documentData3", required = false) final MultipartFile multipartFile3, 
			@RequestParam(value = "id4", required = false) final Integer id4,
			@RequestParam(value = "nameDocument4", required = false) final String name4,
			@RequestParam(value = "documentData4", required = false) final MultipartFile multipartFile4,
			@RequestParam(value = "id5", required = false) final Integer id5,
			@RequestParam(value = "nameDocument5", required = false) final String name5,
			@RequestParam(value = "documentData5", required = false) final MultipartFile multipartFile5, 
			@RequestParam(value = "id6", required = false) final Integer id6,
			@RequestParam(value = "nameDocument6", required = false) final String name6,
			@RequestParam(value = "documentData6", required = false) final MultipartFile multipartFile6,
			@RequestParam(value = "id7", required = false) final Integer id7,
			@RequestParam(value = "nameDocument7", required = false) final String name7,
			@RequestParam(value = "documentData7", required = false) final MultipartFile multipartFile7, 
			@RequestParam(value = "id8", required = false) final Integer id8,
			@RequestParam(value = "nameDocument8", required = false) final String name8,
			@RequestParam(value = "documentData8", required = false) final MultipartFile multipartFile8,
			@RequestParam(value = "id9", required = false) final Integer id9,
			@RequestParam(value = "nameDocument9", required = false) final String name9,
			@RequestParam(value = "documentData9", required = false) final MultipartFile multipartFile9,
			@RequestParam(value = "id10", required = false) final Integer id10,
			@RequestParam(value = "nameDocument10", required = false) final String name10,
			@RequestParam(value = "documentData10", required = false) final MultipartFile multipartFile10,
            @RequestParam(value = "id", required = true) final Integer id
            ) throws DBAccessException {
		return this.documentService.updateDoc(id, id1, name1, multipartFile1, id2, name2, multipartFile2, id3, name3, multipartFile3,
				id4, name4, multipartFile4, id5, name5, multipartFile5, id6, name6, multipartFile6, id7, name7, multipartFile7,
				id8, name8, multipartFile8, id9, name9, multipartFile9, id10, name10, multipartFile10, UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDocuments(
			@PathVariable("id") final Integer id,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.documentService.searchDocumentByIdItem(id, uuid);
	}
	
	@GetMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteDocument(
			@PathVariable("id") final Integer id,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.documentService.deleteDocuments(id, uuid);
	}

}