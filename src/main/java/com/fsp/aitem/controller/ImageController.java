package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fsp.aitem.service.ImageService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	@Qualifier("imageService")
	private ImageService imageService;
	
	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createImage(
			@RequestParam(value = "nameImage1", required = false) final String name1,
			@RequestParam(value = "imgData1", required = false) final MultipartFile multipartFile1,
			@RequestParam(value = "nameImage2", required = false) final String name2,
			@RequestParam(value = "imgData2", required = false) final MultipartFile multipartFile2, 
			@RequestParam(value = "nameImage3", required = false) final String name3,
			@RequestParam(value = "imgData3", required = false) final MultipartFile multipartFile3, 
			@RequestParam(value = "nameImage4", required = false) final String name4,
			@RequestParam(value = "imgData4", required = false) final MultipartFile multipartFile4, 
			@RequestParam(value = "nameImage5", required = false) final String name5,
			@RequestParam(value = "imgData5", required = false) final MultipartFile multipartFile5, 
			@RequestParam(value = "nameImage6", required = false) final String name6,
			@RequestParam(value = "imgData6", required = false) final MultipartFile multipartFile6, 
			@RequestParam(value = "nameImage7", required = false) final String name7,
			@RequestParam(value = "imgData7", required = false) final MultipartFile multipartFile7, 
			@RequestParam(value = "nameImage8", required = false) final String name8,
			@RequestParam(value = "imgData8", required = false) final MultipartFile multipartFile8,
			@RequestParam(value = "nameImage9", required = false) final String name9,
			@RequestParam(value = "imgData9", required = false) final MultipartFile multipartFile9,
			@RequestParam(value = "nameImage10", required = false) final String name10,
			@RequestParam(value = "imgData10", required = false) final MultipartFile multipartFile10,
            @RequestParam(value = "id", required = true) final Integer id
            ) throws DBAccessException {
		return this.imageService.addImage(id, name1, multipartFile1, name2, multipartFile2, name3, multipartFile3,
				name4, multipartFile4, name5, multipartFile5, name6, multipartFile6, name7, multipartFile7,
				name8, multipartFile8, name9, multipartFile9, name10, multipartFile10, UUIDUtil.getUUID());
	}
	
	@PostMapping(value = "/modify", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> updateImage(
			@RequestParam(value = "id1", required = false) final Integer id1,
			@RequestParam(value = "nameImage1", required = false) final String name1,
			@RequestParam(value = "imgData1", required = false) final MultipartFile multipartFile1,
			@RequestParam(value = "id2", required = false) final Integer id2,
			@RequestParam(value = "nameImage2", required = false) final String name2,
			@RequestParam(value = "imgData2", required = false) final MultipartFile multipartFile2, 
			@RequestParam(value = "id3", required = false) final Integer id3,
			@RequestParam(value = "nameImage3", required = false) final String name3,
			@RequestParam(value = "imgData3", required = false) final MultipartFile multipartFile3, 
			@RequestParam(value = "id4", required = false) final Integer id4,
			@RequestParam(value = "nameImage4", required = false) final String name4,
			@RequestParam(value = "imgData4", required = false) final MultipartFile multipartFile4,
			@RequestParam(value = "id5", required = false) final Integer id5,
			@RequestParam(value = "nameImage5", required = false) final String name5,
			@RequestParam(value = "imgData5", required = false) final MultipartFile multipartFile5, 
			@RequestParam(value = "id6", required = false) final Integer id6,
			@RequestParam(value = "nameImage6", required = false) final String name6,
			@RequestParam(value = "imgData6", required = false) final MultipartFile multipartFile6,
			@RequestParam(value = "id7", required = false) final Integer id7,
			@RequestParam(value = "nameImage7", required = false) final String name7,
			@RequestParam(value = "imgData7", required = false) final MultipartFile multipartFile7, 
			@RequestParam(value = "id8", required = false) final Integer id8,
			@RequestParam(value = "nameImage8", required = false) final String name8,
			@RequestParam(value = "imgData8", required = false) final MultipartFile multipartFile8,
			@RequestParam(value = "id9", required = false) final Integer id9,
			@RequestParam(value = "nameImage9", required = false) final String name9,
			@RequestParam(value = "imgData9", required = false) final MultipartFile multipartFile9,
			@RequestParam(value = "id10", required = false) final Integer id10,
			@RequestParam(value = "nameImage10", required = false) final String name10,
			@RequestParam(value = "imgData10", required = false) final MultipartFile multipartFile10,
            @RequestParam(value = "id", required = true) final Integer id
            ) throws DBAccessException {
		return this.imageService.modifyImage(id, id1, name1, multipartFile1, id2, name2, multipartFile2, id3, name3, multipartFile3,
				id4, name4, multipartFile4, id5, name5, multipartFile5, id6, name6, multipartFile6, id7, name7, multipartFile7,
				id8, name8, multipartFile8, id9, name9, multipartFile9, id10, name10, multipartFile10, UUIDUtil.getUUID());
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getImages(
			@PathVariable("id") final Integer id,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.imageService.searchImageByIdItem(id, uuid);
	}
	
	@GetMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteImages(
			@PathVariable("id") final Integer id,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.imageService.deleteImage(id, uuid);
	}

}