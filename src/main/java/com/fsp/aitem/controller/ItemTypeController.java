/*
 * Copyright Farmacias San Pablo
 * 22-10-2019
 */
package com.fsp.aitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.service.ItemTypeService;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.exception.DBResultQueryException;
import com.fsp.commonutil.util.UUIDUtil;

/**
 * Controller type class responsible for managing the http requests of the item types catalog
 * @author Miguel Angel Gonzalez Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 * @see ItemTypeService
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/item-type")
public class ItemTypeController {
	
	@Autowired
	@Qualifier("itemTypeService")
	private ItemTypeService itemTypeService;
	
	/**
	 * Method in charge of receiving a request of type GET, to perform the search for all item types
	 * @param String UUI
	 * @return ResponseEntity object which contains the result
	 * @throws DBAccessException exception that is thrown when the database cannot be accessed
	 * @throws DBResultQueryException exception that is thrown when an error occurs during transactions to the database
	 * @see ResponseEntity
	 * @see UUIDUtil
	 */
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getItemTypes() throws DBAccessException, DBResultQueryException {
		return this.itemTypeService.searchAllItemTypes(UUIDUtil.getUUID());
	}

}