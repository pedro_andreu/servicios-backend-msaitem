package com.fsp.aitem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsp.aitem.service.SapService;
import com.fsp.commonutil.dto.ResponseIdDTO;
import com.fsp.commonutil.exception.DBAccessException;
import com.fsp.commonutil.util.UUIDUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/sap")
public class SapController {
	
	@Autowired
	@Qualifier("sapService")
	private SapService sapService;
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseIdDTO> searchBarCode(
			@RequestParam(value = "barcode", required = true) final String barcode,
			@RequestParam(value = "uuid", required = false) final String uuid) 
					throws DBAccessException {
		return this.sapService.searchBarcode(barcode, uuid);
	}
	
	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ResponseIdDTO>> createSap(
			@RequestParam("items") final List<Integer> items) 
					throws DBAccessException {
		return this.sapService.sendItem(items, UUIDUtil.getUUID());
	}

}